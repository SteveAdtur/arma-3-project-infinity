-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for a3pi_framework
CREATE DATABASE IF NOT EXISTS `a3pi_framework` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `a3pi_framework`;

-- Dumping structure for table a3pi_framework.becunt_hacker_logs
CREATE TABLE IF NOT EXISTS `becunt_hacker_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insertTime` datetime NOT NULL,
  `pid` varchar(50) NOT NULL,
  `profilename` varchar(50) NOT NULL,
  `keyterm` text NOT NULL,
  `keyvalue` text NOT NULL,
  `extravalue` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table a3pi_framework.players
CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(50) NOT NULL,
  `profilename` varchar(25) NOT NULL,
  `coplevel` enum('0','1','2','3') DEFAULT '0',
  `mediclevel` enum('0','1','2','3') DEFAULT '0',
  `donlevel` enum('0','1','2','3') DEFAULT '0',
  `stafflevel` enum('0','1','2','3') DEFAULT '0',
  `virtualitems` text,
  `hunger` int(11) DEFAULT '100',
  `thirst` int(11) DEFAULT '100',
  `gear` text,
  `licenses` text,
  `bankbalance` int(25) DEFAULT '0',
  `jailData` text,
  `bloodType` text,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table a3pi_framework.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) NOT NULL DEFAULT '0',
  `classname` varchar(50) NOT NULL DEFAULT '0',
  `plate` varchar(50) NOT NULL DEFAULT '0',
  `inventory` text NOT NULL,
  `gear` text NOT NULL,
  `damage` text NOT NULL,
  `active` enum('0','1') NOT NULL,
  `impound` enum('0','1') NOT NULL,
  `garage` text,
  `maxTrunk` int(11),
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
