/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_fetchVehicle.sqf
Author: Heisen http://heisen.pw
Description: Input active:1
Parameter(s): N/A
************************************************************/


params [
	"_player",
	"_vehicle",
	"_plate",
	"_garage",
	"_type",
	"_savedTrunk",
	"_maxTrunk"
];

[player,"",_plate,"",1] call A3PIsys_fnc_updateVehicle;

_license = _plate splitString "";

diag_log format ["A3PI Server Fetch Vehicle %1 %2 %3",_vehicle,_plate,_garage];

_newVehicle = createVehicle [_vehicle,getMarkerPos(format["%1_point",_garage]),[],0,"CAN_COLLIDE"];

//_newVehicle setObjectTextureGlobal [0,_colour select 0]; //--- Set Texture

//--- Set Vehicle lock status.
_newVehicle lock 2;

//--- Owner, Key Array.
_newVehicle setVariable ["vehicleData",[getPlayerUID _player,[getPlayerUID _player],_plate],true];

//--- License Plate Set
//_newVehicle setObjectTextureGlobal [4,format["\ivory_data\license\%1.paa",_license select 0]];
//_newVehicle setObjectTextureGlobal [5,format["\ivory_data\license\%1.paa",_license select 1]];
//_newVehicle setObjectTextureGlobal [6,format["\ivory_data\license\%1.paa",_license select 2]];
//_newVehicle setObjectTextureGlobal [7,format["\ivory_data\license\%1.paa",_license select 3]];

//_newVehicle setObjectTextureGlobal [8,format["\ivory_data\license\%1.paa",_license select 4]];
//_newVehicle setObjectTextureGlobal [9,format["\ivory_data\license\%1.paa",_license select 5]];
//_newVehicle setObjectTextureGlobal [10,format["\ivory_data\license\%1.paa",_license select 6]];

_loadTrunkSave = getNumber (missionConfigFile >> "Server_Settings" >> "Save_Settings" >> "VehicleInventorySaveLoad");
if (_loadTrunkSave isEqualTo 1) then {
	_newVehicle setVariable ["A3PI_TrunkInventory",_savedTrunk,true]; // TEMP
} else {
	_newVehicle setVariable ["A3PI_TrunkInventory",[],true]; // TEMP
};
_newVehicle setVariable ["_trunkSpace",_maxTrunk,true];