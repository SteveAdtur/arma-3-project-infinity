/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_existPlayer.sqf
Author: Heisen http://heisen.pw
Description: check for existing initial player in table
Parameter(s): N/A
************************************************************/


params [
	"_player"
];

_check = [0, (format["existPlayer:%1", getPlayerUID _player])] call A3PIMySQL_fnc_DBquery;

diag_log format ["A3PI Server Player %1 found %2.",getPlayerUID player,_check];

if !(_check select 0 select 0) then {
	[_player] call A3PIsys_fnc_insertPlayer;
} else {
	_fetch = [format ["selectPlayer:%1", getPlayerUID _player], 2] call A3PIMySQL_fnc_DBasync;
	[_fetch] remoteExec ["A3PI_fnc_requestRecieved",_player getVariable "communicationID"]; 
	diag_log format ["A3PI Server Player %1 found %2.",getPlayerUID player,_fetch];
};
