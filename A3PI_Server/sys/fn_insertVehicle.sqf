/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_insertVehicle.sqf
Author: Heisen http://heisen.pw - Modified by Steve Adtur
Description: Insert vehicle into vehicle table.
Parameter(s): N/A
************************************************************/


params [
	"_player",
	"_vehicle",
	"_spawnName",
	"_maxTrunk"
];

//--- Generate License Plate
_license = call A3PIsys_fnc_generatePlate;
diag_log format["Error = %1",str(_license)];
_vehOutput = format ['"%1"',_vehicle];
if (str(_license) isEqualTo "true") exitWith {[_player,_vehOutput,_spawnName,_maxTrunk] call A3PIsys_fnc_insertVehicle;};

diag_log format ["A3PI Server Debug Plate Result %1",_license];



_insertstr = format ["insertVehicle:%1:%2:%3:%4:%5:%6:%7:%8:%9:%10",getPlayerUID _player, _vehOutput ,str(_license joinString ""),[],[],[],1,0,"OUT",_maxTrunk];
_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery;

diag_log format ["A3PI Server Vehicle UID:(%1) VEHICLE:(%2) LICENSE:(%3) new data created.",getPlayerUID _player,_vehicle,_license];

//--- Set Vehicle lock status.


//--- Owner, Key Array.
//_vehicle setVariable ["vehicleData",[getPlayerUID _player,[getPlayerUID _player],(_license joinString "")],true];

//--- License Plate Set
//_vehicle setObjectTextureGlobal [4,format["\ivory_data\license\%1.paa",_license select 0]];
//_vehicle setObjectTextureGlobal [5,format["\ivory_data\license\%1.paa",_license select 1]];
//_vehicle setObjectTextureGlobal [6,format["\ivory_data\license\%1.paa",_license select 2]];
//_vehicle setObjectTextureGlobal [7,format["\ivory_data\license\%1.paa",_license select 3]];

//_vehicle setObjectTextureGlobal [8,format["\ivory_data\license\%1.paa",_license select 4]];
//_vehicle setObjectTextureGlobal [9,format["\ivory_data\license\%1.paa",_license select 5]];
//_vehicle setObjectTextureGlobal [10,format["\ivory_data\license\%1.paa",_license select 6]];

_newVehicle = createVehicle [_vehicle,getMarkerPos _spawnName,[],0,"CAN_COLLIDE"];

_newVehicle setVariable ["_trunkSpace",_maxTrunk,true];

//_newVehicle setObjectTextureGlobal [0,_colour select 0]; //--- Set Texture

//--- Set Vehicle lock status.
_newVehicle lock 2;
clearWeaponCargo _newVehicle;

//--- Owner, Key Array.
_newVehicle setVariable ["vehicleData",[getPlayerUID _player,[getPlayerUID _player],_license joinString ""],true];
_newVehicle setVariable ["A3PI_TrunkInventory",[],true];