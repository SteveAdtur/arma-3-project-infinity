// Heisen
systemChat "A3PI Server Cleanup Started!";

_cleanup_zones = [
	"A3PI_cleanUpZone_UnionCity",
	"A3PI_cleanUpZone_FallsChurch"
];

fnc_cleanupTerrainObjects = {
	private ["_count"];
	params ["_zone"];
	_count = 0;
	{
		if !((damage _x) isEqualTo 0) then {
			_count = _count + 1;
			_x setDamage 0;
		};
	} forEach (nearestTerrainObjects[getMarkerPos _zone,[],200]);
	[format["System: Repaired %1 terrain objects.",_count],10,"blue"] remoteExec ["A3PI_fnc_msg",-2];
};

for "_i" from 0 to 1 step 0 do {
	sleep 300; //--- Waits 5 Minutes
	{
		[_x] call fnc_cleanupTerrainObjects;
		sleep 60; //--- Cleanup again in 1 Minute
	} forEach _cleanup_zones;
};


