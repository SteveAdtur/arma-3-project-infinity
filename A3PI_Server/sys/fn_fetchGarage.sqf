/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_fetchGarage.sqf
Author: Heisen http://heisen.pw
Description: Query DB for Vehicle listing under specific PUID.
Parameter(s): N/A
************************************************************/


params [
	"_player"
];

//--- Perform Query
_fetch = [format ["fetchGarage:%1", getPlayerUID _player], 2] call A3PIMySQL_fnc_DBasync;

diag_log format ["A3PI Server FETCH Owner:(%1) Garage:(%2)",getPlayerUID _player,_fetch];

//--- Send Query result to Client.
[_fetch] remoteExec ["A3PI_fnc_remoteRecieved",_player getVariable "communicationID"];