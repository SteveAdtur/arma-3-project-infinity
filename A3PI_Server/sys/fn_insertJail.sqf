// Heisen
/*
0: update
1: start- initial
2: delete null
*/

diag_log "1";

params [
	"_convict",
	"_timeArray",
	"_reasons",
	"_type"
];

_compactData = [_timeArray,_reasons];

call {
	if (_type isEqualTo 0) exitWith {
		_insertstr = format ["updateJail:%1:%2",_compactData,(getPlayerUID _convict)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 
		diag_log format ["A3PI Server UPDATE Jail PUID(%1) DATA(%2)",getPlayerUID _convict,_compactData];
	};
	if (_type isEqualTo 1) exitWith {
		_insertstr = format ["updateJail:%1:%2",_compactData,(getPlayerUID _convict)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 
		[(_timeArray select 0),(_timeArray select 1),_reasons] remoteExec ["A3PI_fnc_setupJail",_convict getVariable "CommunicationID"];
		diag_log format ["A3PI Server INITIAL Jail PUID(%1) DATA(%2)",getPlayerUID _convict,_compactData];
	};
	if (_type isEqualTo 2) exitWith {
		_insertstr = format ["updateJail:%1:%2","",(getPlayerUID _convict)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 
		diag_log format ["A3PI Server DELETE Jail PUID(%1) DATA(%2)",getPlayerUID _convict,_compactData];
	};
};

