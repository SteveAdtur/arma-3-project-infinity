/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_updateVehicle.sqf
Author: Heisen http://heisen.pw
Description: Set Vehicle active status after storage to 0 from 1, active.
Parameter(s): N/A
************************************************************/


private ["_type"];

params [
	"_player",
	"_vehicle",
	"_plate",
	"_garage",
	"_type"
];

call {
	if (_type isEqualTo 0) exitWith {
		diag_log (str(_plate));
		_insertstr = format ["updateVehicle:%1:%2:%3",0,str(_garage),str(_plate)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 
		
		if !(_vehicle isEqualTo "ignore") then {
			deleteVehicle _vehicle;
			diag_log format ["A3PI Server Vehicle UID:(%1) LICENSE:(%2) GARAGE:(%3) UPDATE Active 0!",getPlayerUID _player,str(_plate),_garage];
		};
		
	};
	if (_type isEqualTo 1) exitWith {
		diag_log (str(_plate));
		_insertstr = format ["updateVehicle:%1:%2:%3",1,"OUT",str(_plate)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 
		diag_log format ["A3PI Server Vehicle UID:(%1) LICENSE:(%2) GARAGE:(%3) UPDATE Active 1!",getPlayerUID _player,str(_plate),_garage];
	};
	if (_type isEqualTo 2) exitWith {
		_texture = ((getObjectTextures _vehicle) select 0);
		diag_log (str(_plate));
		_insertstr = format ["updateVehicleColour:%1:%2",([_texture]),str(_plate)]; 
		_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery;
	};
	_type = nil;
};