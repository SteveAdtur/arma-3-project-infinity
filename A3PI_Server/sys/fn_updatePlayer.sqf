/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_updatePlayer.sqf
Author: Heisen http://heisen.pw
Description: update existing player?
Parameter(s): N/A
************************************************************/


params [
	"_player",
	"_dataArray"
];

_uid = getPlayerUID _player;

_items = _dataArray select 0;
_hunger = _dataArray select 1;
_thirst = _dataArray select 2;
_gear = _dataArray select 3;
_factoryItems = _dataArray select 4;

_insertstr = format ["updatePlayer:%1:%2:%3:%4:%5:%6",_items,_hunger,_thirst,_gear,_factoryItems,_uid]; 
_insert = [0, _insertstr] call A3PIMySQL_fnc_DBquery; 

diag_log format ["A3PI Server Player %1 update with Save!",getPlayerUID _player];