A3PI_RespawnVehicle = [];

[] spawn {
	for "_i" from 0 to 1 step 0 do {
		[getMarkerPos "A3PI_Mine_1"] call A3PIsys_fnc_spawnRocks;
		[getMarkerPos "A3PI_Mine_2"] call A3PIsys_fnc_spawnRocks;
		[getMarkerPos "A3PI_Tree_1"] call A3PIsys_fnc_spawnTrees;
		[getMarkerPos "A3PI_Tree_2"] call A3PIsys_fnc_spawnTrees;
		sleep 30;
	};
};

[] spawn A3PIsys_fnc_cleanup;
[] spawn A3PIsys_fnc_handlePay;

A3PI_TOWTRUCKER_PLAYERLIST = [];
publicVariable "A3PI_TOWTRUCKER_PLAYERLIST";

//--- Base Tax
A3PI_Tax = 1.00;
publicVariable "A3PI_Tax";

A3PI_Server_isReady = true;
publicVariable "A3PI_Server_isReady";

diag_log "A3PI Server Ready!";

