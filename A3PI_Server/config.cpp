class CfgPatches {
    class A3PI_Server {
        units[] = {"C_man_1"};
        weapons[] = {};
        requiredAddons[] = {"A3_Data_F","A3_Soft_F","A3_Soft_F_Offroad_01","A3_Characters_F"};
        fileName = "A3PI_Server.pbo";
        author = "Heisen";
    };
};

class CfgFunctions {
    class MySQL_Core {
        tag = "A3PIMySQL";
        class MySQL
        {
            file = "\A3PI_Server\mysql";
            class DBasync {};
			class DBquery {};
			class DBstrip {};
            class DBinit {};
        };
    };
	class sys_Core {
        tag = "A3PIsys";
        class sys
        {
            file = "\A3PI_Server\sys";
            class insertPlayer {};
			class existPlayer {};
			class updatePlayer {};
			class insertVehicle {};
			class updateVehicle {};
			class fetchGarage {};
			class fetchVehicle {};
			class updateBank {};
			class fetchBank {};
			class updateLicense {};
			class insertJail {};
			class cleanup {};
            class cleanupVehicles {};
			class handlePay {};
        };
		class func
		{
			file = "\A3PI_Server\func";
			class generatePlate {};
			class spawnRocks {};
			class spawnTrees {};
			class pushbackNewVehicle {};
			class respawnNewVehicleLoop {};
			class growPlant {};
		};
    };
	class becunt {
        tag = "bec";
        class dir
        {
            file = "\A3PI_Server\becunt";
			class data {};
            class run {};
			class log {};
        };
    };
};