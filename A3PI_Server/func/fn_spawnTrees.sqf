/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_spawnTrees.sqf
Author: Heisen http://heisen.pw
Description: Spawn Trees
Parameter(s): N/A
************************************************************/


private ["_countArr","_num"];
params ["_svrTreeMarker"];

_treeMax = 15;
_treeTypes = ["Paper_Mulberry"]; //--- Tree Types

_countArr = [];
{
	if ((typeOf _x) IN _treeTypes) then {
		_countArr pushback _x;
	};
} forEach (nearestObjects[_svrTreeMarker,[],50]);

if ((count _countArr) < _treeMax) exitWith {
	_arrayIndicie = selectRandom _treeTypes;
	_rock = _arrayIndicie createVehicle [(_svrTreeMarker select 0)+random(25),(_svrTreeMarker select 1)+random(25),(_svrTreeMarker select 2)+random(25)];
	_rock setVariable ["treeHealth",100,true];
	
	call {
		if (_arrayIndicie isEqualTo "Paper_Mulberry") then {_num = 0;};
		_num;
	};
	_rock setVariable ["treeType",_num,true];
	
	
};