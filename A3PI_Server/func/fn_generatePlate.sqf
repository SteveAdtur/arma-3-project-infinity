/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_generatePlate.sqf
Author: Heisen http://heisen.pw
Description: Generate vehicle plate
Parameter(s): N/A
************************************************************/


private ["_plate"];
_plate = [];

_characters = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];

_one = _characters select round(random [0,12.5,25]);
_two = _characters select round(random [0,12.5,25]);
_three = _characters select round(random [0,12.5,25]);
_four = _characters select round(random [0,12.5,25]);

_five = round(random [0,4.5,9]); 
_six = round(random [0,4.5,9]); 
_seven = round(random [0,4.5,9]); 

_plate = [_one,_two,_three,_four,str(_five),str(_six),str(_seven)];

_check = [0, (format["existVehiclePlate:%1",parseText(_plate joinString "")])] call A3PIMySQL_fnc_DBquery;

diag_log format ["A3PI ServerVehicle Plate exists %1",_check];

if (_check select 0 select 0) then {
	_plate = _check select 0 select 0;
};

_plate;