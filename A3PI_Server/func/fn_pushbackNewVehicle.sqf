/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_pushbackNewVehicle.sqf
Author: Heisen http://heisen.pw
Description: Respawn vehicle pushback.
Parameter(s): N/A
************************************************************/


params [
	"_cursorObject"
];
_position = getPos _cursorObject;
_dir = getDir _cursorObject;
A3PI_RespawnVehicle pushback [_cursorObject,_position,_dir];