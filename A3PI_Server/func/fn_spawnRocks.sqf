/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_spawnRocks.sqf
Author: Heisen http://heisen.pw
Description: Spawn Rocks
Parameter(s): N/A
************************************************************/


private ["_countArr","_num"];
params ["_svrMineMarker"];

_rockMax = 15;
_rockTypes = ["Land_W_sharpStone_03","Land_Limestone_01_03_F"]; //--- Rock Types

_countArr = [];
{
	if ((typeOf _x) IN _rockTypes) then {
		_countArr pushback _x;
	};
} forEach (nearestObjects[_svrMineMarker,[],50]);

if ((count _countArr) < _rockMax) exitWith {
	_arrayIndicie = selectRandom _rockTypes;
	_rock = _arrayIndicie createVehicle [(_svrminemarker select 0)+random(25),(_svrminemarker select 1)+random(25),(_svrminemarker select 2)+random(25)];
	_rock setVariable ["rockHealth",100,true];
	
	call {
		if (_arrayIndicie isEqualTo "Land_W_sharpStone_03") then {_num = 0;};
		if (_arrayIndicie isEqualTo "Land_Limestone_01_03_F") then {_num = 1;};
		_num;
	};
	_rock setVariable ["rockType",_num,true];
	
	
};