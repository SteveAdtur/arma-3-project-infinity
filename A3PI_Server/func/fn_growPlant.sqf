/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_growPlant.sqf
Author: Heisen http://heisen.pw
Description: Grow Plant.
Parameter(s): N/A
************************************************************/


params ["_plant","_type"];

_timer = getArray(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Farming" >> "farmingTimer") select _type;

_plant setVariable ["plant_timer",_timer,true];

for "_i" from _timer to 0 step -1 do {
	_plant setVariable ["plant_timer",_i,true];
	if (_i == 540) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
	};
	if (_i == 480) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
		};
	if (_i == 420) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
		};
	if (_i == 360) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
		};
	if (_i == 320) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
		};
	if (_i == 260) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.1];
		};
	if (_i == 200) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.05];
		};
	if (_i == 140) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.05];
		};
	if (_i == 80) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.05];
		};
	if (_i == 20) then {
		_plant setPos [(getPos _plant) select 0,(getPos _plant) select 1,((getPos _plant) select 2) + 0.05];
		};
		if (_i == 1) exitWith {
			_plant setVariable ["plantType",_type,true];
			_random = (round(random 100));
			if !(_random >=25) then {
				_plant setVariable ["plant_failure",true,true];
			} else {
			_plant setVariable ["plant_failure",false,true];
		};
	};
	sleep 1;
};