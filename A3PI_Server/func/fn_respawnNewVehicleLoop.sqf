/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_respawnNewVehicleLoop.sqf
Author: Heisen http://heisen.pw
Description: N/A
Parameter(s): N/A
************************************************************/


if (A3PI_RespawnVehicle isEqualTo []) exitWith {
	diag_log "A3PI Server RESPAWN VEHICLE array EMPTY!";
};

{
	_vehicle = typeOf(_x select 0);
	_position = _x select 1;
	_dir = _x select 2;
	_near = nearestObjects [_position,[_vehicle],3];
	
	diag_log format ["%1 %2",_x,_near];
	
	if (_near isEqualTo []) then {
	
		_newVehicle = createVehicle [_vehicle,_position,[],0,"CAN_COLLIDE"];
		_newVehicle setDir _dir;
		_newVehicle setVariable ["vehiclePurchasable",true,true]; 
		_newVehicle setvehiclelock "LOCKED";
		
		A3PI_RespawnVehicle deleteAt (_forEachIndex);
		diag_log format ["A3PI Server RESPAWN VEHICLE ARRAY DELETE AT (%1) NEW (%2)",_forEachIndex,A3PI_RespawnVehicle];
	} else {
		diag_log format ["A3PI SErver RESPAWN VEHICLE ARRAY blocked %1",_near];
	};
	
} forEach A3PI_RespawnVehicle;

