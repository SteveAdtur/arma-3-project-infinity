/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_setGear.sqf
Author: Heisen http://heisen.pw
Description: Set Gear
Parameter(s): N/A
************************************************************/


params [
	"_data"
];

player forceAddUniform (_data select 0);
player addVest (_data select 1);
player addBackpack (_data select 2);
player addHeadgear (_data select 3);

{
	player linkItem _x;
	systemChat format ["Item = %1", _x];
} forEach (_data select 4);

{
	player addItem _x;
	systemChat format ["Item = %1", _x];
} forEach (_data select 5);

player addWeapon (_data select 6);
player addWeapon (_data select 7);

{
	player addPrimaryWeaponItem _x;
} forEach (_data select 8);

{
	player addHandgunItem _x;
} forEach (_data select 9);

{
	player addMagazine _x;
} forEach (_data select 10);