/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_survival.sqf
Author: Heisen http://heisen.pw
Description: Survival aspect, decrease hunger/food. - spawn this
Parameter(s): N/A
************************************************************/


_survivalConfig = (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Survival");

_hungerDecrease = getNumber (_survivalConfig >> "Hunger" >> "decrease");
_thirstDecrease = getNumber (_survivalConfig >> "Thirst" >> "decrease"); 

_hungerSleep = getNumber (_survivalConfig >> "Hunger" >> "decreaseSleep"); 
_thirstSleep = getNumber (_survivalConfig >> "Thirst" >> "decreaseSleep"); 

for "_i" from 0 to 1 step 0 do {
	sleep _thirstSleep;
	A3PI_Thirst = A3PI_Thirst - _thirstDecrease;
	sleep _hungerSleep;
	A3PI_Hunger = A3PI_Hunger - _hungerDecrease;
	A3PI_Thirst = A3PI_Thirst - _thirstDecrease;
};

