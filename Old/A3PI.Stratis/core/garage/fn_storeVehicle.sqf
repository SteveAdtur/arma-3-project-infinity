/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_storeVehicle.sqf
Author: Heisen http://heisen.pw
Description: Store Vehicle.
Parameter(s): N/A
************************************************************/


private ["_ownerVehicle"];
_ownerVehicle = nil;

params [
	"_garagePoint"
];

_nearestVehicle = nearestObjects [_garagePoint,["Car","Boat","Air"],15];

{
	_nearestVehicleOwner = (_x getVariable "vehicleData") select 0;
	if (_nearestVehicleOwner == (getPlayerUID player)) then {
		_ownerVehicle = _x;
	};
} forEach _nearestVehicle;

if (isNil "_ownerVehicle") exitWith {};

if (isPlayer _ownerVehicle) exitWith {};

hint format ["Stored Vehicle %2(%1)",typeOf _ownerVehicle,_ownerVehicle];

[player,_ownerVehicle,((_ownerVehicle getVariable "vehicleData") select 2),_garagePoint,0] remoteExec ["A3PIsys_fnc_updateVehicle",2]; // Save Vehicle
[player,_ownerVehicle,((_ownerVehicle getVariable "vehicleData") select 2),nil,2] remoteExec ["A3PIsys_fnc_updateVehicle",2]; // Save Colour

//str((getObjectTextures _ownerVehicle) select 0)