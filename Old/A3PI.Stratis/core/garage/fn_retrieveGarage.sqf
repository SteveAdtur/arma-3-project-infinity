/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_retrieveGarage.sqf
Author: Heisen http://heisen.pw
Description: Retrieved List of Stored vehicles!
Parameter(s): N/A
************************************************************/


[player] remoteExec ["A3PIsys_fnc_fetchGarage",2];

diag_log format ["A3PI Client Vehicle PLAYER:(%1) Retrieval Request!",getPlayerUID player];

hint format ["No Garage on PUID:(%1)",getPlayerUID player];
waitUntil {!(A3PI_dataRemoteRecieved isEqualTo [])};
hint format ["Fetched Garage on PUID:(%1)",getPlayerUID player];

//--- Send Data to display via Garage Dialog
[A3PI_dataRemoteRecieved] call A3PI_fnc_openGarage;