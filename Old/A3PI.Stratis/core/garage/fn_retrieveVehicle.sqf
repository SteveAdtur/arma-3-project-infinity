/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_retrieveVehicle.sqf
Author: Heisen http://heisen.pw
Description: Retrieve Selected Vehicle out of specific garage.
Parameter(s): N/A
************************************************************/


_selected = lbCurSel 1500;
_selectedData = call compile (lbData [1500,_selected]);

if (_selected isEqualTo -1) exitWith {hint "No Vehicle Selected.";};
if !(_selectedData select 2) exitWith {hint "Vehicle Stored in another Garage.";};

_near = nearestObjects [getMarkerPos(format["%1_point",_selectedData select 3]),["Car","Boat","Air"],7.5];
if !(_near isEqualTo []) exitWith {hint "Vehicle blocking point.";};

[player,(_selectedData select 0),(_selectedData select 1),_selectedData select 3,1,_selectedData select 4,_selectedData select 5] remoteExec ["A3PIsys_fnc_fetchVehicle",2];

closeDialog 0;

systemChat format ["%1",_selectedData];