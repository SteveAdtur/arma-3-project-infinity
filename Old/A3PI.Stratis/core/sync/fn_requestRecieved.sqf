/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_requestRecieved.sqf
Author: Heisen http://heisen.pw
Description: Handle Inventory Menu
Parameter(s): N/A
************************************************************/


params [
	"_data"
];

A3PI_recievedData = _data select 0;

A3PI_copLevel = A3PI_recievedData select 0;
player setVariable ["copLevel",A3PI_copLevel,true];
A3PI_medicLevel = A3PI_recievedData select 1;
player setVariable ["medicLevel",A3PI_medicLevel,true];
A3PI_donLevel = A3PI_recievedData select 2;
A3PI_staffLevel = A3PI_recievedData select 3;

_A3PI_items = A3PI_recievedData select 4;

{
	call compile format ["%1 = %2",_x select 0,_x select 1];
} forEach _A3PI_items;


A3PI_Hunger = A3PI_recievedData select 5;
A3PI_Thirst = A3PI_recievedData select 6;

A3PI_Bank = A3PI_recievedData select 9;

[A3PI_recievedData select 7] call A3PI_fnc_setGear;

A3PI_JailFetch = A3PI_recievedData select 10;
A3PI_MedicalBloodType = A3PI_recievedData select 11;

if !(A3PI_JailFetch isEqualTo "") then {
	[(A3PI_JailFetch select 0 select 0),(A3PI_JailFetch select 0 select 1),A3PI_JailFetch select 1] call A3PI_fnc_setupJail;
} else {
	
};


//--- Steve, you could do a count of licenses then for "_i"..
A3PI_LicenseData = A3PI_recievedData select 8;
//--- Do a check if data is "" blank
player setVariable["A3PI_License",A3PI_LicenseData,true];
player setVariable["A3PI_Driver_License",A3PI_LicenseData select 0 select 1,true];
player setVariable["A3PI_Truck_License",A3PI_LicenseData select 1 select 1,true];
player setVariable["A3PI_Pilot_License",A3PI_LicenseData select 2 select 1,true];
player setVariable["A3PI_Firearm_License",A3PI_LicenseData select 3 select 1,true];
player setVariable["A3PI_Rifle_License",A3PI_LicenseData select 4 select 1,true];




