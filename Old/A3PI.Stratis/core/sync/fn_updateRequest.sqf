/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_updateRequest.sqf
Author: Heisen http://heisen.pw
Description: Save Items
Parameter(s): N/A
************************************************************/


if (A3PI_SyncRecent) exitWith {[localize "STR_Notification_SyncRecent",10,"blue"] call A3PI_fnc_msg;};
A3PI_SyncRecent = true;

private ["_itemsSave"];
_itemsSave = [];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	if ((call compile(configName _x)) >= 1) then {
		_itemsSave pushback [configName _x,call compile(configName _x)];
	};
} forEach _items;
[localize "STR_Notification_SyncData",10,"blue"] call A3PI_fnc_msg;

//--- Object, Data to Save (Items.)
[player,[_itemsSave,A3PI_Hunger,A3PI_Thirst,(call A3PI_fnc_getGear)]] remoteExec ["A3PIsys_fnc_updatePlayer",2];

sleep 300;
A3PI_SyncRecent = false;

