//Steve
disableSerialization;
params[
	"_drug"
];

if(_drug == "Item_Redgull")exitWith
{
	player enableStamina false;
	sleep 180;
	player enableStamina true;
};

if(_drug == "Item_Amphet")exitWith
{
	_dmg = damage player;
	player setVariable ["_onDrugs",true,true];
	player setDamage 0;
	sleep 180;
	_newdmg = damage player;
	_newdmg = _newdmg + _dmg;
	player setVariable ["_onDrugs",false,true];
	if(_newdmg >= 0.9) then {
			player setVariable ['A3PI_Cuffed',0,true]; 
			[1] call A3PI_fnc_restrainAdditions;
			player setDamage 0.9;
			player setUnconscious true;
			player allowDamage false;
			systemChat format ["Life state of player = %1" ,lifeState player];
			[] spawn A3PI_fnc_respawn;
	}else{
		player setDamage _newdmg;
	};
	[_drug] spawn A3PI_fnc_drugLonglast;
};

if(_drug == "Item_Heroin")exitWith
{
	_dmg = damage player;
	player setVariable ["_onDrugs",true,true];
	_dmg = _dmg - 0.35;
	if(_dmg < 0) then {_dmg = 0};
	player setDamage _dmg;
	player allowSprint false;
	sleep 180;
	player allowSprint true;	
	player setVariable ["_onDrugs",false,true];
	[_drug] spawn A3PI_fnc_drugLonglast;
};

if(_drug == "Item_Benzo")exitWith
{
	player setVariable ["_onDrugs",true,true];
	player setUnitRecoilCoefficient 0.75;
	sleep 120;
	player setUnitRecoilCoefficient 0.85;
	sleep 60;
	player setUnitRecoilCoefficient 1;
	player setVariable ["_onDrugs",false,true];
	enableCamShake true;
	addCamShake [4, 120, 45];
	[_drug] spawn A3PI_fnc_drugLonglast;
};

if(_drug == "Item_GHB")exitWith 
{
	player setVariable ["_onDrugs",true,true];
	sleep 60;
	player setVariable ['A3PI_Cuffed',0,true]; 
	[1] call A3PI_fnc_restrainAdditions;
	player setUnconscious true;
	sleep 120;
	player setUnconscious false;
	player setVariable ["_onDrugs",false,true];
	[_drug] spawn A3PI_fnc_drugLonglast;
};