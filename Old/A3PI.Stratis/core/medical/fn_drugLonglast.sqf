// Steve

disableSerialization;

params[
	"_drugType"
	];
	
_drugName = getText(missionConfigFile >> "Server_Items" >> _drugType >> "displayName");	

_drugArray = player getVariable "drugsIngested";

_drugArray pushBack _drugName;

player setVariable ["drugsIngested",_drugArray,true];
