//Steve
disableSerialization;


_respawnTimer = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Medical" >> "respawnTimer");
_isDone = false;
_exit = false;
72 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
_text = "Waiting to Respawn";
_mCount = -1;
{
if(lifeState _x == "INCAPACITATED")then
{
 _mCount = _mCount +1;
};
}forEach (allPlayers);
_mName = format["DownedPlayer%1",_mCount];
player setVariable ["Marker",_mName];
_marker1 = createMarker [_mName, position player];
_mName setMarkerColor "ColorRed";
_mName setMarkerText "Downed Player";
_mName setMarkerShape "ICON";
_mName setMarkerType "mil_objective";


for "_i" from _respawnTimer to 0 step -1 do {
	_current = progressPosition ((_display)displayCtrl 1001);
	((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _respawnTimer));
	((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2 seconds remaining!",_text,_i];
	sleep 1;
	if(player getVariable "revived")then{
		72 cutText ["","PLAIN"];
		_exit = true;
	};

};
if (_exit) exitWith {
	["You were revived",10,"green"] call A3PI_fnc_msg;
	player setVariable ["revived",false,true];
};
_isDone = true;
waitUntil {_isDone}; // Wait till progress is done
72 cutText ["","PLAIN"]; 

for "_i" from -1 to 11 step +1 do {
	_hitOut = format["hitIndex%1",_i];
	player setVariable[_hitOut,0,true];
};

_items = "true" configClasses (missionConfigFile >> "Server_Items");
{
	_varName = configName _x;
	_varAmount = call compile _varName;


	if (_varAmount >= 1) then {
		[_varName, _varAmount, false] call A3PI_fnc_handleItem;
	};
} forEach _items;

A3PI_RespawnReady = true;
createDialog "RscDisplay_Respawn";