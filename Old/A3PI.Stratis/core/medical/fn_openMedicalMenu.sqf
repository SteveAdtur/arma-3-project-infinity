// Heisen


private ["_push"];

params [
	"_patient"
];

createdialog "RscDisplay_MedicalMenu"; 

_healInjuries = _patient getVariable "A3PI_healInjuriesHistory";

((findDisplay 19824)displayCtrl 1100) ctrlSetStructuredText parseText format ["<t align='center'>%1</t>",name _patient];

_push = [];
for "_i" from ((count _healInjuries) -1) to 0 step -1 do {
	_push pushback (_healInjuries select _i);
};

{
	lbAdd [1500,(format["%1 - %2",_x select 0,_x select 1])];
} forEach _push;

systemChat str(_push);