// Steve Adtur, Heisen

disableSerialization;


private ["_isDone","_display","_pos","_exit"];
_isDone = false;
_exit = false;

if (A3PI_antiSpam) exitWith {
	["You're already refining..",10,"red"] call A3PI_fnc_msg;
};
A3PI_antiSpam = true;

closeDialog 0; // Close display

_ctrl1 = (findDisplay 19872) displayCtrl 1101;
_itemNumber = lbCurSel 1500;
_itemName = lbData [1500,_itemNumber];

if (_itemNumber isEqualTo -1) exitWith {
	["You've not selected anything..",10,"red"] call A3PI_fnc_msg;
	A3PI_antiSpam = false;
};

_itemArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Unrefined");
_refineTime = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Refining" >> "refineTimer");

if(_itemName IN _itemArray) then {
	
	_stats = getArray(missionConfigFile >> "Server_Items" >> _itemName >> "refineryStats");
	_outputItem = getText(missionConfigFile >> "Server_Items" >> _itemName >> "refined");
	_varAmount = call compile _itemName;

	_required = _stats select 0;
	_output = _stats select 1;
	if(_varAmount < _required)then {
		["Not enough materials",10,"red"] call A3PI_fnc_msg;
		A3PI_antiSpam = false;
	}else{
		["Refining Ore",10,"blue"] call A3PI_fnc_msg;
		
		32 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Refining Ore";
		_pos = getPos player;
		
		for "_i" from 0 to _refineTime step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _refineTime));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			sleep 1;
			if (player distance _pos > 10) exitWith {
				32 cutText ["","PLAIN"];
				A3PI_antiSpam = false;
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		
		_amount = _varAmount/_required;
		_realAmount = floor _amount;
		_realInput = _realAmount*_required;
		_realOutput = _output*_realAmount;
		[_itemName, _realInput, false] call A3PI_fnc_handleItem;
		[_outputItem, _realOutput, true] call A3PI_fnc_handleItem;
		
		["Refining Completed",10,"green"] call A3PI_fnc_msg;
		32 cutText ["","PLAIN"]; // Remove Progress Bar
		A3PI_antiSpam = false;
	};
};