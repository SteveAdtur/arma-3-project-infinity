// Heisen


params [
	"_request"
];


call {
	if (_request isEqualTo "REVIVE") exitWith {
		if (lbCurSel 1500 isEqualTo -1) exitWith {
			[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;
		};
		[format[localize"STR_AdminMenu_RevivePlayer",lbData[1500,lbCurSel 1500]],10,"green"] call A3PI_fnc_msg;
		[player] remoteExec ["A3PI_fnc_adminRevive",parseNumber(lbData[1500,lbCurSel 1500])];
		[([player,format["Revived Player (%1)",lbText[1500,lbCurSel 1500]],"",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
	};
	if (_request isEqualTo "SPAWN") exitWith {
		call {
			if (A3PI_AdminSpawnBox isEqualTo "ITEMS") exitWith {
				_data = lbData [1501,(lbCurSel 1501)];
				_reciever = lbData [1500,(lbCurSel 1500)];
				if ((lbCurSel 1500 isEqualTo -1)or(lbCurSel 1501 isEqualTo -1)) exitWith {
					[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;
				};
				[_data,1,true] remoteExec ["A3PI_fnc_handleItem",parseNumber(_reciever)];
				[([player,format["Spawned (%1) Virtual Item to Inventory",_data],"",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
			};
		};
	};
	if (_request isEqualTo "SWITCH") exitWith {
		call {
			private ["_countNum"];
			_countNum = -1;
			if (A3PI_AdminSpawnBox isEqualTo "ITEMS") exitWith {
				_items = "true" configClasses (missionConfigFile >> "Server_Items");
				{
					if !((configName _x) isEqualTo "Item_Cash") then {
						_isPhysicalClass = (_x >> "physicalClass");
						if ((getText(_isPhysicalClass)) isEqualTo "") then {
							_countNum = _countNum + 1;
							_varDisplayName = getText (_x >> "displayName");
							lbAdd [1501,format["%1",localize(_varDisplayName)]];
							lbSetData [1501,_countNum,(configName _x)];
						};
					};
				} forEach _items;
			};
		};
	};
	if (_request isEqualTo "SETBANK") exitWith {
		//Waiting for Steve to fix banking
	};
	if (_request isEqualTo "SETCASH") exitWith {
		call compile format["Item_Cash = %1",parseNumber(ctrlText 1400)];
		[([player,format["SET LOCAL CASH to %1",parseNumber(ctrlText 1400)],"",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
	};
	if (_request isEqualTo "SPECTATE") exitWith {
		[] spawn A3PI_fnc_adminSpectate;
	};
};