// Heisen
// Setup Spectator Mode - not completed


if (isNil "A3PI_AdminSpector") then { 
	A3PI_AdminSpector = false;
};

if (A3PI_AdminSpector) then {
	A3PI_AdminSpector = false;
	[localize"STR_AdminMenu_AdminSpectateOff",10,"blue"] call A3PI_fnc_msg;
	[A3PI_AdminSpectorGear] call A3PI_fnc_setGear;
	[([player,"Spectate Deactivated","",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
} else {
	A3PI_AdminSpector = true;
	A3PI_AdminSpectorGear = call A3PI_fnc_getGear;
	call A3PI_fnc_wipeGear;
	player forceAddUniform "U_B_Protagonist_VR"; //--- Admin Clothing
	[localize"STR_AdminMenu_AdminSpectateOn",10,"blue"] call A3PI_fnc_msg;
	[([player,"Spectate Activated","",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
	//call A3PI_fnc_adminSpectateStart;
};
