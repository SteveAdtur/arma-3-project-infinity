// Heisen, Steve


private ["_maxWeightValue","_maxWeightValuePly","_countNum"];


params [
	"_vehicle"
];

lbClear 1500;
lbClear 1501;

_maxWeightValue = 0;

{
	_varAmount = _x select 1;	
	_missionConfigData = (missionConfigFile >> "Server_Items" >> (_x select 0));
	_displayName = getText (_missionConfigData >> "displayName");
	_varWeightValue = getNumber (_missionConfigData >> "weightValue");
	_maxWeightValue = _maxWeightValue + (_varWeightValue * _varAmount);
	lbAdd [1501,format["%1x %2 - %3.kg",_varAmount,localize(_displayName),(_varWeightValue * _varAmount)]];
	lbSetData [1501,_forEachIndex,_x select 0];
} forEach (_vehicle getVariable "A3PI_TrunkInventory");

_maxVehicleValue = getNumber(missionConfigFile >> "Server_Vehicles" >> (typeOf _vehicle) >> "maxStore");
((findDisplay 998)displayCtrl 1002) ctrlSetText format["%1/%2",_maxWeightValue,_maxVehicleValue];

_maxWeightValuePly = 0;
_countNum = -1;

_items = "true" configClasses (missionConfigFile >> "Server_Items");
{
	_varName = configName _x;
	_varAmount = call compile (configName _x);
	if (_varAmount >= 1) then {
		_countNum = _countNum + 1;
		_missionConfigData = (missionConfigFile >> "Server_Items" >> (configName _x));
		_displayName = getText (_missionConfigData >> "displayName");
		_varWeightValue = getNumber (_missionConfigData >> "weightValue");
		_maxWeightValuePly = _maxWeightValuePly + (_varWeightValue * _varAmount);
		lbAdd [1500,format["%1x %2 - %3.kg",_varAmount,localize(_displayName),(_varWeightValue * _varAmount)]];
		lbSetData [1500,_countNum,_varName];
	};
} forEach _items;

_maxVehicleValuePly = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "maxCarry");
((findDisplay 998)displayCtrl 1001) ctrlSetText format["%1/%2",_maxWeightValuePly,_maxVehicleValuePly];
lbSetCurSel[1500,-1];
lbSetCurSel[1501,-1];