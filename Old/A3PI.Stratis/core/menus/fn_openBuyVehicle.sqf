/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_openBuyVehicle.sqf
Author: Heisen http://heisen.pw // Steve Adtur
Description: Buy Vehicle Menu setup.
Parameter(s): N/A
************************************************************/


params [
	"_vehicle"
];

createDialog "RscDisplay_PurchaseVehicle";

_vehicleMissionConfig = (missionConfigFile >> "Server_Vehicles" >> (typeOf _vehicle));
_vehicleConfig = (configFile >> "CfgVehicles" >> (typeOf _vehicle));
_vehicleLibrary = (_vehicleConfig >> "Library");

if(((getNumber(_vehicleMissionConfig >> "copVehicle")) >= 1) && (player getVariable "coplevel" < 1) && (!A3PI_coponduty))exitWith{
	["You cannot buy government vehicles.",10,"red"] call A3PI_fnc_msg;
	closeDialog 0;
};

_vehCost = getNumber(_vehicleMissionConfig >> "buyValue");
_vehOutput = [_vehCost] call BIS_fnc_numberText;

ctrlSetText [1100,(getText(_vehicleConfig >> "displayName"))];
ctrlSetText [1201,(getText(_vehicleConfig >> "editorPreview"))];
((findDisplay 1003) displayCtrl 1101) ctrlSetStructuredText parseText format ["Cost: $%1 <br />Manufacturer: %2 <br />Model: %3",(_vehOutput),(getText(_vehicleLibrary >> "manufacturer")),(getText(_vehicleLibrary >> "model"))];

//--- Add colour radial ctrl


//Library
// go find ivory liabary config file >> pull data display as structured text with picture