/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_handleItem.sqf
Author: Heisen http://heisen.pw
Description: Setup Actions
Parameter(s): N/A
************************************************************/


params [
	"_item",
	"_amount",
	"_bool"
];

_weight = (getNumber (missionConfigFile >> "Server_Items" >> _item >> "weightValue")) * _amount;

//--- Add, true - Remove, false
if (_bool) then {
	if !([_item,_amount] call A3PI_fnc_calculateWeight) then {
		systemChat "here";
		A3PI_Weight = A3PI_Weight + _weight;
		call compile format ["%1 = %1 + %2",_item,_amount];
	} else {
		hint "Carry too much! Can't carry anymore.";
	};
} else {
	A3PI_Weight = A3PI_Weight - _weight;
	call compile format ["%1 = %1 - %2",_item,_amount];
};


