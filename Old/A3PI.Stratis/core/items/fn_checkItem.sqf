/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_checkItem.sqf
Author: Heisen http://heisen.pw
Description: Check item virtual number
Parameter(s): N/A
************************************************************/


params [
	"_item"
];

_sum = call compile _item;
_sum;