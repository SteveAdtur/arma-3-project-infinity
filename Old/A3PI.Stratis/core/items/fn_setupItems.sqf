/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_setupItems.sqf
Author: Heisen http://heisen.pw
Description: Setup Items
Parameter(s): N/A
************************************************************/


_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	call compile (format["%1 = 0",configName _x]); 
} forEach _items;