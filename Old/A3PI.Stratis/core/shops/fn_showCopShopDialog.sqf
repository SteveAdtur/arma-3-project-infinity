// Steve Adtur, Heisen

disableSerialization;

private ["_countNum","_curOb"];
_countNum = -1;
_count = -1;

_itemArray = "true" configClasses (missionConfigFile >> "Server_Items");

_curOb = cursorObject getVariable "shopType";
// Type of var options = BlackMarket, General, Market, FarmMarket, ResTrader, Drug, Gun, CopShop, FBIShop
createDialog "RscDisplay_ShopWindow";

waitUntil {!isNull (findDisplay 19874);};

if(_curOb isEqualTo "CopShop") then {

{
	
	if(A3PI_CopLevel >= _count)then{
		_count = _count +1;
		lbAdd[2100, _x];
		lbSetData[2100,_count,_x];
	};
} forEach["Virtual Item","Cadet","Deputy","Corporal","Sergeant","Command","SERT","High Command", "SERT Command","FBI"];
}else{
	{
	_count = _count +1;
	lbAdd[2100, _x];
	lbSetData[2100,_count,_x];
	}forEach["Virtual Item","Uniforms","Vests","Backpack","Items","Weapons"];
};

_shopType = getArray(missionConfigFile >> "Server_Items" >> (format["Server_Items_%1",_curOb]));

{
	_varName = configName _x;
	_varAmount = call compile _varName;

	if(_varName IN _shopType) then {
		_shopSell = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "sellValue");
		if( _shopSell > 0)then{
			if(_varAmount >= 1)then{
				_countNum = _countNum + 1;
				_varDisplayName = getText (_x >> "displayName");
				
				lbAdd [1500,format["%1 x %2",_varAmount,localize(_varDisplayName)]];
				lbSetData[1500,_countNum,_varName];
				
			};
		};
		
	};
} forEach _itemArray;

_countNum = -1;

{
	
	_varName = configName _x;
	if(_varName IN  _shopType) then {
		_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
		if(_shopBuy > 0) then {
			_countNum = _countNum +1;
			_varDisplayName = getText (_x >> "displayName");
			if(_varDisplayName != "physicalItem")then{
				lbAdd [1501, format["%1 Costs $%2",localize(_varDisplayName), _shopBuy]];
				lbSetData[1501,_countNum,_varName];
			}else{
				_varDisplayName = getText (configFile / "CfgWeapons" / _x / "displayName");
				lbAdd [1501, format["%1 Costs $%2",localize(_varDisplayName), _shopBuy]];
				lbSetData[1501,_countNum,_varName];
			};
		};
		
	};
	
}forEach _itemArray;

lbSetCurSel[2100,0];
call A3PI_fnc_refreshShopDialog;