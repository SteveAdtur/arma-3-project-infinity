// Heisen


params [
	"_type"
];

A3PI_CurrentShop_Interact = _type;

createDialog "RscDisplay_ShopMenu"; 

{
	lbAdd [2100,_x];
	lbSetData [2100,_forEachIndex,_x];
} forEach ["All","Items","Uniforms","Vests","Backpacks","Headgear","Weapons/Magazines"];

["All"] spawn A3PI_fnc_newShopFillData;

shopCamera = "CAMERA" camCreate getPos player;
showCinemaBorder false;
shopCamera cameraEffect ["Internal", "Back"];
shopCamera camSetTarget (player modelToWorld [0,0,1]);
shopCamera camSetPos (player modelToWorld [1,4,2]);
shopCamera camSetFOV .33;
shopCamera camSetFocus [50, 0];
shopCamera camCommit 0;
