//Steve

disableSerialization;



_copNear = false;
_onDutyCops = [];

{
	if(_x getVariable "A3PI_CopOnDuty") then
	{
		if((player distance _x) <= 800) then 
		{
			_copNear = true;
		};
		_onDutyCops pushBack _x;
	};
} forEach(allPlayers);

_curOb = cursorObject;
_typeofRobbery = cursorObject getVariable "RobberyType";

_robberyTimer = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Antagonistic" >> "robberyTimer");
_robberyBase = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Antagonistic" >> "robberyBase");
if(_copNear)then
{
	_robberyTimer = _robberyTimer - (_robberyTimer/4);
};
_exit = false;
_isDone = false;
_location = cursorObject getVariable "storeLoc";
_shopType = cursorObject getVariable "shopType";
_outputString = format["Robbery at %1 %2",_location, _shopType];
if(_typeofRobbery == "Store") exitWith {
	if(currentWeapon player != "")then{
		[_outputString] remoteExec ["hint",_onDutyCops, true];
		75 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
			_text = "Robbery in progress";
			_pos = getPos player;
			
			for "_i" from _robberyTimer to 0 step -1 do {
				_current = progressPosition ((_display)displayCtrl 1001);
				((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _robberyTimer));
				((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2 seconds remaining!",_text,_i]; 
				sleep 1;
				if (player distance _pos > 10) exitWith {
					75 cutText ["","PLAIN"];
					_exit = true;
				};
				if(lifeState player == "INCAPACITATED") exitWith {
					75 cutText ["","PLAIN"];
					_exit = true;
				};
			};
			if (_exit) exitWith {
				["You moved too far away..",10,"red"] call A3PI_fnc_msg;
			};
			
			_isDone = true;
			
			waitUntil {_isDone}; // Wait till progress is done
			
			_rand = random 11;
			_rand = _rand/10;
			_sub = random 2;
			
			if(_sub == 0)then{
			_cashOut = _robberyBase + (_robberyBase * _rand); 
			
			["Item_Cash", _cashOut, true] call A3PI_fnc_handleItem;
			}else{
			_cashOut = _robberyBase - (_robberyBase * _rand); 
			
			["Item_Cash", _cashOut, true] call A3PI_fnc_handleItem;
			75 cutText ["","PLAIN"];
			};
	}else{
		["You need a weapon",10,"red"] call A3PI_fnc_msg;
	};
};
if(_typeofRobbery == "Pharmacy")exitWith {
closeDialog 0;
	if((lbCurSel 1500) isEqualTo -1) exitWith { call A3PI_fnc_robberyDialog;
	if(currentWeapon player != "")then{
	[_outputString] remoteExec ["hint",_onDutyCops, true];
	75 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
	_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Robbery in progress";
		_pos = getPos player;
		
		for "_i" from _robberyTimer to 0 step -1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _robberyTimer));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2 seconds remaining!",_text,_i]; 
			sleep 1;
			if (player distance _pos > 10) exitWith {
				75 cutText ["","PLAIN"];
				_exit = true;
			};
			if(lifeState player == "INCAPACITATED") exitWith {
				75 cutText ["","PLAIN"];
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done

		_item = lbCurSel 1500;
		
		[_item, 10, true] call A3PI_fnc_handleItem;

		75 cutText ["","PLAIN"];
		
	}else{
		["You need a weapon",10,"red"] call A3PI_fnc_msg;
	};
};