// Steve Adtur

disableSerialization;

private ["_countNum"];
_countNum = -1;

_partsArray =  "true" configClasses(missionConfigFile >> "Server_Items");
_listedPart = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Crafting");
	
							
createDialog "RscDisplay_CraftingWindow";

waitUntil {!isNull (findDisplay 19872);};

{
	_varName = configName _x;
	if(_varName IN _listedPart)then {
	
	
	_countNum = _countNum + 1;
	_varDisplayName = getText (_x >> "displayName");
	lbAdd [1500,format["%1",localize(_varDisplayName)]];
	lbSetData[1500,_countNum,_varName];
	};
	
} forEach _partsArray;

