/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_harvestPlant.sqf
Author: Heisen http://heisen.pw
Description: Harvest.
Parameter(s): N/A
************************************************************/


params [
	"_plant"
];

if ((_plant getvariable "plant_timer") > 1) exitWith {
	[(format["Plant not fully grown, %1 seconds remain.",(_plant getvariable "plant_timer")]),10,"red"] call A3PI_fnc_msg;
};

_type = _plant getVariable "plantType";
_failure = _plant getVariable "plant_failure";

deleteVehicle _plant;

if (_failure) exitWith {
	["Failed Harvest",10,"red"] call A3PI_fnc_msg;
};

_byProduct = (getArray(missionConfigFile >> "Server_Items" >> "Server_Items_HarvestedPlants")) select _type;

[_byProduct,1,true] call A3PI_fnc_handleItem;
[format["Harvested %1",localize(getText(missionConfigFile >> "Server_Items" >> _byProduct >> "displayName"))],10,"green"] call A3PI_fnc_msg;