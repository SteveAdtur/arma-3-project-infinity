/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_dutySwitch.sqf
Author: Heisen http://heisen.pw
Description: Duty Switch
Parameter(s): string: cop/medic
************************************************************/


params [
	"_faction"
];

_duty = call compile format ["A3PI_%1Level",_faction];
_side = call compile format ["A3PI_%1OnDuty",_faction];

if (_duty < 1) exitWith {
	["You're not employed into this position.",10,"red"] call A3PI_fnc_msg;
};

if (_side) then {
	call compile format ["A3PI_%1OnDuty = false",_faction];
	["Gone off duty.",10,"blue"] call A3PI_fnc_msg;
	if(_faction == "cop")then{
		A3PI_CopOnDuty = false;
		myjob = "Civ";
		player setVariable ["coplevel",0,true];
		player setvariable ["A3PI_CopOnDuty",false,true];
	}else{
		A3PI_MedicOnDuty = false;
		myjob = "Civ";
		player setVariable ["coplevel",0,true];
		player setvariable ["A3PI_MedicOnDuty",false,true];
	};
} else {
	call compile format ["A3PI_%1OnDuty = true",_faction];
	["Gone on duty.",10,"blue"] call A3PI_fnc_msg;
	if(_faction == "cop")then{
		A3PI_CopOnDuty = true;
		myjob = "Cop";
		player setVariable ["coplevel",1,true];
		player setvariable ["A3PI_CopOnDuty",true,true];
	}else{
		A3PI_MedicOnDuty = true;
		myjob = "EMS";
		player setVariable ["coplevel",1,true];
		player setvariable ["A3PI_MedicOnDuty",true,true];
	};
};