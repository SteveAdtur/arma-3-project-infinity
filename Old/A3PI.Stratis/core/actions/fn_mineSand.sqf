// Heisen


_surfaceType = surfaceType (getPos player);
_isSand = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Mining" >> "sandSurfaceTypes");


if !(_surfaceType IN _isSand) exitWith {
	[localize"STR_Notification_noSand",10,"red"] call A3PI_fnc_msg;
};


A3PI_SandMiner = A3PI_SandMiner + round(random 5);
if (A3PI_SandMiner >= 100) exitWith {
	A3PI_SandMiner = 0;
	["Item_Sand",(round(random 3)),true] call A3PI_fnc_handleItem;
	[localize"STR_Notification_sandMined",10,"green"] call A3PI_fnc_msg;
};