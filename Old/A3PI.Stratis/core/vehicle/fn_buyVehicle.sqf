/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_buyVehicle.sqf
Author: Heisen http://heisen.pw
Description: Purchase Vehicle
Parameter(s): N/A
************************************************************/


params [
	"_vehicle",
	"_colour"
];

_vehicleConfig = (missionConfigFile >> "Server_Vehicles" >> (typeOf _vehicle));

if ((call compile "Item_Cash") < (getNumber (_vehicleConfig >> "buyValue"))) exitWith {["Not enough Cash!",10,"red"] call A3PI_fnc_msg;}; 

if(((getNumber(_vehicleConfig >> "copVehicle")) >= 1) && (player getVariable "coplevel" < 1) && (!A3PI_coponduty))exitWith{
	["You cannot buy government vehicles.",10,"red"] call A3PI_fnc_msg;
	closeDialog 0;
};

//--- Make it so it can't be purchased anymore..
cursorObject setVariable ["vehiclePurchasable",nil,true];

[player,_vehicle,_colour] remoteExec ["A3PIsys_fnc_insertVehicle",2]; 
[_vehicle] remoteExec ["A3PIsys_fnc_pushbackNewVehicle",2];

["Item_Cash",(getNumber (_vehicleConfig >> "buyValue")),false] call A3PI_fnc_handleItem;

["You've Purchased a Vehicle! Key's Added.",10,"blue"] call A3PI_fnc_msg;
_vehicle setVariable ["A3PI_TrunkInventory",[],true];
_vehicle setVariable ["A3PI_TrunkAntiSpam",false,true];
//--- Close Buy dialog
closeDialog 0;
