// Heisen


private ["_found","_vehicleWeight"];

params [
	"_vehicle"
];

_vehicleInv = _vehicle getVariable "A3PI_TrunkInventory";
_vehicleInvSelected = lbData [1500,lbCurSel 1500];

_amount = parseNumber(ctrlText 1400);

if ((lbCurSel 1500) isEqualTo -1) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};
if (_amount < 1) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};
if (call compile (_vehicleInvSelected) < _amount) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};

_vehicleWeight = 0;
{
	_theItem = getNumber(missionConfigFile >> "Server_Items" >> (_x select 0) >> "weightValue");
	_vehicleWeight = _vehicleWeight + (_theItem * (_x select 1));
} forEach _vehicleInv;



_maxVehicleValue = getNumber(missionConfigFile >> "Server_Vehicles" >> (typeOf _vehicle) >> "maxStore");
_theItem = getNumber(missionConfigFile >> "Server_Items" >> _vehicleInvSelected >> "weightValue");



if ((_vehicleWeight + (_theItem * _amount)) > _maxVehicleValue) exitWith {};

[_vehicleInvSelected,_amount,false] call A3PI_fnc_handleItem;

_found = -1;
{
	_find = ((_x select 0) find _vehicleInvSelected);
	if !(_find isEqualTo -1) exitWith {
		_found = _forEachIndex;
		_current = _x select 1;
		_x set[1,(_current + _amount)]; 

		[_vehicle] spawn A3PI_fnc_updateVehicleInventory;

	};
} forEach _vehicleInv;

_vehicle setVariable ["A3PI_TrunkInventory",_vehicleInv,true];

if !(_found isEqualTo -1) exitWith {};
_vehicleInv pushback [_vehicleInvSelected,_amount];
_vehicle setVariable ["A3PI_TrunkInventory",_vehicleInv,true];
[_vehicle] spawn A3PI_fnc_updateVehicleInventory;

