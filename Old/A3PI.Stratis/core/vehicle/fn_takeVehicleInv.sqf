// Heisen, Steve


private ["_found"];

params [
	"_vehicle"
];

_vehicleInv = _vehicle getVariable "A3PI_TrunkInventory";
_vehicleInvSelected = lbData [1501,lbCurSel 1501];

_amount = parseNumber(ctrlText 1400);

if ((lbCurSel 1501) isEqualTo -1) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};
if (_amount < 1) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};

_maxCarry = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "maxCarry");
_theItem = getNumber(missionConfigFile >> "Server_Items" >> _vehicleInvSelected >> "weightValue");

if ((A3PI_Weight + (_theItem * _amount)) > _maxCarry) exitWith {};

systemChat str(_vehicleInvSelected);

_found = -1;
{
	systemChat str(_x select 0);
	_find = ((_x select 0) find _vehicleInvSelected);
	if !(_find isEqualTo -1) then {
		_found = _forEachIndex;
		_currentAmount = _vehicleInv select _found select 1;
		if (_amount > _currentAmount) exitWith {[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;};
		if (_amount isEqualTo _currentAmount) then {
			_vehicleInv deleteAt _found;
			[(_x select 0),_amount,true] call A3PI_fnc_handleItem;
			systemChat "REMOVE OUT OF ARRAY";
		} else {
			(_vehicleInv select _found) set [1,(_currentAmount - _amount)];
			[(_x select 0),_amount,true] call A3PI_fnc_handleItem;
			systemChat "SET";
		};
	};
	systemChat str(_find);
} forEach _vehicleInv;

_vehicle setVariable ["A3PI_TrunkInventory",_vehicleInv,true];

[_vehicle] spawn A3PI_fnc_updateVehicleInventory;
systemChat "Done??";
