// Heisen


params [
	"_hours",
	"_minutes",
	"_reasons"
];

jailHours = _hours;
jailMinutes = _minutes;
jailSeconds = 59;

jailReasons = _reasons;

A3PI_Jailed = true;

[localize"STR_Notification_JailSetupDone",10,"blue"] call A3PI_fnc_msg;

98 cutRsc ["RscTitle_JailTimer","PLAIN"];

[] spawn {
	while {A3PI_Jailed} do {
		jailSeconds = jailSeconds - 1;
		if (jailMinutes > 0 && jailSeconds == 0) then {
			jailMinutes = jailMinutes - 1;
			jailSeconds = 59;
			[player,[jailHours,jailMinutes,jailSeconds],jailReasons,0] remoteExec ["A3PIsys_fnc_insertJail",2];
		} else {
			if (jailHours > 0 && jailMinutes == 0 && jailSeconds == 0) then {
				jailHours = jailHours - 1;
				jailMinutes = 59;
				jailSeconds = 59;
				[player,[jailHours,jailMinutes,jailSeconds],jailReasons,0] remoteExec ["A3PIsys_fnc_insertJail",2];
			};
		};
		if (jailHours isEqualTo 0 && jailMinutes isEqualTo 0 && jailSeconds <=0) exitWith {
			[player,"","",2] remoteExec ["A3PIsys_fnc_insertJail",2]; //--- No Jail Anymore
			A3PI_Jailed = false;
			98 cutText ["","PLAIN"]; //--- Destroy Display
		};
		sleep 1;
		((uiNamespace getVariable "RscTitle_JailTimer") displayCtrl 1100) ctrlSetStructuredText parseText format ["<t align='center' color='#a50318'>IN JAIL</t><br/><t align='center'>%1 hours, %2 minutes, %3 seconds</t><br/><t align='center' color='#f49842'>Charges: %4</t>",jailHours,jailMinutes,jailSeconds,jailReasons];
	};
};
