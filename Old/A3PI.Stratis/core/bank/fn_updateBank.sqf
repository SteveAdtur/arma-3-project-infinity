//Steve

params ["_deposit","_amount","_currentBalance","_currentCash"];

if(_deposit == 0) then {
	_currentBalance = _currentBalance + _amount;
	systemChat format["Updating Bank = %1",_currentBalance];
	[player,_currentBalance,_currentCash] remoteExec ["A3PIsys_fnc_updateBank",2];
	player setVariable["bankBalance",_currentBalance,true];
	A3PI_Bank = _currentBalance;
	call A3PI_fnc_reloadBankDialog;
	
}else{
	
	_currentBalance = _currentBalance - _amount;
	[player,_currentBalance,_currentCash] remoteExec ["A3PIsys_fnc_updateBank",2];
	player setVariable["bankBalance",_currentBalance,true];
	A3PI_Bank = _currentBalance;
	call A3PI_fnc_reloadBankDialog;
};