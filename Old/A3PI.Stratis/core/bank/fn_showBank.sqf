// Steve , Heisen


disableSerialization;

params [
	"_bankData"
];

_incomingData = _bankData select 0;
_parseData = _incomingData select 0;

systemChat format ["bankData parse = %1",_parseData];

_display = findDisplay 19875;
systemChat format ["Checking display = %1",str(_display)];
if(str(_display) == "no display") then{
	createDialog "RscDisplay_BankWindow";
	systemChat format["creating dialog + %1", 0];
	
};
player setVariable["bankBalance", _parseData,true];
_bankBalace = player getVariable "bankBalance";
_strBalance = [_bankBalace] call BIS_fnc_numberText;
systemChat format["setting data bank amount = %1",_bankBalace];
_ctrl1 = (findDisplay 19875) displayCtrl 1101;

_ctrl1 ctrlSetStructuredText parseText format["On Hand Cash: %1%2","$",([(call compile "Item_Cash")]call BIS_fnc_numberText)];

_ctrl2 = (findDisplay 19875) displayCtrl 1100;
_ctrl2 ctrlSetStructuredText parseText format["Bank Balance: %1%2","$", _strBalance];


A3PI_dataRemoteRecieved = [];