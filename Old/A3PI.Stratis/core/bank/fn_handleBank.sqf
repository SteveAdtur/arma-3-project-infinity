// Steve, Heisen


params [
	"_value",
	"_type"
];

call {
	if (_type isEqualTo "ADD") then {
		A3PI_Bank = A3PI_Bank + _value;
	};
	if (_type isEqualTo "TAKE") then {
		A3PI_Bank = A3PI_Bank - _value;
	};
};

[player,A3PI_Bank,call A3PI_fnc_getInventory] remoteExec ["A3PIsys_fnc_updateBank",2];