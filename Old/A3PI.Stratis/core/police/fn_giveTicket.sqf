/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_giveTicket.sqf
Author: Heisen http://heisen.pw
Description: Give a ticket
Parameter(s): N/A
************************************************************/

player playAction "hbm_mod_anim_ply_ticket_main";

_reason = ctrlText 1400;
_value = parseNumber(ctrlText 1401);

if !(isPlayer cursorObject) exitWith {};


if !(_value isEqualType 2) exitWith {
	[localize"STR_Notification_InvalidTicket",10,"red"] call A3PI_fnc_msg;
};


[_reason,_value,(player getVariable "CommunicationID")] remoteExec ["A3PI_fnc_recieveTicket",(cursorObject getVariable "CommunicationID")];