/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_payTicket.sqf
Author: Heisen http://heisen.pw
Description: Pay Ticket
Parameter(s): N/A
************************************************************/


if (A3PI_Bank <= A3PI_TicketRecentValue) exitWith {
	[localize"STR_Notification_NoFunds",10,"red"] call A3PI_fnc_msg;
	A3PI_TicketRecentSender = nil;
	A3PI_TicketRecentValue = nil;
};

[A3PI_TicketRecentValue,"TAKE"] call A3PI_fnc_handleBank;

[localize"STR_Notification_PaidFunds",10,"green"] call A3PI_fnc_msg;

[localize"STR_Notification_PaidTicket",10,"green"] remoteExec ["A3PI_fnc_msg",A3PI_TicketRecentSender];
A3PI_TicketRecentSender = nil;
A3PI_TicketRecentValue = nil;