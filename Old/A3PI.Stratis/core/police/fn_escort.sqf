// Heisen


params [
	"_player",
	"_type"
];


if (_type isEqualTo 0) exitWith {
	{
		_x setVariable ["A3PI_drag",false,true];
		detach _x;
	} forEach (attachedObjects player);
	player playaction "hbm_mod_anim_ply_escort_start"; 
};
if (_type isEqualTo 1) exitWith {
	if (_player getVariable "A3PI_drag") exitWith {
			[localize"STR_Notification_EscourtAlready",10,"red"] call A3PI_fnc_msg;
	};
	if (player distance _player > 3) exitWith {
			[localize"STR_Notification_TooFar",10,"red"] call A3PI_fnc_msg;
	};
	
	if !(currentWeapon player isEqualTo "") exitWith {};
	
	_player setVariable ["A3PI_drag",true,true];
	_player attachTo [player,[0,0.5,0]];
	
	player forceWalk true;
	
	player playaction "hbm_mod_anim_ply_escort_start"; 
	sleep 1;
	player playaction "hbm_mod_anim_ply_escort_loop";
};

