//Heisen

[] spawn {
	[localize"STR_Notification_BeingSearchPlayer",10,"red"] call A3PI_fnc_msg;

	private ["_holder"];

	_primaryWeapon = primaryWeapon player;
	_secondaryWeapon = handgunWeapon player;

	_primaryWeaponItems = primaryWeaponItems player;
	_secondaryWeaponItems = secondaryWeaponItems player;

	_holder = createVehicle ["groundweaponholder",getPos player,[],0,"CAN_COLLIDE"]; //--- Create ground weapon holder
	
	_holder addWeaponCargoGlobal [_primaryWeapon,1];
	_holder addWeaponCargoGlobal [_secondaryWeapon,1];
	
	player removeWeaponGlobal _primaryWeapon;
	player removeWeaponGlobal _secondaryWeapon;
	
	};