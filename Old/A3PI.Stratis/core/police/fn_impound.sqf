//STeve

_curOb = cursorObject getVariable "vehicleData";
_cursorObjectt = cursorObject;
_isDone = false;
_exit = false;
if (A3PI_antiSpam) exitWith {
	["You're already impounding..",10,"red"] call A3PI_fnc_msg;
};
A3PI_antiSpam = true;

82 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Impounding Vehicle";
		_pos = getPos player;
		
		for "_i" from 0 to 30 step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / 30));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			sleep 1;
			if (player distance _pos > 10) exitWith {
				82 cutText ["","PLAIN"];
				A3PI_antiSpam = false;
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; 
		
		["Impounding Completed",10,"green"] call A3PI_fnc_msg;
		82 cutText ["","PLAIN"]; // Remove Progress Bar
		A3PI_antiSpam = false;
[_curOb select 0,_cursorObjectt,(_curOb select 2),A3PI_Garage_11,0] remoteExec ["A3PIsys_fnc_updateVehicle",2]; // Save Vehicle
[_curOb select 0,_cursorObjectt,(_curOb select 2),nil,2] remoteExec ["A3PIsys_fnc_updateVehicle",2]; // Save Colour