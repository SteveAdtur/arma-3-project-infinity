/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_cuff.sqf
Author: Heisen http://heisen.pw
Description: Cuffed.
Parameter(s): N/A
************************************************************/


[] spawn { 
	[localize"STR_Notification_Restrained",10,"green"] call A3PI_fnc_msg;
	player setVariable["tf_unable_to_use_radio",true];
	while {(player getVariable "A3PI_Cuffed") isEqualTo 1} do
	{
		player playMove "AmovPercMstpSnonWnonDnon_Ease";
		waitUntil {(animationState player != "AmovPercMstpSnonWnonDnon_Ease")};
		if !(((player getVariable "A3PI_Cuffed") isEqualTo 1) OR ((player getVariable "A3PI_Cuffed") isEqualTo 2)) exitWith {
			[localize"STR_Notification_UnRestrained",10,"green"] call A3PI_fnc_msg;
			player switchmove "";
			player setVariable ["A3PI_cuffed",0,true];
		};
	};
	while {(player getVariable "A3PI_Cuffed") isEqualTo 2} do
	{
		sleep 0.5;
		if !((player getVariable "A3PI_Cuffed") isEqualTo 2) exitWith {
			player setVariable ["A3PI_cuffed",1,true];
		};
	};
};


//player playMovenow "Acts_AidlPsitMstpSsurWnonDnon_loop"; 