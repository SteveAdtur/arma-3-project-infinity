class Server_Vehicles
{
	rotationSpeed = 2;
	
	class localization {
		msgParamEmpty = "Shop Parameter is Empty";
		msgInVehicle =						"You cannot be in a Vehicle!";
		msgShopExists =						"Shop doesn't Exist!";
		msgCondition =						"Not permitted to access this Shop!";
		msgCashOnHand =						"Cash on Hand - %1%2";
		msgCartTotal =						"Your Cart - %1%2";
		msgInfoTooltip =					"--> HOLD YOUR LEFT MOUSE BUTTON DOWN WHILE MOVING MOUSE TO ROTATE WEAPON.\n--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgInfoTooltip2 = 					"--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgEmptyShop = 						"Nothing Found...";
		msgInfoText	=						"<t color='#FFFFFF'>Price:</t> <t color='%1'>%3%2</t>";
		msgCartFull	=						"Cart is Full";
		msgCartEmpty =						"Cart is Empty";
		msgNotEnoughCash =					"Not enough Cash for this Transaction";
		msgOverrideAlert =					"Use the override feature to override gear!";
		msgTransactionComplete =			"Purchase completed for %1%2";
		msgNotEnoughSpace =				 	"You didn't have enough space for all the items. You however only paid for those you had space for!";
		msgClear =							"Clear";
		msgSearch =							"Search";
		
		#define dialogCompleteBtn			"Purchase"
		#define dialogCloseBtn 				"Close"
	};
	
	class shops {
		
		class example_veh_shop{
			title = "Example Vehicle Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "example_veh_spawn_1";
			
			vehicles[] = {
				// ---- Veh Class Name , price, condition, custom display name
				{"Raptorlu_civ_P",500,"true",""},
				{"CHRlu_civ_bleu",500,"true",""},
				{"IVORY_R8",500,"true",""},
				{"AMG_Tahoe",500,"true",""},
				{"AlessioR8",500,"true",""},
				{"ivory_e36",500,"true",""},
				{"M_CVPI",500,"true",""},
				{"Alessio718",500,"true",""},
				{"ivory_evox_marked",500,"true",""},
				{"Mk3lu_civ",500,"true",""},
				{"Jonzie_Ambulance",500,"true",""}
			};
		};
		
	};
	
};
