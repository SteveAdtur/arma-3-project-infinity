class RscDisplay_LoginSystem
{
	idd = 19885;
	movingEnabled = false;
	
	class controls
	{
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "\A3PI_textures\dialogs\login.paa";
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.192 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.627 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "LOGIN"; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.128906 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscControl_Password: A3PIRscEdit
		{
			idc = 1400;
			text = ""; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.128906 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscControl_Username: A3PIRscEdit
		{
			idc = 1401;
			text = ""; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.445 * safezoneH + safezoneY;
			w = 0.128906 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCombo_2100: A3PIRscCombo
		{
			idc = 2100;
			text = "1"; //--- ToDo: Localize;
			x = 0.402031 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.0360937 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCombo_2101: A3PIRscCombo
		{
			idc = 2101;
			text = "1"; //--- ToDo: Localize;
			x = 0.443281 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.04125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCombo_2102: A3PIRscCombo
		{
			idc = 2102;
			text = "1"; //--- ToDo: Localize;
			x = 0.489687 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.04125 * safezoneW;
			h = 0.022 * safezoneH;
		};

	};	
	
};
