class RscDisplay_LicenseWindow
{
	idd = 19878;
	movingEnabled = false;
	class controls
	{

		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			x = 0.358973 * safezoneW + safezoneX;
			y = 0.321325 * safezoneH + safezoneY;
			w = 0.255611 * safezoneW;
			h = 0.312714 * safezoneH;
			colorBackground[] = {0,0,0,0.5};
		};
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.330729 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1002: A3PIRscText
		{
			idc = 1002;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.387153 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1003: A3PIRscText
		{
			idc = 1003;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.424769 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1004: A3PIRscText
		{
			idc = 1004;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.462384 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1005: A3PIRscText
		{
			idc = 1005;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1006: A3PIRscText
		{
			idc = 1006;
			text = ""; //--- ToDo: Localize;
			x = 0.513221 * safezoneW + safezoneX;
			y = 0.387153 * safezoneH + safezoneY;
			w = 0.096956 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1007: A3PIRscText
		{
			idc = 1007;
			text = ""; //--- ToDo: Localize;
			x = 0.513221 * safezoneW + safezoneX;
			y = 0.424769 * safezoneH + safezoneY;
			w = 0.096956 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1008: A3PIRscText
		{
			idc = 1008;
			text = ""; //--- ToDo: Localize;
			x = 0.513221 * safezoneW + safezoneX;
			y = 0.462384 * safezoneH + safezoneY;
			w = 0.096956 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1009: A3PIRscText
		{
			idc = 1009;
			text = ""; //--- ToDo: Localize;
			x = 0.513221 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.096956 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1010: A3PIRscText
		{
			idc = 1010;
			text = ""; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.54 * safezoneH + safezoneY;
			w = 0.141027 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscText_1011: A3PIRscText
		{
			idc = 1011;
			text = ""; //--- ToDo: Localize;
			x = 0.513221 * safezoneW + safezoneX;
			y = 0.54 * safezoneH + safezoneY;
			w = 0.096956 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Close License Check"; //--- ToDo: Localize;
			x = 0.398637 * safezoneW + safezoneX;
			y = 0.577616 * safezoneH + safezoneY;
			w = 0.163062 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "closeDialog 0";
		};


	};
};