class RscDisplay_Bec
{
	idd = 19882;
	movingEnabled = false;
	
	class controls
	{
	
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.55 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscListbox_1500: A3PIRscListbox
		{
			idc = 1500;
			x = 0.561875 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.134062 * safezoneW;
			h = 0.308 * safezoneH;
		};
		class RscListbox_1501: A3PIRscListbox
		{
			idc = 1501;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.134062 * safezoneW;
			h = 0.308 * safezoneH;
		};
		class RscEdit_1400: A3PIRscEdit
		{
			idc = 1400;
			style = 16;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.577 * safezoneH + safezoneY;
			w = 0.278437 * safezoneW;
			h = 0.11 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2402: A3PIRscShortcutButtoneco
		{
			idc = 2402;
			text = "Set Bank"; //--- ToDo: Localize;
			onButtonClick = "['SETBANK'] call A3PI_fnc_adminRequest";
			x = 0.618594 * safezoneW + safezoneX;
			y = 0.709 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2400: A3PIRscShortcutButtoneco
		{
			idc = 2400;
			text = "Set Cash"; //--- ToDo: Localize;
			onButtonClick = "['SETCASH'] call A3PI_fnc_adminRequest";
			x = 0.618594 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2401: A3PIRscShortcutButtoneco
		{
			idc = 2401;
			text = "Local Exec"; //--- ToDo: Localize;
			onButtonClick = "(call compile (ctrlText 1400))";
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2403: A3PIRscShortcutButtoneco
		{
			idc = 2403;
			text = "Set Health"; //--- ToDo: Localize;
			x = 0.542281 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2404: A3PIRscShortcutButtoneco
		{
			idc = 2404;
			text = "Set Thirst"; //--- ToDo: Localize;
			x = 0.542281 * safezoneW + safezoneX;
			y = 0.709 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2405: A3PIRscShortcutButtoneco
		{
			idc = 2405;
			text = "Vehicles"; //--- ToDo: Localize;
			onButtonClick = "A3PI_AdminSpawnBox = 'VEHICLES';";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2406: A3PIRscShortcutButtoneco
		{
			idc = 2406;
			text = "Weapons"; //--- ToDo: Localize;
			onButtonClick = "A3PI_AdminSpawnBox = 'WEAPONS';";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.269 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2407: A3PIRscShortcutButtoneco
		{
			idc = 2407;
			text = "Clothing"; //--- ToDo: Localize;
			onButtonClick = "A3PI_AdminSpawnBox = 'CLOTHING';";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.291 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2408: A3PIRscShortcutButtoneco
		{
			idc = 2408;
			text = "Items"; //--- ToDo: Localize;
			onButtonClick = "A3PI_AdminSpawnBox = 'ITEMS';";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.313 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2409: A3PIRscShortcutButtoneco
		{
			idc = 2409;
			text = "Spawn"; //--- ToDo: Localize;
			onButtonClick = "['SPAWN'] call A3PI_fnc_adminRequest";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.357 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		}; // 4
		class RscButtonMenu_2410: A3PIRscShortcutButtoneco
		{
			idc = 2410;
			text = "Revive"; //--- ToDo: Localize;
			onButtonClick = "['REVIVE'] call A3PI_fnc_adminRequest";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscButtonMenu_2411: A3PIRscShortcutButtoneco
		{
			idc = 2411;
			text = "Spectate"; //--- ToDo: Localize;
			onButtonClick = "['SPECTATE'] call A3PI_fnc_adminRequest";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.445 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
	
	};
};

