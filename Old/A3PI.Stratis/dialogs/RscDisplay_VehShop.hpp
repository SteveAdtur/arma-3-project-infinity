////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Steve Adtur, v1.063, #Cynece)
////////////////////////////////////////////////////////
class RscDisplay_VehShop
{
	idd = 29872;
	movingEnabled = false;
	
	class controls
	{
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.0945475 * safezoneW + safezoneX;
			y = 0.0956312 * safezoneH + safezoneY;
			w = 0.185098 * safezoneW;
			h = 0.771122 * safezoneH;
		};
		class RscListbox_1500: A3PIRscListbox
		{
			idc = 1500;
			x = 0.0989546 * safezoneW + safezoneX;
			y = 0.105035 * safezoneH + safezoneY;
			w = 0.176284 * safezoneW;
			h = 0.686487 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1600;
			text = "Purchase Vehicle"; //--- ToDo: Localize;
			x = 0.0989545 * safezoneW + safezoneX;
			y = 0.800926 * safezoneH + safezoneY;
			w = 0.0705135 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3PI_fnc_purchaseVeh";
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1601;
			text = "Exit Shop"; //--- ToDo: Localize;
			x = 0.204725 * safezoneW + safezoneX;
			y = 0.800926 * safezoneH + safezoneY;
			w = 0.0705135 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "closeDialog 0";
		};
		
	};
	
};
