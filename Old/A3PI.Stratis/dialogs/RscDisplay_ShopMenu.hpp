class RscDisplay_ShopMenu
{
	idd = 19864;
	movingEnabled = false;
	
	class controlsBackground
	{
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			x = 0.0100316 * safezoneW + safezoneX;
			y = 0.213911 * safezoneH + safezoneY;
			w = 0.201145 * safezoneW;
			h = 0.45114 * safezoneH;
			colorBackground[] = {0.102,0.102,0.102,0.75};
			//colorBackground[] = {0.102,0.102,0.102,1}; cb,cb,cb,1
		};

	};

	class controls
	{
		class RscCombo_2100: A3PIRscCombo
		{
			idc = 2100;
			onLBSelChanged = "[(lbData[2100,lbCurSel 2100])] call A3PI_fnc_newShopFillData";
			x = 0.0100316 * safezoneW + safezoneX;
			y = 0.665051 * safezoneH + safezoneY;
			w = 0.201145 * safezoneW;
			h = 0.0330102 * safezoneH;
		};
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			text = "Shop"; //--- ToDo: Localize;
			x = 0.0100316 * safezoneW + safezoneX;
			y = 0.191904 * safezoneH + safezoneY;
			w = 0.201145 * safezoneW;
			h = 0.0220068 * safezoneH;
			colorBackground[] = {0.996,0,0.078,0.75};
		};
		class RscText_1005: A3PIRscListbox
		{
			idc = 1501;
			columns[] = {0,0.5,0.7};
			disableOverflow = 1;
			x = 0.0151892 * safezoneW + safezoneX;
			y = 0.224915 * safezoneH + safezoneY;
			w = 0.19083 * safezoneW;
			h = 0.429133 * safezoneH;
			colorBackground[] = {0,0,0,0.75};
			onLBSelChanged = "call A3PI_fnc_newShopShowClass";
		};
		class RscButtonMenu_2401: A3PIRscShortcutButtoneco
		{
			idc = 2401;

			text = "Purchase"; //--- ToDo: Localize;
			onButtonClick = "[] spawn A3PI_fnc_newShopPurchaseItem";
			x = 0.0100317 * safezoneW + safezoneX;
			y = 0.69586 * safezoneH + safezoneY;
			w = 0.201145 * safezoneW;
			h = 0.0330102 * safezoneH;
		};
	};	
	
};