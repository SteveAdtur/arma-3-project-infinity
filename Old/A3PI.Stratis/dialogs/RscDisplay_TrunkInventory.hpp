class RscDisplay_TrunkInventory {
    idd = 998;
    name= "RscDisplay_TrunkInventory";
	movingEnable=true;
	onLoad = "";
	onUnLoad = "";
    class controlsBackground {
		class RscText_1000: A3PIRscText
		{
			moving = 1; 
			idc = 1000;
			x = 0.345312 * safezoneW + safezoneX;
			y = 0.335 * safezoneH + safezoneY;
			w = 0.309375 * safezoneW;
			h = 0.33 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
	};

    class controls {
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Store"; //--- ToDo: Localize;
			x = 0.530937 * safezoneW + safezoneX;
			onButtonClick = "[A3PI_VehicleInteract] spawn A3PI_fnc_storeVehicleInv;";
			y = 0.665 * safezoneH + safezoneY;
			w = 0.061875 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscEdit_1400: A3PIRscEdit
		{
			idc = 1400;
			text = "1"; //--- ToDo: Localize;
			x = 0.469062 * safezoneW + safezoneX;
			y = 0.665 * safezoneH + safezoneY;
			w = 0.061875 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Take"; //--- ToDo: Localize;
			onButtonClick = "[A3PI_VehicleInteract] spawn A3PI_fnc_takeVehicleInv;";
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.665 * safezoneH + safezoneY;
			w = 0.061875 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class inventory: A3PIRscListbox
		{
			idc = 1500;
			x = 0.505156 * safezoneW + safezoneX;
			y = 0.368 * safezoneH + safezoneY;
			w = 0.144375 * safezoneW;
			h = 0.286 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class trunk: A3PIRscListbox
		{
			idc = 1501;
			x = 0.350469 * safezoneW + safezoneX;
			y = 0.368 * safezoneH + safezoneY;
			w = 0.144375 * safezoneW;
			h = 0.286 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscText_1001: A3PIRscStructuredText
		{
			idc = 1001;
			text = "100/100"; //--- ToDo: Localize;
			x = 0.505156 * safezoneW + safezoneX;
			y = 0.346 * safezoneH + safezoneY;
			w = 0.144375 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
		class RscText_1002: A3PIRscStructuredText
		{
			idc = 1002;
			text = "100/100"; //--- ToDo: Localize;
			x = 0.350469 * safezoneW + safezoneX;
			y = 0.346 * safezoneH + safezoneY;
			w = 0.144375 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.8};
		};
    };
};

