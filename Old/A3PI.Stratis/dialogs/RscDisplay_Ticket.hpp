class RscDisplay_Ticket
{
	idd = 19876;
	movingEnabled = false;
	
	class controls
	{
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.143 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			text = "Ticket"; //--- ToDo: Localize;
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.9};
		};
		class RscText_1002: A3PIRscText
		{
			idc = 1002;
			text = "Reason:"; //--- ToDo: Localize;
			x = 0.412344 * safezoneW + safezoneX;
			y = 0.434 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.033 * safezoneH;
			colorBackground[] = {0,0,0,0.9};
		};
		class RscText_1003: A3PIRscText
		{
			idc = 1003;
			text = "Price:"; //--- ToDo: Localize;
			x = 0.412344 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.033 * safezoneH;
			colorBackground[] = {0,0,0,0.9};
		};
		class RscEdit_1400: A3PIRscEdit
		{
			idc = 1400;
			text = ""; //--- ToDo: Localize;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.434 * safezoneH + safezoneY;
			w = 0.139219 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscEdit_1401: A3PIRscEdit
		{
			idc = 1401;
			text = ""; //--- ToDo: Localize;
			x = 0.448437 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.139219 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Give Ticket"; //--- ToDo: Localize;
			onButtonClick = "[] call A3PI_fnc_giveTicket; closeDialog 0;";
			x = 0.412344 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.033 * safezoneH;
		};
	};	
};


class RscDisplay_RecieveTicket
{
	idd = 19877;
	movingEnabled = false;
	
	class controls
	{
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			x = 0.365937 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.268125 * safezoneW;
			h = 0.176 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			x = 0.365937 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.268125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.9};
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Refuse Ticket"; //--- ToDo: Localize;
			onButtonClick = "closeDialog 0;[localize'STR_Notification_RefuseTicket',10,'red'] remoteExec ['A3PI_fnc_msg',A3PI_TicketRecentSender];";
			x = 0.505156 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.118594 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Pay Ticket"; //--- ToDo: Localize;
			onButtonClick = "closeDialog 0; call A3PI_fnc_payTicket;";
			x = 0.37625 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscStructuredText_1100: A3PIRscStructuredText
		{
			idc = 1100;
			colorBackground[] = {0,0,0,0.7};
			x = 0.37625 * safezoneW + safezoneX;
			y = 0.434 * safezoneH + safezoneY;
			w = 0.2475 * safezoneW;
			h = 0.044 * safezoneH;
		};
		class RscStructuredText_1101: A3PIRscStructuredText
		{
			idc = 1101;
			colorBackground[] = {0,0,0,0.7};
			x = 0.37625 * safezoneW + safezoneX;
			y = 0.489 * safezoneH + safezoneY;
			w = 0.2475 * safezoneW;
			h = 0.044 * safezoneH;
		};
	};	
};