class RscDisplay_RobberyDialog
{
	idd = 21010;
	movingEnabled = false;
	class controls
	{
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.385416 * safezoneW + safezoneX;
			y = 0.405961 * safezoneH + safezoneY;
			w = 0.207133 * safezoneW;
			h = 0.159867 * safezoneH;
			colorText[] = {0,0,0,0.65};
			colorBackground[] = {0,0,0,0.5};
		};
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			text = "What Item to Steal"; //--- ToDo: Localize;
			x = 0.385416 * safezoneW + safezoneX;
			y = 0.377749 * safezoneH + safezoneY;
			w = 0.207133 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.95};
		};
		class RscListbox_1500: A3PIRscListbox
		{
			idc = 1500;
			x = 0.398637 * safezoneW + safezoneX;
			y = 0.415365 * safezoneH + safezoneY;
			w = 0.180691 * safezoneW;
			h = 0.0752314 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Start Robbery"; //--- ToDo: Localize;
			x = 0.403044 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0837348 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "spawn A3PI_fnc_robbery";
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Cancel Robbery"; //--- ToDo: Localize;
			x = 0.491186 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0837348 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "closeDialog 0";
		};


	};
	
};
