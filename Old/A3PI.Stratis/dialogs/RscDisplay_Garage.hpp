class RscDisplay_Garage {
    idd = 1002;
    name= "RscDisplay_Garage";
	movingEnable=true;
	onLoad = "";
	onUnLoad = "";
    class controlsBackground {
    };

    class controls {
		

		class RscDisplay_Main: A3PIRscPicture
		{
			idc = 1200;
			moving = 1; 
			text = "dialogs\data\menu_base.paa";
			x = 0.185469 * safezoneW + safezoneX;
			y = -0.171 * safezoneH + safezoneY;
			w = 0.520781 * safezoneW;
			h = 0.946 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			moving = 1; 
			text = "Retrieve Vehicle"; //--- ToDo: Localize;
			onButtonClick = "call A3PI_fnc_retrieveVehicle;";
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.786 * safezoneH + safezoneY;
			w = 0.128906 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscDisplay_Listbox: A3PIRscListbox
		{
			idc = 1500;
			moving = 1; 
			x = 0.298905 * safezoneW + safezoneX;
			y = 0.236 * safezoneH + safezoneY;
			w = 0.402187 * safezoneW;
			h = 0.528 * safezoneH;
		};

		
    };
};