class A3PI_Messages
{
		idd = -1;
		duration = 1e+012;
		onLoad = "uiNamespace setVariable [""A3PI_messages_display"", _this select 0]";
		class Controls
		{
			class messages: A3PIRscStructuredText
			{
				idc = 100;
				style = 256;
				colorText[] = {0,0,0,1};
				colorBackground[] = {0,0,0,0};
				colorShadow[] = {0.2,0.2,0.2,1};
				size = 0.04; // 0.03
				sizeEx = 0.015;
				/*
				size = 0.06;
				sizeEx = 0.03;
				*/
				x = "safeZoneX";
				y = "safeZoneY + safeZoneH*1/3";
				w = "0.6 * safeZoneW";
				h = "0.03 * 8";
				text = "Message 1<br />Message 2";
				class Attributes
				{
					font = "PuristaMedium";
					color = "#ffffff";
					align = "left";
					valign = "middle";
					shadow = 1;
					shadowColor = "#333333";
					size = 1;
				};
			};
		};
};

class A3PI_MessagesDebug
{
		idd = -1;
		duration = 1e+012;
		onLoad = "uiNamespace setVariable [""A3PI_MessagesDebug_Display"", _this select 0]";
		class Controls
		{
			class messages: A3PIRscStructuredText
			{
				idc = 100;
				style = 256;
				colorText[] = {1,0,0,1};
				colorBackground[] = {0,0,0,0};
				colorShadow[] = {0.2,0.2,0.2,1};
				size = 0.03;
				sizeEx = 0.015;
				/*
				size = 0.06;
				sizeEx = 0.03;
				*/
				x = 0.0151888 * safezoneW + safezoneX;
				y = 0.741958 * safezoneH + safezoneY;
				w = 0.598277 * safezoneW;
				h = 0.626891 * safezoneH;
				text = "Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message<br/>Message";
				class Attributes
				{
					font = "PuristaMedium";
					color = "#ffffff";
					align = "left";
					valign = "middle";
					shadow = 1;
					shadowColor = "#333333";
					size = 1;
				};
			};
		};
};

////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Heisen Beira-Mar, v1.063, #Golepi)
////////////////////////////////////////////////////////
