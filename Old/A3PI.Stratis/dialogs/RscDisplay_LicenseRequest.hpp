class RscDisplay_LicenseRequest
{
	idd = 19879;
	movingEnabled = false;
	class controls
	{
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.385416 * safezoneW + safezoneX;
			y = 0.405961 * safezoneH + safezoneY;
			w = 0.207133 * safezoneW;
			h = 0.159867 * safezoneH;
			colorText[] = {0,0,0,0.65};
			colorBackground[] = {0,0,0,0.5};
		};
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			text = "License Being Requested"; //--- ToDo: Localize;
			x = 0.385416 * safezoneW + safezoneX;
			y = 0.377749 * safezoneH + safezoneY;
			w = 0.207133 * safezoneW;
			h = 0.0282118 * safezoneH;
			colorBackground[] = {0,0,0,0.95};
		};
		class RscListbox_1500: A3PIRscListbox
		{
			idc = 1500;
			x = 0.398637 * safezoneW + safezoneX;
			y = 0.415365 * safezoneH + safezoneY;
			w = 0.180691 * safezoneW;
			h = 0.0752314 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Hand over License"; //--- ToDo: Localize;
			x = 0.403044 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0837348 * safezoneW;
			h = 0.0470196 * safezoneH;
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Refuse License"; //--- ToDo: Localize;
			x = 0.491186 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0837348 * safezoneW;
			h = 0.0470196 * safezoneH;
		};


	};
	
};
