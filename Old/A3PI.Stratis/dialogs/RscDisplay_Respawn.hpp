class RscDisplay_Respawn
{
	idd = 19885;
	movingEnabled = false;
	
	class controls
	{

		class Sikoras_button1: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Respawn"; //--- ToDo: Localize;
			x = 0.689505 * safezoneW + safezoneX;
			y = 0.697482 * safezoneH + safezoneY;
			w = 0.0793277 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3PI_fnc_RespawnNow";
		};
		
	};	
	
};