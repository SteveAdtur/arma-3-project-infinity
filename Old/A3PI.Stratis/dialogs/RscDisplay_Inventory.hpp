class RscDisplay_Inventory {
    idd = 1001;
    name= "RscDisplay_Inventory";
	movingEnable=true;
	onLoad = "";
	onUnLoad = "";
    class controlsBackground {
    };

    class controls {
		
		class RscDisplay_Background: A3PIRscPicture
		{
			idc = 1200;
			moving = 1; 
			text = "dialogs\data\menu_base.paa";
			x = 0.267969 * safezoneW + safezoneX;
			y = -0.248 * safezoneH + safezoneY;
			w = 0.386719 * safezoneW;
			h = 1.056 * safezoneH;
		};
		class RscDisplay_ListBox: A3PIRscListbox
		{
			idc = 1500;
			x = 0.354594 * safezoneW + safezoneX;
			y = 0.2008 * safezoneH + safezoneY;
			w = 0.293906 * safezoneW;
			h = 0.594 * safezoneH;
		};
		class RscDisplay_UseItem: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Use Item"; //--- ToDo: Localize;
			onButtonClick = "call A3PI_fnc_useItem";
			x = 0.345312 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscDisplay_DropItem: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Drop Item"; //--- ToDo: Localize;
			onButtonClick = "call A3PI_fnc_dropItem";
			x = 0.432969 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscDisplay_ThrowItem: A3PIRscShortcutButtoneco
		{
			idc = 1702;
			text = "Give Item"; //--- ToDo: Localize;
			onButtonClick = "call A3PI_fnc_giveItem";
			x = 0.520625 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscDisplay_UseItemAmount: A3PIRscEdit
		{
			idc = 1400;
			text = "1";
			x = 0.608281 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.0464063 * safezoneW;
			h = 0.033 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
		
    };
};