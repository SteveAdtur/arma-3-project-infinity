// Steve

params[
	"_cursorObject"
];

_cursorLicense = _cursorObject getVariable "A3L_License";
_playerName = name _cursorObject;

_driverArray = _cursorLicense select 0;
_truckArray = _cursorLicense select 1;
_pilotArray = _cursorLicense select 2;
_firearmArray = _cursorLicense select 3;
_rifleArray = _cursorLicense select 4;

_driver = _driverArray select 0;
_driverCount = _driverArray select 1;
_truck = _truckArray select 0;
_truckCount = _truckArray select 1;
_pilot = _pilotArray select 0;
_pilotCount = _pilotArray select 1;
_firearm = _firearmArray select 0;
_firearmCount = _firearmArray select 1;
_rifle = _rifleArray select 0;
_rifleCount = _rifleArray select 1;
createDialog "RscDisplay_LicenseWindow";
_ctrlName = (findDisplay 19878) displayCtrl 1001;
_ctrlName ctrlSetText _playerName;


if(_driverCount > 0)then{
	ctrl1 = (findDisplay 19878) displayCtrl 1002;
	ctrl1 ctrlSetText localize _driver;
	ctrl2 = (findDisplay 19878) displayCtrl 1006;
	if(_driverCount == 1)then{
		ctrl2 ctrlSetText "Valid";
	}else{
		ctrl2 ctrlSetText "Suspended";
	};
}else{
	
	ctrl1 = (findDisplay 19878) displayCtrl 1002;
	ctrl1 ctrlSetText "";
	ctrl2 = (findDisplay 19878) displayCtrl 1006;
	ctrl2 ctrlSetText "";
};

if(_truckCount > 0)then{
	ctrl1 = (findDisplay 19878) displayCtrl 1003;
	ctrl1 ctrlSetText localize _truck;
	ctrl2 = (findDisplay 19878) displayCtrl 1007;
	if(_truckCount == 1)then{
		ctrl2 ctrlSetText "Valid";
	}else{
		ctrl2 ctrlSetText "Suspended";
	};
}else{
	ctrl1 = (findDisplay 19878) displayCtrl 1003;
	ctrl1 ctrlSetText "";
	ctrl2 = (findDisplay 19878) displayCtrl 1007;
	ctrl2 ctrlSetText "";
};
if(_pilotCount > 0)then{
	ctrl1 = (findDisplay 19878) displayCtrl 1004;
	ctrl1 ctrlSetText localize _pilot;
	ctrl2 = (findDisplay 19878) displayCtrl 1008;
	if(_pilotCount == 1)then{
		ctrl2 ctrlSetText "Valid";
	}else{
		ctrl2 ctrlSetText "Suspended";
	};
}else{
	ctrl1 = (findDisplay 19878) displayCtrl 1004;
	ctrl1 ctrlSetText "";
	ctrl2 = (findDisplay 19878) displayCtrl 1008;
	ctrl2 ctrlSetText "";
};
if(_firearmCount > 0)then{
	ctrl1 = (findDisplay 19878) displayCtrl 1005;
	ctrl1 ctrlSetText localize _firearm;
	ctrl2 = (findDisplay 19878) displayCtrl 1009;
	if(_firearmCount == 1)then{
		ctrl2 ctrlSetText "Valid";
	}else{
		ctrl2 ctrlSetText "Suspended";
	};
}else{
	ctrl1 = (findDisplay 19878) displayCtrl 1005;
	ctrl1 ctrlSetText "";
	ctrl2 = (findDisplay 19878) displayCtrl 1009;
	ctrl2 ctrlSetText "";
};
if(_rifleCount > 0)then{
	ctrl1 = (findDisplay 19878) displayCtrl 1010;
	ctrl1 ctrlSetText localize _rifle;
	ctrl2 = (findDisplay 19878) displayCtrl 1011;
	if(_rifleCount == 1)then{
		ctrl2 ctrlSetText "Valid";
	}else{
		ctrl2 ctrlSetText "Suspended";
	};
}else{
	ctrl1 = (findDisplay 19878) displayCtrl 1005;
	ctrl1 ctrlSetText "";
	ctrl2 = (findDisplay 19878) displayCtrl 1009;
	ctrl2 ctrlSetText "";
};

