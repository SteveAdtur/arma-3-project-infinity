//Steve
private ["_countNum"];
player allowDamage false;
player enableSimulation false;
_countNum = -1;

createDialog "RscDisplay_SpawnWindow";

_spawnPoints = getArray (missionConfigFile >> "Server_Settings" >> "Player_Spawns" >> "spawnPoints");

if(A3L_Jailed)then{
	_countNum = _countNum +1;
	lbAdd[1500, "Jail"];
	lbSetData[1500, _countNum, "Jail"];
}else{

	{
		_countNum = _countNum +1;
		lbAdd [1500, _x];
		lbSetData[1500,_countNum,_x];
		
	}forEach _spawnPoints;
};