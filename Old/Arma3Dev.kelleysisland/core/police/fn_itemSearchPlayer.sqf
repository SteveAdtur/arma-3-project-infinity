//Steve

params[
	"_player"
];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

_itemOut = [];
{

	_varName = configName _x;
	_varAmount = call compile _varName;
	
	if(_varAmount >= 1) then {
	
		_itemOut pushBack [_varName,_varAmount];
	
	};

} forEach _items;

[_itemOut] remoteExec ['A3L_fnc_itemSearch',_player getVariable 'CommunicationID'];