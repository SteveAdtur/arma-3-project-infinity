/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_cuffPerson.sqf
Author: Heisen http://heisen.pw
Description: Cuff Action.
Parameter(s): OBJECT-PLAYER
************************************************************/


params [
	"_cursorObject"
];

if !(isPlayer _cursorObject) exitWith {};

if !(((call compile "Item_Handcuff_Normal") >= 1) OR ((call compile "Item_handcuff_Kinky") >= 1) OR ((call compile "Item_Zipties") >= 1)) exitWith {
	[localize"STR_Notification_NoZipTies",10,"red"] call A3L_fnc_msg;
};

if (((_cursorObject getVariable "A3L_cuffed") isEqualTo 1) OR ((_cursorObject getVariable "A3L_cuffed") isEqualTo 2)) exitWith { 
	[localize"STR_Notification_AlreadyRestrained",10,"red"] call A3L_fnc_msg;
};

// IF DOWNED IN REVIVE MODE DONT ALLOW.... NEED TO DO THIS

[localize"STR_Notification_PersonRestrained",10,"green"] call A3L_fnc_msg;

_cursorObject setVariable ["A3L_cuffed",1,true];

[] remoteExec ["A3L_fnc_cuff",_cursorObject getVariable "CommunicationID"]; 

call {
	if !((call compile "Item_Handcuff_Normal") < 1) exitWith {
		["Item_Handcuff_Normal",1,false] call A3L_fnc_handleitem;
	};
	if !((call compile "Item_handcuff_Kinky") < 1) exitWith {
		["Item_handcuff_Kinky",1,false] call A3L_fnc_handleitem;
	};
	if !((call compile "Item_Zipties") < 1) exitWith {
		["Item_Zipties",1,false] call A3L_fnc_handleitem;
	};
};

