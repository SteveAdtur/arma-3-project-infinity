//Steve

params[
	"_items"
];

_playerItems = _items;
_illegalItems = [];
_hintStr = "";

{
	_isIllegal = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Illegal" );
	if(_x IN _isIllegal) then {
		_illegalItems pushBack _x;
	};

}forEach _playerItems;

{
_outputStr = format[" + %1", _x select 0];
_hintStr = _hintStr + _outputStr;
[_x select 0,_x select 1, false] call A3L_fnc_handleItem;
}forEach _illegalItems;

systemChat _hintStr;
