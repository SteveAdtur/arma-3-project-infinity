// Steve, Heisen


params [
	"_value",
	"_type"
];

call {
	if (_type isEqualTo "ADD") then {
		A3L_Bank = A3L_Bank + _value;
	};
	if (_type isEqualTo "TAKE") then {
		A3L_Bank = A3L_Bank - _value;
	};
};

[player,A3L_Bank,call A3L_fnc_getInventory] remoteExec ["A3Lsys_fnc_updateBank",2];