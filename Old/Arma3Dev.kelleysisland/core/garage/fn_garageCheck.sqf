/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_garageCheck.sqf
Author: Heisen http://heisen.pw
Description: Check if Garage or Not!
Parameter(s): N/A
************************************************************/


params [
	"_curObject"
];

_garages = getArray (missionConfigFile >> "Server_Settings" >> "Player_Garages" >> "garages");

if ((str(_curObject)) IN _garages) then {
	_sum = true;
	_sum;
} else {
	_sum = false;
	_sum;
};
