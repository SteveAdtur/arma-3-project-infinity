/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_openGarage.sqf
Author: Heisen http://heisen.pw
Description: Open Garage.
Parameter(s): N/A
************************************************************/


private ["_thisGarage"];

params [
	"_garageData"
];

createDialog "RscDisplay_Garage"; 

{
	_isActive = _x select 9;
	if (_isActive isEqualTo 0) then {
		_garage = _x select 11;
		_near = player distance getPos(call compile _garage);
		if (_near < 5) then {
			_thisGarage = true;
			lbAdd [1500,format["%1 - %2 - This Garage",getText(configFile >> "cfgVehicles" >> _x select 2 >> "displayName"),_x select 3]];
			lbSetData [1500,_forEachIndex,str([_x select 2,_x select 3,_thisGarage,_garage,_x select 5,_x select 4])];
		} else {
			lbAdd [1500,format["%1 - %2 - Other Garage",getText(configFile >> "cfgVehicles" >> _x select 2 >> "displayName"),_x select 3]];
			_thisGarage = false;
		};
	} else {
		_thisGarage = false;
		lbAdd [1500,format["%1 - %2 - OUT",getText(configFile >> "cfgVehicles" >> _x select 2 >> "displayName"),_x select 3]];
	};
} forEach _garageData;

//--- Put make to orginal no value within array.. not needed till the next request...
A3L_dataRemoteRecieved = [];