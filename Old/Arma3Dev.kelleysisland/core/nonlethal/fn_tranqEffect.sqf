//Steve


if (player getVariable "A3L_Tazed") exitWith {};
player setVariable ["A3L_Tazed",true,true];



sleep 30;
call KK_fnc_forceRagdoll;
disableUserInput true;
[localize"STR_Notification_Tranq",10,"red"] call A3L_fnc_msg;

sleep 60;
player setVariable ["A3L_Tazed",false,true];
disableUserInput false;
[localize"STR_Notification_TranqRecovered",10,"green"] call A3L_fnc_msg;
