/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_remoteRecieved.sqf
Author: Heisen http://heisen.pw
Description: Pass through A3L_dataRemoteRecieved var data.
Parameter(s): N/A
************************************************************/


params [
	"_packet"
];

A3L_dataRemoteRecieved = _packet;

diag_log format ["A3L Client REMOTE RECIEVED new PACKET(%1)",_packet];