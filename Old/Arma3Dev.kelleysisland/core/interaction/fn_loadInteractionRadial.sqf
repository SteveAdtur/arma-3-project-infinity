/**
*
*	Author: MrBoolean
*	Website: http://mrboolean.io
*	Name: fn_loadInteraction.sqf
*	Description: Loads the interaction menu and populates it with datazzzzz
*
**/
disableSerialization;

params[["_target",objNull,[objNull]]];

MB_Interaction_Target = _target;
MB_actionList = [];
MB_overflowList = [];
/* Load all our interaction options */

_interactionOptions = (missionConfigFile >> "cfgInteractions") call BIS_fnc_getCfgSubClasses;
{

	if(count MB_actionList >= 7) then {

		_check = (missionConfigFile >> "cfgInteractions" >> _x >> "check") call BIS_fnc_getCfgData;

		if((call compile _check)) then {
			_action = (missionConfigFile >> "cfgInteractions" >> _x >> "action") call BIS_fnc_getCfgData;
			_title = (missionConfigFile >> "cfgInteractions" >> _x >> "title") call BIS_fnc_getCfgData;

			MB_overflowList pushBack [_title,_action];
		};
	} else {

		_check = (missionConfigFile >> "cfgInteractions" >> _x >> "check") call BIS_fnc_getCfgData;

		if((call compile _check)) then {
			_action = (missionConfigFile >> "cfgInteractions" >> _x >> "action") call BIS_fnc_getCfgData;
			_title = (missionConfigFile >> "cfgInteractions" >> _x >> "title") call BIS_fnc_getCfgData;

			MB_actionList pushBack [_title,_action];
		};
	};

} forEach _interactionOptions;

/* Check for More Options */
if(count MB_overflowList > 0) then {
	if(count MB_overflowList < 2) then {
		MB_actionList pushBack [(MB_overflowList select 0 select 0),(MB_overflowList select 0 select 1)];
	} else {
		MB_actionList pushBack ["More Options","[] call A3L_fnc_loadMoreInteractions;"];
	};
};

//Only load if we've got interactions! Dumbass
if(count MB_actionList < 1) exitWith {};

//Load our UI

closeDialog 0;
createDialog "MB_Interaction_Menu";

_idd = 1001;

{
	ctrlSetText[_idd,(_x select 0)];
	_idd = _idd + 1;
} forEach MB_actionList;
