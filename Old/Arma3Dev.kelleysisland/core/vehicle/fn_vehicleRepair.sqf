//Steve

params[
	"_curOb",
	"_wheel"
	];
	
_tyreCount = call compile "Item_SpareWheel";
	
if(_tyreCount == 0)exitWith{
[localize"STR_Notification_NoTyre",10,"red"]call A3L_fnc_msg;
};
	
	
if(_wheel == "FL")exitWith{
//Front Left
	["Item_SpareWheel",1,false] call A3L_fnc_handleItem;
	if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
	_curOb setHit ["wheel_1_1_steering",0];
	[localize"STR_Notification_Repaired",10,"red"]call A3L_fnc_msg;
};
if(_wheel == "FR")exitWith{
//Front Right
	["Item_SpareWheel",1,false] call A3L_fnc_handleItem;
	if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
	_curOb setHit ["wheel_2_1_steering",0];
	[localize"STR_Notification_Repaired",10,"red"]call A3L_fnc_msg;

};
if(_wheel == "BL")exitWith{
//Back Left
	["Item_SpareWheel",1,false] call A3L_fnc_handleItem;
	if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
	_curOb setHit ["wheel_1_2_steering",0];
	[localize"STR_Notification_Repaired",10,"red"]call A3L_fnc_msg;
};
if(_wheel == "BR")exitWith{
//Back Right
	["Item_SpareWheel",1,false] call A3L_fnc_handleItem;
	if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
	_curOb setHit ["wheel_2_2_steering",0];
	[localize"STR_Notification_Repaired",10,"red"]call A3L_fnc_msg;
};