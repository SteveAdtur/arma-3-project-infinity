// Steve Adtur
_itemNumber = lbCurSel 1501;
_itemName = lbData [1501,_itemNumber];
	

_cost = getNumber(missionConfigFile >> "Server_Items" >> _itemName >> "buyValue");
_isPhysical = cursorObject getVariable 'isPhysical';
if!(_isPhysical)then {

_sellAmount = parseNumber(ctrlText 1401);
if(player getVariable "A3L_CopOnDuty")then{_cost = _cost/2};
_varAmount = call compile "Item_Cash";

if !(_sellAmount >= 1) exitWith {["0 Selected!",10,"red"] call A3L_fnc_msg;};
_finalcost = _cost * _sellAmount;
if !([_itemName,_sellamount] call A3L_fnc_calculateWeight) then {

	if(_varAmount >= _finalcost)then{
		[_itemName, _sellAmount, true] call A3L_fnc_handleItem;
		["Item_Cash", _finalcost, false] call A3L_fnc_handleItem;
		if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
		[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
	}
	else{
		[localize"STR_Notification_NoMoney",10,"red"] call A3L_fnc_msg;
	};
}else{
	[localize"STR_Notification_NoRoom", 10, "red"] call A3L_fnc_msg;
};
call A3L_fnc_refreshShopDialog;
}else{
	_sellAmount = parseNumber(ctrlText 1401);
	if(player getVariable "A3L_CopOnDuty")then{_cost = _cost/2};
	_varAmount = call compile "Item_Cash";
	_finalcost = _cost * _sellAmount;
	if !(_sellAmount >=1) exitWith {["0 Selected!",10,"red"] call A3L_fnc_msg;};
	if(_varAmount >= _finalcost)then{
	_class = getText(missionConfigFile >> "Server_Items" >> _itemName >> "physicalClass");
	_type = getText(missionConfigFile >> "Server_Items" >> _itemName >> "physicalType");

	_finalcost = _cost * _sellAmount;
		if(_type == "Rifle") exitWith {
			_pweap = primaryWeapon player;
			
			if(_pweap == "")then{
				player addWeapon _class;
				["Item_Cash",  _cost, false] call A3L_fnc_handleItem;
				[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
			}else{
				[localize"STR_Notification_Rifle", 10, "red"] call A3L_fnc_msg;
			};
		};
		if(_type == "Pistol") exitWith {
			_pweap = handgunWeapon player;
		
			if(_pweap == "")then{
				player addWeapon _class;
				["Item_Cash",  _cost, false] call A3L_fnc_handleItem;
				[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
			}else{
				[localize"STR_Notification_Pistol", 10, "red"] call A3L_fnc_msg;
			};
		};
		if(_type == "Mag") exitWith {
			for[{_i = 0},{_i<_sellAmount},{_i = _i+1}] do{
				player addMagazine _class;
			};
			["Item_Cash",  _finalcost, false] call A3L_fnc_handleItem;
			[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
		};
		if(_type == "Cloth") exitWith {
			_clothing = getText(missionConfigFile >> "Server_Items" >> _itemName >> "clothingType");
			if(_clothing == "Uniform")exitWith{
				_uni = uniform player;
				if(_uni == "")then{
					player forceAddUniform _class;
					["Item_Cash", _cost, false] call A3L_fnc_handleItem;
					[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
				}else{
					[localize"STR_Notification_Uniform", 10, "red"] call A3L_fnc_msg;
				};
			};
			if(_clothing == "Vest") exitWith{
				_uni = vest player;
				if(_uni == "")then{
					player addVest _class;
					["Item_Cash", _cost, false] call A3L_fnc_handleItem;
					[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
				}else{
					[localize"STR_Notification_Vest", 10, "red"] call A3L_fnc_msg;
				};
			};
			if(_clothing == "Backpack") exitWith {
				_uni = backpack player;
				if(_uni == "")then{
					player addBackpack _class;
					["Item_Cash", _cost, false] call A3L_fnc_handleItem;
					[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
				}else{
					[localize"STR_Notification_Backpack", 10, "red"] call A3L_fnc_msg;
				};
			};
			if(_clothing == "Item") exitWith{
				player linkItem _class;
				["Item_Cash", _cost, false] call A3L_fnc_handleItem;
				[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
			};
			if(_clothing == "Tools") exitWith {
				player addItem _class;
				["Item_Cash",_cost,false] call A3L_fnc_handleItem;
				[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
			};
			if(_class == "Item_Binoc") exitWith {
				player addWeapon _class;
				["Item_Cash",_cost,false] call A3L_fnc_handleItem;
				[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
			};
		};
	if !([_itemName,_sellamount] call A3L_fnc_calculateWeight) then {

		if(_varAmount >= _finalcost)then{
			[_itemName, _sellAmount, true] call A3L_fnc_handleItem;
			["Item_Cash", _finalcost, false] call A3L_fnc_handleItem;
			[localize"STR_Notification_Purchased", 10, "green"] call A3L_fnc_msg;
		}else{
			[localize"STR_Notification_NoMoney",10,"red"] call A3L_fnc_msg;
		};
	}else{
		[localize"STR_Notification_NoRoom", 10, "red"] call A3L_fnc_msg;
	};
	}else{
		[localize"STR_Notification_NoMoney", 10, "red"] call A3L_fnc_msg;
	};
};