// Heisen


params [
	"_type"
];
systemChat format ["type passed: %1",_type];

lbClear 1501;

private [
	"_count",
	"_typeWhitelist"
];
_count = -1;

switch (A3L_CurrentShop_Interact) do {
	case "Police": {
		_typeWhitelist = "A3L_CopLevel";
	};
	case "Medic": {
		_typeWhitelist = "A3L_MedicLevel";
	};
	default {
		_typeWhitelist = "0";
	};
};

_items = getArray(missionConfigFile >> "Server_Items" >> (format["Items_Shops_%1",A3L_CurrentShop_Interact]));
{
	private ["_displayName"];
			
	_missionConfigFile = (missionConfigFile >> "Server_Items" >> _x);
	_displayName = getText (_missionConfigFile >> "displayName");
	_physicalClassName = getText (_missionConfigFile >> "physicalClassName");
	_physicalClassType = getText (_missionConfigFile >> "physicalClassType");
	_physicalClassSpecificBracket = getText (_missionConfigFile >> "physicalClassSpecificBracket");
			
	_whitelistedLevel = getNumber (_missionConfigFile >> "whitelistedLevel");
	systemChat format ["Item: %1 | Class: %2 | Type: %3 | Level: %4 | Bracket: %5 %6",_displayName,_physicalClassName,_physicalClassType,_whitelistedLevel,_physicalClassSpecificBracket,_type];
	if ((_physicalClassSpecificBracket isEqualTo _type) OR (_type isEqualTo "All")) then {

		if ((call compile _typeWhitelist) >= _whitelistedLevel) then {
			_count = _count + 1;
						
			if (_displayName isEqualTo "") then {
				_displayName = getText (configFile >> (getText(_missionConfigFile >> "physicalClassType")) >> _physicalClassName >> "displayName");
			};
			_price = format ["%1%2","$",((getNumber (missionConfigFile >> "Server_Items" >> _x >> "buyValue"))*A3L_Tax)];
			_formatted = format ["%1 %2",_displayName,_price];
			lbAdd [1501,_formatted];
			lbSetData [1501,_count,_x];
		};
	};
} forEach _items;