class RscDisplay_ShopUse
{
	idd = 19854;
	movingEnabled = false;
    class controlsBackground {
		class RscPicture_1200: A3LRscPicture
		{
			idc = 1200;
			text = "\Life_Client\Images\additional\menu_base.paa";
			x = 0.283438 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.360937 * safezoneW;
			h = 0.418 * safezoneH;
		};
    };
	class controls 
	{
		class RscPicture_1200: A3LRscPicture
		{
			idc = 1200;
			text = "\Life_Client\Images\additional\menu_base.paa";
			x = 0.283438 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.360937 * safezoneW;
			h = 0.418 * safezoneH;
		};
		class RscShortcutButton_1700: A3LRscShortcutButtoneco
		{
			idc = 1700;
			text = "Use Store"; //--- ToDo: Localize;
			x = 0.360781 * safezoneW + safezoneX;
			y = 0.599 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscShortcutButton_1701: A3LRscShortcutButtoneco
		{
			idc = 1701;
			text = "Add Stock"; //--- ToDo: Localize;
			x = 0.453594 * safezoneW + safezoneX;
			y = 0.599 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscShortcutButton_1702: A3LRscShortcutButtoneco
		{
			idc = 1702;
			text = "Buy Store"; //--- ToDo: Localize;
			x = 0.546406 * safezoneW + safezoneX;
			y = 0.599 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscStructuredText_1102: A3LRscStructuredText
		{
			idc = 1102;
			text = "Stock Value: 1000"; //--- ToDo: Localize;
			x = 0.365937 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.257813 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0};
		};
		class RscStructuredText_1100: A3LRscStructuredText
		{
			idc = 1100;
			text = "Extra Value: 1000"; //--- ToDo: Localize;
			x = 0.365938 * safezoneW + safezoneX;
			y = 0.445 * safezoneH + safezoneY;
			w = 0.257813 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0};
		};
	};
};



