class RscDisplay_SpawnWindow
{
	idd = 19881;
	movingEnabled = false;
	
	class controls
	{

		class RscText_1000: A3LRscText
		{
			idc = 1000;
			x = 0.39423 * safezoneW + safezoneX;
			y = 0.208478 * safezoneH + safezoneY;
			w = 0.215948 * safezoneW;
			h = 0.451388 * safezoneH;
			colorBackground[] = {0,0,0,0.5};
		};
		class RscListbox_1500: A3LRscListbox
		{
			idc = 1500;
			x = 0.407451 * safezoneW + safezoneX;
			y = 0.264902 * safezoneH + safezoneY;
			w = 0.189505 * safezoneW;
			h = 0.197482 * safezoneH;
		};
		class RscShortcutButton_1700: A3LRscShortcutButtoneco
		{
			idc = 1600;
			text = "Spawn"; //--- ToDo: Localize;
			x = 0.433894 * safezoneW + safezoneX;
			y = 0.537616 * safezoneH + safezoneY;
			w = 0.132213 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3L_fnc_selectSpawn";
		};


	};
		
};

