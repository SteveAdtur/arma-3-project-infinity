class RscDisplay_PhoneMessageMenu
{
	idd = 19844;
	movingEnabled = false;
	
	class controls
	{
		class RscPicture_1200: A3LRscPicture
		{
			idc = 1200;
			text = "\A3L_Textures\Dialogs\Phone\base.paa";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.17 * safezoneH + safezoneY;
			w = 0.391875 * safezoneW;
			h = 0.693 * safezoneH;
		};
		class RscPicture_1201: A3LRscPicture
		{
			idc = 1201;
			text = "\A3L_Textures\Dialogs\Phone\frame.paa";
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.17 * safezoneH + safezoneY;
			w = 0.391875 * safezoneW;
			h = 0.693 * safezoneH;
		};
		class batpic: A3LRscPicture
		{
			idc = 1202;
			text = "\A3L_Textures\Dialogs\Phone\0bat.paa";
			x = 0.555688 * safezoneW + safezoneX;
			y = 0.258 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.011 * safezoneH;
		};
		class RscStructuredText_1102: A3LRscStructuredText
		{
			idc = 1102;
			text = "100%"; //--- ToDo: Localize;
			x = 0.422656 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.149531 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0};
		};
		class RscListbox_1500: A3LRscListbox
		{
			idc = 1500;
			x = 0.438125 * safezoneW + safezoneX;
			y = 0.335 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.099 * safezoneH;
		};
		class RscButton_1600: A3LRscButton
		{
			idc = 1600;
			x = 0.437094 * safezoneW + safezoneX;
			y = 0.4406 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.088 * safezoneH;
		};
		class RscButton_1601: A3LRscButton
		{
			idc = 1601;
			text = "Send"; //--- ToDo: Localize;
			x = 0.533001 * safezoneW + safezoneX;
			y = 0.676 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscEdit_1400: A3LRscEdit
		{
			idc = 1400;
			x = 0.438125 * safezoneW + safezoneX;
			y = 0.5748 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.099 * safezoneH;
		};
		class RscEdit_1401: A3LRscEdit
		{
			idc = 1401;
			x = 0.435032 * safezoneW + safezoneX;
			y = 0.676 * safezoneH + safezoneY;
			w = 0.0979687 * safezoneW;
			h = 0.022 * safezoneH;
		};
	};	
};