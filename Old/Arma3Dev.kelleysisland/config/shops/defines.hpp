#define CADET 1
#define DEPUTY 2
#define CORPORAL 3
#define SERGEANT 4
#define LIEUTENANT 5
#define CAPTAIN 6
#define SERT 7
#define SERTCOMMAND 8
#define UNDERSHERIFF 9
#define SHERIFF 10
#define FBI 11
#define FBICOMMAND 12


#define UNIFORM_TYPE "cfgWeapons"
#define WEAPON_TYPE "cfgWeapons"
#define MAGAZINE_TYPE "cfgMagazines"
#define HEADGEARLINKITEM "linkItem"


#define ITEMSCLASS "Items"
#define UNIFORMS "Uniforms"
#define VESTS "Vests"
#define BACKPACKS "Backpacks"
#define HEADGEARCLASS "Headgear"
#define WEAPONSMAGAZINES "Weapons/Magazines"