/*--------------------------------------------------------------------------
    Author:		Maverick Applications
    Website:	https://maverick-applications.com

    You're not allowed to use this file without permission from the author!
---------------------------------------------------------------------------*/

// Configuration file for the weaponshop

class maverick_weaponshop_cfg {

	cashVar = "['Item_Cash'] call A3PI_fnc_checkItem";		//--- Cash Variable
	cashSymbol = "$"; 									//--- Currency Symbol
	rotationSpeed = 2; 									//--- Rotation speed for item in preview (1 - 10)
	saveFunction = "[] call SOCK_fnc_updateRequest"; 	//--- Function to save gear etc. once items are purchased

	class localization {
		//--- localization for hints etc.
		msgParamEmpty =						"Shop Parameter is empty!";
		msgInVehicle =						"You cannot be in a Vehicle!";
		msgShopExists =						"Shop doesn't Exist!";
		msgCondition =						"Not permitted to access this Shop!";
		msgCashOnHand =						"Cash on Hand - %1%2";
		msgCartTotal =						"Your Cart - %1%2";
		msgInfoTooltip =					"--> HOLD YOUR LEFT MOUSE BUTTON DOWN WHILE MOVING MOUSE TO ROTATE WEAPON.\n--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgInfoTooltip2 = 					"--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgEmptyShop = 						"Nothing Found...";
		msgInfoText	=						"<t color='#FFFFFF'>Price:</t> <t color='%1'>%3%2</t>";
		msgCartFull	=						"Cart is Full";
		msgCartEmpty =						"Cart is Empty";
		msgNotEnoughCash =					"Not enough Cash for this Transaction";
		msgOverrideAlert =					"Use the override feature to override gear!";
		msgTransactionComplete =			"Purchase completed for %1%2";
		msgNotEnoughSpace =				 	"You didn't have enough space for all the items. You however only paid for those you had space for!";
		msgClear =							"Clear";
		msgSearch =							"Search";

		//--- localization for dialogs
		#define dialogTabWeapon				"Weapon"
		#define dialogTabMagazines			"Magazines"
		#define dialogTabAttachments		"Attachments"
		#define dialogTabOther				"Other"
		#define dialogAddBtn				"Add"
		#define dialogOverrideTooltip		"Override Gear"
		#define dialogCompleteBtn			"Complete"
		#define dialogCloseBtn 				"Close"
	};

		class shops {
			class civcloth_shop {
			title = "Clothing Shop";
			condition = "true";
			simple = 0;
			maxCart = 20;
			shopType = "civ";
			
			weapons[] = {};
			
			magazines[] = {};
			
			attachments[] = {};
			
			items[] = {
				{"janp2_batman_1", 200, "true", ""},
				{"janp2_bobmarley_1", 200, "true", ""},
				{"janp2_blackhawks_1", 200, "true", ""},
				{"U_C_Poloshirt_blue", 300, "true", ""},
				{"U_C_Poloshirt_burgundy", 300, "true", ""},
				{"U_C_Poloshirt_redwhite", 300, "true", ""},
				{"U_C_Poloshirt_salmon", 300, "true", ""},
				{"U_C_Poloshirt_stripped", 300, "true", ""},
				{"U_C_Poloshirt_tricolour", 300, "true", ""},
				{"janp2_redwings_1", 200, "true", ""},
				{"janp2_elvis_1", 200, "true", ""},
				{"janp2_gnr_1", 200, "true", ""},
				{"janp2_gnr2_1", 200, "true", ""},
				{"janp2_mj_1", 200, "true", ""},
				{"janp2_nirvana_1", 200, "true", ""},
				{"janp2_patriots_1", 200, "true", ""},
				{"janp2_pokemon_1", 200, "true", ""},
				{"janp2_pokemon2_1", 200, "true", ""},
				{"janp2_pokemon3_1", 200, "true", ""},
				{"janp2_pokemon4_1", 200, "true", ""},
				{"janp_ralph_1", 500, "true", ""},
				{"janp_ralph5_1", 500, "true", ""},
				{"U_C_man_sport_2_F", 200, "true", ""},
				{"U_C_Man_casual_6_F", 200, "true", ""},
				{"U_C_Man_casual_4_F", 200, "true", ""},
				{"U_C_Man_casual_5_F", 200, "true", ""},
				{"janp2_superman_1", 200, "true", ""},
				{"janp2_mapleleafs_1", 200, "true", ""},
				{"U_C_Man_casual_6_F", 200, "true", ""},
				{"U_C_Man_casual_6_F", 200, "true", ""},
				{"janp2_vape_1", 200, "true", ""},
				{"janp2_vape2_1", 200, "true", ""},
				{"janp2_zlatan_1", 200, "true", ""},
				{"TRYK_shirts_DENIM_BL", 600, "true", ""},
				{"TRYK_shirts_DENIM_BWH", 600, "true", ""},
				{"TRYK_shirts_DENIM_R", 600, "true", ""},
				{"TRYK_shirts_DENIM_od", 600, "true", ""},
				{"TRYK_shirts_DENIM_RED2", 600, "true", ""},
				{"TRYK_shirts_DENIM_ylb", 600, "true", ""},
				{"U_C_Journalist", 600, "true", ""},
				{"EF_FEM_3_2", 300, "true", ""},
				{"EF_FEM_3_9B2", 600, "true", ""},
				{"EF_FEM_3_9W", 600, "true", ""},
				{"EF_FEM_3_9W2", 600, "true", ""},
				{"EF_FEM_3A_2", 600, "true", ""},
				{"EF_FEM_4", 600, "true", ""},
				{"V_TacVest_blk", 700, "true", ""},
				{"TRYK_V_Bulletproof_BLK", 900, "true", ""},
				{"B_Carryall_cbr", 600, "true", ""},
				{"TRYK_B_Carryall_blk", 600, "true", ""},
				{"B_Carryall_ocamo", 600, "true", ""},
				{"B_Carryall_mcamo", 600, "true", ""},
				{"B_Carryall_oli", 600, "true", ""},
				{"B_Carryall_oucamo", 600, "true", ""},
				{"B_FieldPack_blk", 400, "true", ""},
				{"B_FieldPack_cbr", 400, "true", ""},
				{"B_FieldPack_ocamo", 400, "true", ""},
				{"B_FieldPack_khk", 400, "true", ""},
				{"B_FieldPack_oli", 400, "true", ""},
				{"B_FieldPack_oucamo", 400, "true", ""},
				{"B_Kitbag_cbr", 500, "true", ""},
				{"TRYK_B_Kitbag_blk", 500, "true", ""},
				{"B_Kitbag_rgr", 500, "true", ""},
				{"B_Kitbag_mcamo", 500, "true", ""},
				{"B_Kitbag_sgg", 500, "true", ""},
				{"EF_FBAG_BK", 500, "true", ""},
				{"EF_FBAG_RD", 500, "true", ""},
				{"H_Booniehat_khk", 90, "true", ""},
				{"H_Booniehat_mcamo", 90, "true", ""},
				{"H_Booniehat_oli", 90, "true", ""},
				{"H_Booniehat_tan", 90, "true", ""},
				{"H_Cap_press", 90, "true", ""},
				{"H_Cap_red", 90, "true", ""},
				{"H_Cap_tan", 90, "true", ""},
				{"H_Cap_usblack", 90, "true", ""},
				{"TRYK_H_woolhat", 90, "true", ""},
				{"TRYK_H_woolhat_WH", 90, "true", ""},
				{"EF_PGlass2_NV", 800, "true", ""},
				{"G_Spectacles_Tinted", 50, "true", ""},
				{"G_Squares_Tinted", 50, "true", ""},
				{"G_Aviator", 50, "true", ""}
			};
		};
	
	
		class civsuit_shop {
			title = "Suit Shop";
			condition = "true";
			simple = 0;
			maxCart = 20;
			shopType = "civ";
			
			weapons[] = {};
			
			magazines[] = {};
			
			attachments[] = {};
			
			items[] = {
				{"EF_M_jkt42", 2500, "true", ""},
				{"EF_M_jkt4", 2500, "true", ""},
				{"EF_suit_1", 3000, "true", ""},
				{"EF_suit_2", 3000, "true", ""},
				{"EF_suit_3", 3000, "true", ""},
				{"EF_suit_4", 3000, "true", ""},
				{"EF_suit_5", 3000, "true", ""},
				{"EF_suit_6", 3000, "true", ""},
				{"TRYK_SUITS_BLK_F", 3000, "true", ""},
				{"TRYK_SUITS_BR_F", 3000, "true", ""}
			};
		};
		
		class cadet_cop_shop {
        title = "JPD Cadet Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
			{"6Rnd_357M_Magazine", 90, "true", ""},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"}
        };
       
        attachments[] = {};
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_CDT", 10, "true", "JPD Cadet Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class officer_cop_shop {
        title = "JPD Officer Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", "Barska"}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_OFC", 10, "true", "JPD Officer Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class corporal_cop_shop {
        title = "JPD Corporal Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", "Barska"}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_CPL", 10, "true", "JPD Corporal Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class sergeant_cop_shop {
        title = "JPD Sergeant Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_SGT", 10, "true", "JPD Sergeant Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class lieutenant_cop_shop {
        title = "JPD Lieutenant Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"KA_RO991", 10, "true", "Colt 9mm SMG"},
            {"KA_UMP45", 10, "true", "UMP 45acp"},
            {"Bnae_r1_virtual", 10, "true", "R1 1911"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"KA_32Rnd_9x19_FMJ_Mag", 10, "true", "Colt 9mm SMG 32rnd Mag"},
            {"KA_25Rnd_45ACP_FMJ_Mag", 10, "true", "UMP 45 25rnd Mag"},
            {"8Rnd_45GAP_Magazine", 10, "true", "R1 1911 8rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_LT", 10, "true", "JPD Lieutenant Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class captain_cop_shop {
        title = "JPD Captain Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"KA_RO991", 10, "true", "Colt 9mm SMG"},
            {"KA_UMP45", 10, "true", "UMP 45acp"},
            {"Bnae_r1_virtual", 10, "true", "R1 1911"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"KA_32Rnd_9x19_FMJ_Mag", 10, "true", "Colt 9mm SMG 32rnd Mag"},
            {"KA_25Rnd_45ACP_FMJ_Mag", 10, "true", "UMP 45 25rnd Mag"},
            {"8Rnd_45GAP_Magazine", 10, "true", "R1 1911 8rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"PI_JPD_CPT", 10, "true", "JPD Captain Uni"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class swat_cop_shop {
        title = "JPD SWAT Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"KA_RO991", 10, "true", "Colt 9mm SMG"},
            {"KA_UMP45", 10, "true", "UMP 45acp"},
            {"Bnae_r1_virtual", 10, "true", "R1 1911"},
            {"RH_m4", 10, "true", "M4"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"KA_32Rnd_9x19_FMJ_Mag", 10, "true", "Colt 9mm SMG 32rnd Mag"},
            {"KA_25Rnd_45ACP_FMJ_Mag", 10, "true", "UMP 45 25rnd Mag"},
            {"8Rnd_45GAP_Magazine", 10, "true", "R1 1911 8rnd Mag"},
            {"RH_30Rnd_556x45_M855A1", 10, "true", "M4 5.56 30rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"},
            {"PI_SWAT_OFC", 10, "true", "SWAT Operator"},
            {"PI_SWAT_CPL", 10, "true", "SWAT Corporal"},
            {"PI_SWAT_SGT", 10, "true", "SWAT Sergeant"},
            {"RM_SWAT_Uniform_01", 10, "true", "SWAT Uni Blue"},
            {"RM_SWAT_Uniform_02", 10, "true", "SWAT Uni Black"},
            {"JPD_SwatVest_Blk", 10, "true", "SWAT Vest Black"},
            {"JPD_SwatVest_Grn", 10, "true", "SWAT Vest Green"},
            {"JPD_SwatVest_Tan", 10, "true", "SWAT Vest Tan"},
            {"RM_SWAT_Helmet_01", 10, "true", "SWAT Helmet"},
            {"TRYK_kio_balaclava_BLK", 10, "true", "SWAT Balaclava"},
            {"TRYK_ESS_BLKBLK_NV", 10, "true", "SWAT ESS"}
        };
    };
   
    class swatc_cop_shop {
        title = "JPD SWAT Command Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"KA_RO991", 10, "true", "Colt 9mm SMG"},
            {"KA_UMP45", 10, "true", "UMP 45acp"},
            {"Bnae_r1_virtual", 10, "true", "R1 1911"},
            {"RH_m4", 10, "true", "M4"},
            {"BP_M4_300MK", 10, "true", "M4 Masterkey"},
            {"BP_R700", 10, "true", "R700"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
			{"6Rnd_357M_Magazine", 90, "true", ""},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"KA_32Rnd_9x19_FMJ_Mag", 10, "true", "Colt 9mm SMG 32rnd Mag"},
            {"KA_25Rnd_45ACP_FMJ_Mag", 10, "true", "UMP 45 25rnd Mag"},
            {"8Rnd_45GAP_Magazine", 10, "true", "R1 1911 8rnd Mag"},
            {"RH_30Rnd_556x45_M855A1", 10, "true", "M4 5.56 30rnd Mag"},
            {"BP_556x45_Stanag", 10, "true", "M4 Masterkey 30rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "4rnd Non Lethal Mag Masterkey"},
            {"BP_5Rnd_762x51_Mag", 10, "true", "R700 5rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""},
            {"BP_762Muzzle", 10, "true", "R700 Muzzle"},
            {"BP_MRT", 10, "true", "R700 MRT Scope"},
            {"BP_Harris", 10, "true", "R700 Bipod"}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"},
            {"PI_SWAT_LT", 10, "true", "SWAT Lieutenant"},
            {"PI_SWAT_CPT", 10, "true", "SWAT Captain"},
            {"RM_SWAT_Uniform_01", 10, "true", "SWAT Uni Blue"},
            {"RM_SWAT_Uniform_02", 10, "true", "SWAT Uni Black"},
            {"JPD_SwatVest_Blk", 10, "true", "SWAT Vest Black"},
            {"JPD_SwatVest_Grn", 10, "true", "SWAT Vest Green"},
            {"JPD_SwatVest_Tan", 10, "true", "SWAT Vest Tan"},
            {"RM_SWAT_Helmet_01", 10, "true", "SWAT Helmet"},
            {"TRYK_kio_balaclava_BLK", 10, "true", "SWAT Balaclava"},
            {"TRYK_ESS_BLKBLK_NV", 10, "true", "SWAT ESS"}
        };
    };
   
    class dtu_cop_shop {
        title = "JPD DTU Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
			{"bnae_saa_virtual", 10, "true", ""},
            {"RH_g22", 10, "true", "Glock 22"},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
			{"6Rnd_357M_Magazine", 10, "true", ""},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
			{"EF_BADGE_P_NV",10,"true","Badge"},
			{"G_Aviator",10,"true",""},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"A3L_DTU_tan", 10, "true", "JPD DTU Tan Ops Uni"},
            {"A3L_DTU_blue", 10, "true", "JPD DTU Blue Ops Uni"},
            {"JPD_DTUVest_Blk", 10, "true", "DTU Vest Black"},
            {"JPD_DTUVest_Grn", 10, "true", "DTU Vest Green"},
            {"JPD_DTUVest_Tan", 10, "true", "DTU Vest Tan"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_BLT_M1B", 10, "true", ""},
            {"EF_BS", 10, "true", ""},
            {"EF_SH_BK", 10, "true", ""},
            {"EF_Mcap_PB", 10, "true", "Police Cap"},
            {"Nikos_test", 10, "true", ""},
            {"TRYK_U_B_PCUGs_BLK", 10, "true", ""},
            {"TRYK_shirts_PAD_BL", 10, "true", ""},
            {"EF_HM_B1", 10, "true", ""},
            {"EF_HM_OD1", 10, "true", ""},
            {"RYK_shirts_PAD_BLU3", 10, "true", ""},
            {"RedPlaid_uni", 10, "true", ""},
            {"BluePlad_uni", 10, "true", ""},
            {"GreenPlaid_uni", 10, "true", ""},
            {"U_I_G_resistanceLeader_F", 10, "true", ""},
            {"EF_M_jkt1", 10, "true", ""},
            {"EF_M_jkt1_3", 10, "true", ""},
            {"EF_M_jkt4", 10, "true", ""},
            {"EF_M_jkt42", 10, "true", ""},
            {"EF_suit_1", 10, "true", ""},
            {"EF_suit_2", 10, "true", ""},
            {"EF_suit_3", 10, "true", ""},
            {"EF_suit_4", 10, "true", ""},
            {"EF_suit_5", 10, "true", ""},
            {"EF_suit_6", 10, "true", ""},
            {"U_C_HunterBody_grn", 10, "true", ""},
            {"janp_ralph_1 ", 10, "true", ""},
            {"TRYK_U_B_BLKBLK_CombatUniform", 10, "true", ""},
            {"TRYK_U_B_BLKOCP_CombatUniform", 10, "true", ""},
            {"TRYK_U_B_BLKTAN_CombatUniform", 10, "true", ""},
            {"TRYK_OVERALL_SAGE_BLKboots_nk_blk2", 10, "true", ""},
            {"TRYK_OVERALL_nok_flesh", 10, "true", ""},
            {"TRYK_U_Bts_PCUODs", 10, "true", ""},
            {"TRYK_U_B_PCUGs_BLK_R", 10, "true", ""},
            {"TRYK_U_B_PCUGs_gry_R", 10, "true", ""},
            {"TRYK_U_B_PCUGs_OD_R", 10, "true", ""},
            {"TRYK_shirts_DENIM_BL", 10, "true", ""},
            {"TRYK_shirts_DENIM_od", 10, "true", ""},
            {"TRYK_shirts_DENIM_RED2", 10, "true", ""},
            {"TRYK_U_hood_mc", 10, "true", ""},
            {"TRYK_U_denim_hood_mc", 10, "true", ""},
            {"TRYK_U_denim_jersey_blk", 10, "true", ""},
            {"TRYK_U_denim_jersey_blu", 10, "true", ""},
            {"TRYK_U_taki_G_COY", 10, "true", ""},
            {"TRYK_U_taki_G_WH", 10, "true", ""},
            {"Janp2_vape2_1", 10, "true", ""},
            {"TRYK_SUITS_BLK_F", 10, "true", ""},
            {"TRYK_U_B_Denim_T_BK", 10, "true", ""},
            {"V_TacVest_blk", 10, "true", ""},
            {"V_TacVest_brn", 10, "true", ""},
            {"V_TacVest_camo", 10, "true", ""},
            {"V_TacVest_khk", 10, "true", ""},
            {"V_TacVest_oli", 10, "true", ""},
            {"TRYK_V_Bulletproof_BLK", 10, "true", ""},
            {"TRYK_V_Bulletproof_BL", 10, "true", ""},
            {"TRYK_V_Bulletproof", 10, "true", ""},
            {"V_Press_F", 10, "true", ""},
            {"B_Carryall_khk", 10, "true", ""},
            {"B_Carryall_oli", 10, "true", ""},
            {"TRYK_B_tube_blk", 10, "true", ""},
            {"TRYK_B_tube_cyt", 10, "true", ""},
            {"TRYK_B_tube_od", 10, "true", ""},
            {"H_Bandanna_surfer", 10, "true", ""},
            {"H_Booniehat_tan", 10, "true", ""},
            {"H_Cap_blk", 10, "true", ""},
            {"H_Cap_surfer", 10, "true", ""},
            {"H_Hat_grey", 10, "true", ""},
            {"H_Hat_tan", 10, "true", ""},
            {"TRYK_UA_CAP_U", 10, "true", ""},
            {"TRYK_H_woolhat_tan", 10, "true", ""},
            {"TRYK_ESS_CAP", 10, "true", ""},
            {"TRYK_H_woolhat", 10, "true", ""},
            {"TRYK_H_wig", 10, "true", ""},
            {"TRYK_H_Bandana", 10, "true", ""},
            {"TRYK_H_Bandana_wig", 10, "true", ""},
            {"EF_RG2", 10, "true", ""},
            {"G_Spectacles_Tinted", 10, "true", ""},
            {"TRYK_Beard_BK", 10, "true", ""},
            {"TRYK_Beard_Bl", 10, "true", ""},
            {"G_Bandanna_beast", 10, "true", ""},
            {"TRYK_Beard_Gr", 10, "true", ""},
            {"G_Bandanna_aviator", 10, "true", ""},
            {"G_Bandanna_Sport", 10, "true", ""},
            {"TRYK_kio_balaclavas_BLK", 10, "true", ""},
            {"TRYK_kio_balaclavas_ESS", 10, "true", ""},
            {"TRAYK_beard_BW", 10, "true", ""},
            {"G_Bandanna_blk", 10, "true", ""},
            {"G_balaclava_blk", 10, "true", ""},
            {"TRYK_kio_balaclavas   ", 10, "true", ""},
			{"PI_JPD_CPT", 10, "true", "JPD Captain Uni"},
			{"PI_JPD_LT", 10, "true", "JPD Lieutenant Uni"},
			{"PI_JPD_OFC", 10, "true", "JPD Officer Uni"},
            {"G_Bandanna_Shades", 10, "true", ""}  
        };
    };
 
    class dtuc_cop_shop {
        title = "JPD DTU Command Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"BP_M16OLD", 10, "true", "AR15"},
            {"BP_LeeEnfield", 10, "true", ""},
            {"BP_Garand", 10, "true", ""},
            {"KA_MK23_black", 10, "true", ""},
            {"Mossberg_590", 10, "true", ""}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"BP_556x45_Stanag", 10, "true", "AR15 Mag"},
            {"BP_10Rnd_303_Mag", 10, "true", ""},
            {"BP_8Rnd_3006_Mag", 10, "true", ""},
            {"KA_12Rnd_45ACP_FMJ_Mag", 10, "true", ""},
            {"8Rnd_Mossberg_590_Pellets", 10, "true", ""}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"A3L_DTU_tan", 10, "true", "JPD DTU Tan Ops Uni"},
            {"A3L_DTU_blue", 10, "true", "JPD DTU Blue Ops Uni"},
            {"JPD_DTUVest_Blk", 10, "true", "DTU Vest Black"},
            {"JPD_DTUVest_Grn", 10, "true", "DTU Vest Green"},
            {"JPD_DTUVest_Tan", 10, "true", "DTU Vest Tan"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_BLT_M1B", 10, "true", ""},
            {"EF_BS", 10, "true", ""},
            {"EF_SH_BK", 10, "true", ""},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
   
    class highc_cop_shop {
        title = "JPD High Command Command Shop";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "cop";
       
        weapons[] = {
            {"RH_m9", 10, "true", "Beretta M9"},
            {"RH_g17", 10, "true", "Glock 17"},
            {"KA_P226_Black", 10, "true", "Sig P226"},
            {"Taser_26_tw", 10, "true", "X26 Taser 3rnd"},
            {"BP_Benelli", 10, "true", "Benelli Super 90"},
            {"BP_Rem870", 10, "true", "Remington 870"},
            {"KA_Px4_Black", 10, "true", "Px4 Storm"},
            {"RH_usp", 10, "true", "USP45"},
            {"RH_fnp45", 10, "true", "FNP45"},
            {"RH_g22", 10, "true", "Glock 22"},
            {"BP_MP5", 10, "true", "HK MP5"},
            {"BP_RugerTraining", 10, "true", "Ruger Training Rifle"},
            {"BP_m9Training", 10, "true", "Beretta Training Pistol"},
            {"KA_RO991", 10, "true", "Colt 9mm SMG"},
            {"KA_UMP45", 10, "true", "UMP 45acp"},
            {"Bnae_r1_virtual", 10, "true", "R1 1911"},
            {"RH_m4", 10, "true", "M4"},
            {"BP_M4_300MK", 10, "true", "M4 Masterkey"},
            {"BP_R700", 10, "true", "R700"}
        };
       
        magazines[] = {
            {"RH_15Rnd_9x19_M9", 10, "true", "Beretta M9 15rnd Mag"},
            {"RH_17Rnd_9x19_g17", 10, "true", "Glock 17 17rnd Mag"},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 10, "true", "Sig P226 15rnd Mag"},
            {"26_cartridge_b", 10, "true", "X26 3rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "Shotgun 4rnd NON Lethal"},
            {"BP_8Rnd_Buckshot", 10, "true", "Shotgun 8rnd Lethal"},
            {"KA_Px4_17Rnd_9x19_FMJ_Mag", 10, "true", "Px4 Storm 17rnd Mag"},
            {"RH_12Rnd_45cal_usp", 10, "true", "USP45 12rnd Mag"},
            {"RH_15Rnd_45cal_fnp", 10, "true", "FNP45 15rnd Mag"},
            {"RH_17Rnd_40SW_g22", 10, "true", "Glock 22 17rnd Mag"},
            {"BP_30Rnd_9x21_Mag", 10, "true", "HK MP5 30rnd Mag"},
            {"BP_25Rnd_22_MagTraining", 10, "true", "Ruger 22lr 25rnd Training Mag"},
            {"BP_16Rnd_9x21_MagTraining", 10, "true", "Beretta 16rnd Training Mag"},
            {"KA_32Rnd_9x19_FMJ_Mag", 10, "true", "Colt 9mm SMG 32rnd Mag"},
            {"KA_25Rnd_45ACP_FMJ_Mag", 10, "true", "UMP 45 25rnd Mag"},
            {"8Rnd_45GAP_Magazine", 10, "true", "R1 1911 8rnd Mag"},
            {"RH_30Rnd_556x45_M855A1", 10, "true", "M4 5.56 30rnd Mag"},
            {"BP_556x45_Stanag", 10, "true", "M4 Masterkey 30rnd Mag"},
            {"BP_4Rnd_NL", 10, "true", "4rnd Non Lethal Mag Masterkey"},
            {"BP_5Rnd_762x51_Mag", 10, "true", "R700 5rnd Mag"}
        };
       
        attachments[] = {
            {"RB_barska_rds", 10, "true", ""},
            {"optic_Aco", 10, "true", "ACO RDS MP5"},
            {"RH_compm2l", 10, "true", ""},
            {"RH_compM2", 10, "true", ""},
            {"RH_compm4s", 10, "true", ""},
            {"RH_t1", 10, "true", ""},
            {"RH_cmore", 10, "true", ""},
            {"RH_eotech553", 10, "true", ""},
            {"RH_eotechxps3", 10, "true", ""},
            {"RH_LTdocter", 10, "true", ""},
            {"RH_LTdocterl", 10, "true", ""},
            {"BP_762Muzzle", 10, "true", "R700 Muzzle"},
            {"BP_MRT", 10, "true", "R700 MRT Scope"},
            {"BP_Harris", 10, "true", "R700 Bipod"}
        };
       
        items[] = {
            {"pmc_earpiece", 10, "true", "Radio Earpiece"},
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc152", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"},
            {"PI_JPD_DEPCHF", 10, "true", "JPD Deputy Chief Uni"},
            {"PI_SOD_DEPCHF", 10, "true", "JPD Deputy Chief SOD Uni"},
            {"PI_JPD_ASCHF", 10, "true", "JPD Asst Chief Uni"},
            {"PI_JPD_CHF", 10, "true", "JPD Chief Uni"},
            {"JPD_SwatVest_Blk", 10, "true", "SWAT Vest Black"},
            {"PI_JPD_Patrol_PoliceV", 10, "true", "Patrol Vest"},
            {"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"},
            {"EF_Mcap_PB", 10, "true", "Police Cap"}
        };
    };
	
	class general_shop {
        title = "General Store";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "civ";
       
        weapons[] = {
            {"A3PI_Pickaxe2017",100,"true","Pickaxe"},
			{"A3PI_Spade2017",100,"true","Spade"},
			{"A3PI_Hatchet2017",100,"true","Hatchet"}
        };
       
        magazines[] = {
			{"Pickaxe_Swing",25,"true","Tool Ammo"}
        };
       
        attachments[] = {
           
        };
       
        items[] = {
            {"Binocular", 10, "true", "Binocular"},
            {"ItemMap", 10, "true", "Map"},
            {"ItemGPS", 10, "true", "GPS"},
            {"tf_anprc148jem", 10, "true", ""},
            {"ItemCompass", 10, "true", "Compass"},
            {"FirstAidKit", 10, "true", "First Aid"},
            {"ToolKit", 10, "true", "ToolKit"},
            {"Chemlight_red", 10, "true", "Red Chemlight"}
        };
    };
	
	class gun_shop {
        title = "Gun Store";
        condition = "true";
        simple = 0;
        maxCart = 20;
        shopType = "civ";
       
        weapons[] = {
            {"bnae_r1_e_virtual", 11000, "true", ""},
            {"KA_Glock_17_Single", 7000, "true", ""},
            {"KA_P226", 7500, "true", ""},
            {"bnae_saa_virtual", 4500, "true", ""},
            {"bnae_spr220_virtual", 16500, "true", ""},
            {"bnae_m97_virtual", 17000, "true", ""},
            {"RH_mk2", 20000, "true", ""},
            {"BP_M16OLD", 35000, "true", ""},
            {"BP_LeeEnfield", 90000, "true", ""},
            {"BP_M1903", 125000, "true", ""}
        };
       
        magazines[] = {
            {"8Rnd_45ACP_Magazine", 250, "true", ""},
            {"KA_17Rnd_9x19_Mag", 150, "true", ""},
            {"KA_P226_15Rnd_9x19_FMJ_Mag", 150, "true", ""},
            {"6Rnd_357M_Magazine", 90, "true", ""},
            {"2Rnd_00_Buckshot_Magazine", 350, "true", ""},
            {"6Rnd_00_Buckshot_Magazine", 350, "true", ""},
            {"RH_10Rnd_22LR_mk2", 400, "true", ""},
            {"BP_556x45_StanagMK262", 700, "true", ""},
            {"BP_10Rnd_303_Mag", 1800, "true", ""},
            {"BP_5Rnd_3006_Mag", 2500, "true", ""}
        };
       
        attachments[] = {};
       
        items[] = {};
    };
	
	class blackmarket_shop {
	title = "Black Market";
	condition = "true";
	simple = 0;
	maxCart = 20;
	shopType = "civ";
	
	weapons[] = {
		{"BP_GarandU", 100000, "true", ""},
		{"KA_Mx4", 40000, "true", ""},
		{"KA_RO991", 60000, "true", ""},
		{"BP_Rem870", 25000, "true", ""},
		{"KA_UMP9", 35000, "true", ""},
		{"KA_Glock_17", 15000, "true", ""},
		{"KA_Glock_18_Single", 18000, "true", ""},
		{"KA_Glock_18", 21000, "true", ""},
		{"KA_MP7_Rifle_Black_20Rnd", 37500, "true", ""},
		{"KA_KSG_Black", 50000, "true", ""},
		{"BP_Lupara", 23000, "true", ""}
	};
	
	magazines[] = {
		{"BP_8Rnd_3006_Mag", 1500, "true", "M1 Garand 8Rnd 30-06"},
		{"KA_Mx4_30Rnd_9x19_FMJ_Mag", 800, "true", "Mx4 30Rnd 9x19 FMJ"},
		{"KA_32Rnd_9x19_FMJ_Mag", 1100, "true", "RO991 32Rnd 9x19 FMJ"},
		{"BP_8Rnd_Buckshot", 500, "true", "12 Gauge 8Rnd Buckshot"},
		{"KA_30Rnd_9x19_FMJ_Mag", 700, "true", "UMP 30Rnd 9x19 FMJ"},
		{"KA_17Rnd_9x19_Mag", 300, "true", "Glock 17Rnd 9x19"},
		{"KA_20Rnd_46x30_FMJ", 750, "true", "MP7 20Rnd 46x30 FMJ"},
		{"7Rndx2_KSG_buck_mag", 1000, "true", "KelTech 7Rndx2 Buckshot"},
		{"BP_2Rnd_Buckshot", 300, "true", "12 Gauge 2Rnd Buckshot"}
	};
	
	attachments[] = {
		{"RH_comp4s", 7500, "true", "Aimpoint CompM4"},
		{"RH_t1", 3500, "true", "Aimpoint T1"},
		{"RH_barska_rds", 5000, "true", "Barska Red Dot"},
		{"RH_cmore", 5000, "true", "Cmore Red Dot"},
		{"RH_eotech553", 4200, "true", "EOTech 553 Red Dot"},
		{"KA_Pistol_Rail_Public", 1500, "true", "Glock Modular Rail"},
		{"Rail_Public_Flashlight", 1750, "true", "Glock Modular Rail/Light"},
		{"optic_Yorris", 2500, "true", "Yorris J2 Pistol Sight"},
	};
	
	items[] = {
		{"ItemGPS", 100, "true", ""},
		{"ItemCompass", 50, "true", ""},
		{"ItemWatch", 50, "true", ""},
		{"ItemMap", 50, "true", ""},
		{"Binocular", 200, "true", ""},
		{"tf_anprc148jem", 500, "true", ""},
		{"NVGoggles_OPFOR", 2000, "true", ""},
		{"NVGoggles", 2000, "true", ""},
		{"NVGoggles_INDEP", 2000, "true", ""},
		{"TRYK_U_B_ARO1_BLK_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_ARO1_BLK_R_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_PCUHsW8", 3500, "true", ""},
		{"TRYK_U_B_PCUHsW3", 3500, "true", ""},
		{"TRYK_U_B_PCUHsW9", 3500, "true", ""},
		{"TRYK_U_B_PCUHsW7", 3500, "true", ""},
		{"TRYK_U_B_BLKBLK_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_BLKBLK_R_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_BLKOCP_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_BLKOCP_R_CombatUniformTshirt", 3500, "true", ""},
		{"TRYK_U_B_BLKTAN_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_BLKTANR_CombatUniformTshirt", 3500, "true", ""},
		{"TRYK_U_B_OD_BLK", 3500, "true", ""},
		{"TRYK_U_B_OD_BLK2", 3500, "true", ""},
		{"TRYK_U_B_GRYOCP_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_GRYOCP_R_CombatUniformTshirt", 3500, "true", ""},
		{"TRYK_U_B_ODTAN_CombatUniform", 3500, "true", ""},
		{"TRYK_U_B_ODTANR_CombatUniformTshirt", 3500, "true", ""},
		{"TRYK_U_B_BLKTAN_CombatUniform", 3500, "true", ""},
		{"TRYK_v_ArmorVest_Brown", 4500, "true", ""},
		{"TRYK_v_ArmorVest_CBR", 4500, "true", ""},
		{"TRYK_v_ArmorVest_Coyo", 4500, "true", ""},
		{"TRYK_v_ArmorVest_Delta", 4500, "true", ""},
		{"TRYK_v_ArmorVest_green", 4500, "true", ""},
		{"TRYK_v_ArmorVest_tan", 4500, "true", ""},
		{"TRYK_B_Belt_BLK", 3750, "true", ""},
		{"TRYK_B_Belt_br", 3750, "true", ""},
		{"TRYK_B_Belt_tan", 3750, "true", ""},
		{"TRYK_B_Belt", 3750, "true", ""},
		{"TRYK_B_Belt_GR", 3750, "true", ""},
		{"TRYK_B_Belt_CYT", 3750, "true", ""},
		{"TRYK_B_Alicepack", 4000, "true", ""},
		{"B_Carryall_cbr", 600, "true", ""}, 
		{"TRYK_B_Carryall_blk", 600, "true", ""},
		{"B_Carryall_ocamo", 600, "true", ""},
		{"B_Carryall_mcamo", 600, "true", ""},
		{"B_Carryall_oli", 600, "true", ""},
		{"B_Carryall_oucamo", 600, "true", ""},
		{"B_FieldPack_blk", 400, "true", ""},
		{"B_FieldPack_cbr", 400, "true", ""},
		{"B_FieldPack_ocamo", 400, "true", ""},
		{"B_FieldPack_khk", 400, "true", ""},
		{"B_FieldPack_oli", 400, "true", ""},
		{"B_FieldPack_oucamo", 400, "true", ""},
		{"B_Kitbag_cbr", 500, "true", ""},
		{"TRYK_B_Kitbag_blk", 500, "true", ""},
		{"B_Kitbag_rgr", 500, "true", ""},
		{"B_Kitbag_mcamo", 500, "true", ""},
		{"B_Kitbag_sgg", 500, "true", ""},
		{"G_Spectacles_Tinted", 50, "true", ""},
		{"G_Squares_Tinted", 50, "true", ""},
		{"G_Aviator", 50, "true", ""},
		{"H_Booniehat_khk", 90, "true", ""},
		{"H_Booniehat_mcamo", 90, "true", ""},
		{"H_Booniehat_oli", 90, "true", ""},
		{"H_Booniehat_tan", 90, "true", ""},
		{"H_Cap_red", 90, "true", ""},
		{"H_Cap_tan", 90, "true", ""},
		{"H_Cap_usblack", 90, "true", ""},
		{"TRYK_H_woolhat", 90, "true", ""},
		{"TRYK_H_woolhat_WH", 90, "true", ""},
		{"TRYK_R_CAP_BLK", 90, "true", ""},
		{"TRYK_R_CAP_OD_US", 90, "true", ""},
		{"G_Balaclava_blk", 300, "true", ""},
		{"G_Balaclava_blk", 300, "true", ""},
		{"G_Bandanna_aviator", 300, "true", ""},
		{"G_Bandanna_beast", 300, "true", ""},
		{"G_Bandanna_blk", 300, "true", ""},
		{"TRYK_kio_balaclava_ear", 300, "true", ""},
		{"TRYK_kio_balaclava_BLK_ear", 300, "true", ""},
		{"TRYK_kio_balaclavas", 300, "true", ""},
		{"TRYK_kio_balaclava_BLK", 300, "true", ""},
		{"TRYK_US_ESS_Glasses_NV", 2500, "true", ""},
		{"TRYK_US_ESS_Glasses_TAN_NV", 2500, "true", ""},
		{"TRYK_ESS_BLKTAN_NV", 2500, "true", ""},
		{"TRYK_ESS_wh_NV", 2500, "true", ""},
		{"TRYK_ESS_BLKBLK_NV", 2500, "true", ""}
	};
};
		
		class virt_shop {
			title = "Market";
			condition = "true";
			simple = 1;
			maxCart = 20;
			shopType = "virt";
			
			items[] = {
				{"Item_WaterBottle",50,"true","Water Bottle"},
				{"Item_Donuts",50,"true","Donuts"},
				{"Item_CocaCola",50,"true","Coca Cola"},
				{"Item_Redgull",100,"true","Redgull"},
				{"Item_Coffee",50,"true","Coffee"},
				{"Item_Sandwich",50,"true","Sandwich"},
				{"Item_CookedFox",100,"true","Cooked Fox"},
				{"Item_Zipties",50,"true","Zipties"},
				{"Item_Toolkit",250,"true","Toolkit"},
				{"Item_Bandage",10,"true","Bandage"}
			};
		};
		
		class virtems_shop {
			title = "EMS Virtual Shop";
			condition = "true";
			simple = 1;
			maxCart = 20;
			shopType = "virt";
			
			items[] = {
				{"Item_WaterBottle",50,"true","Water Bottle"},
				{"Item_Donuts",50,"true","Donuts"},
				{"Item_CocaCola",50,"true","Coca Cola"},
				{"Item_Redgull",100,"true","Redgull"},
				{"Item_Coffee",50,"true","Coffee"},
				{"Item_Sandwich",50,"true","Sandwich"},
				{"Item_CookedFox",100,"true","Cooked Fox"},
				{"Item_Toolkit",250,"true","Toolkit"},
				{"Item_Bandage",1,"true","Bandage"},
				{"Item_Splint",1,"true","Splint"},
				{"Item_Cast",1,"true","Cast"},
				{"Item_SutureKit",1,"true","Suture Kit"},
				{"Item_SurgeryKit",1,"true","Surgery Kit"},
				{"Item_BroadSpec",1,"true","Broad Spectrum Antibiotics"},
				{"Item_Gloves",1,"true","Gloves"},
				{"Item_BloodBagAPos",10,"true","Blood Bag A Positive"},
				{"Item_BloodBagOPos",10,"true","Blood Bag O Positive"},
				{"Item_BloodBagBPos",10,"true","Blood Bag B Positive"},
				{"Item_BloodBagABPos",10,"true","Blood Bag AB Positive"},
				{"Item_BloodBagANeg",10,"true","Blood Bag A Negative"},
				{"Item_BloodBagONeg",10,"true","Blood Bag O Negative"},
				{"Item_BloodBagBNeg",10,"true","Blood Bag B Negative"},
				{"Item_BloodBagABNeg",10,"true","Blood Bag AB Negative"},
				{"Item_Defib",10,"true","Defib"},
				{"Item_Plaster",1,"true","Plaster"}
			};
		};
		
		class virtcop_shop {
			title = "LEO Virtual Shop";
			condition = "true";
			simple = 1;
			maxCart = 20;
			shopType = "virt";
			
			items[] = {
				{"Item_WaterBottle",50,"true","Water Bottle"},
				{"Item_Donuts",50,"true","Donuts"},
				{"Item_CocaCola",50,"true","Coca Cola"},
				{"Item_Redgull",100,"true","Redgull"},
				{"Item_Coffee",50,"true","Coffee"},
				{"Item_Sandwich",50,"true","Sandwich"},
				{"Item_CookedFox",100,"true","Cooked Fox"},
				{"Item_Handcuff_Normal",50,"true","Handcuffs"},
				{"Item_Handcuff_Kinky", 50, "true", "Nick's Handcuff's"},
				{"Item_Toolkit",250,"true","Toolkit"},
				{"Item_Bandage",10,"true","Bandage"},
				{"Item_Plaster",1,"true","Plaster"}
			};
		};
		class ems_shop {
	title = "EMS Shop";
	condition = "true";
	simple = 0;
	maxcart = 20;
	shoptype = "civ";
	
	weapons[] = {
	};
	
	magazines[] = {
	};
	
	atatachments[] = {
	};
	
	items[] = {
			{"Toolkit", 10, "true", ""},
			{"Medikit", 10, "true", ""},
			{"FirstAidKit",10,"true",""},
			{"ItemGPS", 10, "true", ""},
			{"ItemCompass", 10, "true", ""},
			{"ItemWatch", 10, "true", ""},
			{"ItemMap", 10, "true", ""},
			{"Binocular", 10, "true", ""},
			{"tf_anprc148jem", 10, "true", ""},
			{"Chemlight_blue", 5, "true", ""},
			{"Chemlight_green", 5, "true", ""},
			{"Chemlight_red", 5, "true", ""},
			{"Chemlight_yellow", 5, "true", ""},
			{"aa_uni1", 10, "true", ""},
			{"pmc_earpiece", 10, "true", ""},
			{"EF_FEM_4A_EMS", 10, "true", ""},
			{"EF_FEM_4A_EMS2", 10, "true", ""},
			{"EF_FEM_4_5_EMS", 10, "true", ""},
			{"EF_F_DR", 10, "true", ""},
			{"EF_MKJKT_EMS", 10, "true", ""},
			{"EF_MKJKT_EMS2", 10, "true", ""},
			{"EF_M_EMS_U", 10, "true", ""},
			{"hg_ems_uniform_airunit", 10, "true", ""},
			{"hg_ems_uniform_advancedcare", 10, "true", ""},
			{"hg_ems_uniform_chief", 10, "true", ""},
			{"hg_ems_uniform_doctor", 10, "true", ""},
			{"hg_ems_uniform_prakti", 10, "true", ""},
			{"EF_Mcap_EMSB", 10, "true", ""},
			{"HG_EMS_Beret", 10, "true", ""},
			{"aa_H", 10, "true", ""},
			{"TRYK_B_Belt_BLK", 10, "true", "Duty Belt"}
			};
		};
		
	};
};

#include "gui\weapon_gui_master.cpp"
