class RscDisplay_MedicalMenu
{
	idd = 19824;
	movingEnabled = false;
    class controlsBackground {
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "\Life_Client\Images\additional\menu_base.paa";
			x = 0.185469 * safezoneW + safezoneX;
			y = -0.16 * safezoneH + safezoneY;
			w = 0.520781 * safezoneW;
			h = 0.935 * safezoneH;
		};
    };
	class controls 
	{
		class RscPicture_1201: A3PIRscPicture
		{
			idc = 1201;
			text = "\A3PI_Textures\Dialogs\MedicalMenu\base.paa";
			x = 0.21125 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.350625 * safezoneW;
			h = 0.572 * safezoneH;
		};
		class RscStructuredText_1100: A3PIRscStructuredText
		{
			idc = 1100;
			text = "test"; //--- ToDo: Localize;
			x = 0.329844 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.18375 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscListbox_1500: A3PIRscListbox
		{
			idc = 1500;
			x = 0.520625 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.22 * safezoneH;
			onLBSelChanged = "call A3PI_fnc_treatOptions;";
		};
		class RscListbox_1501: A3PIRscListbox
		{
			idc = 1501;
			x = 0.520625 * safezoneW + safezoneX;
			y = 0.489 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.198 * safezoneH;
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Close Dialog"; //--- ToDo: Localize;
			onButtonClick = "closeDialog 0;";
			x = 0.613438 * safezoneW + safezoneX;
			y = 0.709 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Use Treatment"; //--- ToDo: Localize;
			x = 0.520625 * safezoneW + safezoneX;
			y = 0.709 * safezoneH + safezoneY;
			w = 0.0876563 * safezoneW;
			h = 0.033 * safezoneH;
			onButtonClick = "call A3PI_fnc_treat;";
		};
		class head: A3PIRscButtonHidden
		{
			idc = 1600;
			x = 0.37625 * safezoneW + safezoneX;
			y = 0.269 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.077 * safezoneH;
			onButtonClick = "[2]call A3PI_fnc_switchUI;";
		};
		class torso: A3PIRscButtonHidden
		{
			idc = 1601;
			x = 0.367999 * safezoneW + safezoneX;
			y = 0.3856 * safezoneH + safezoneY;
			w = 0.0464063 * safezoneW;
			h = 0.099 * safezoneH;
			onButtonClick = "[5]call A3PI_fnc_switchUI;";
		};
		class rightarm: A3PIRscButtonHidden
		{
			idc = 1602;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.379 * safezoneH + safezoneY;
			w = 0.0464063 * safezoneW;
			h = 0.11 * safezoneH;
			onButtonClick = "[9]call A3PI_fnc_switchUI;";
		};
		class leftarm: A3PIRscButtonHidden
		{
			idc = 1603;
			x = 0.319531 * safezoneW + safezoneX;
			y = 0.379 * safezoneH + safezoneY;
			w = 0.0464063 * safezoneW;
			h = 0.11 * safezoneH;
			onButtonClick = "[8]call A3PI_fnc_switchUI;";
		};
		class leftleg: A3PIRscButtonHidden
		{
			idc = 1604;
			x = 0.360781 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.231 * safezoneH;
			onButtonClick = "[12]call A3PI_fnc_switchUI;";
		};
		class rightleg: A3PIRscButtonHidden
		{
			idc = 1605;
			x = 0.391719 * safezoneW + safezoneX;
			y = 0.511 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.231 * safezoneH;
			onButtonClick = "[13]call A3PI_fnc_switchUI;";
		};
	};
};

