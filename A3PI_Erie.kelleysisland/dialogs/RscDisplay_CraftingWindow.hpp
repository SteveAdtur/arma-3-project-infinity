class RscDisplay_CraftingWindow
{
	idd = 19872;
	movingEnabled = false;
	
	class controls
	{
		class Sikoras_rscPic: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.336938 * safezoneW + safezoneX;
			y = 0.28371 * safezoneH + safezoneY;
			w = 0.330532 * safezoneW;
			h = 0.441985 * safezoneH;
			colorText[] = {1,1,1,0.5};
		};
		class Sikoras_rscList: A3PIRscListbox
		{
			idc = 1500;
			x = 0.345752 * safezoneW + safezoneX;
			y = 0.302518 * safezoneH + safezoneY;
			w = 0.18662 * safezoneW;
			h = 0.404369 * safezoneH;
			onLBSelChanged = "call A3PI_fnc_checkReqs";
		};
		class Sikoras_rscButton_1: A3PIRscShortcutButtoneco
		{
			idc = 1600;
			text = "Craft Weapon"; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.594039 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3PI_fnc_craftItem";
		};
		class Sikoras_rscButton_2: A3PIRscShortcutButtoneco
		{
			idc = 1601;
			text = "Exit Menu"; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.669271 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "closeDialog 0";
		};
		class Sikoras_rscText_1: A3PIRscStructuredText
		{
			idc = 1100;
			text = ""; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.311922 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.106886 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};
		class Sikoras_rscText_2: A3PIRscStructuredText
		{
			idc = 1101;
			text = ""; //--- ToDo: Localize;
			x = 0.544071 * safezoneW + safezoneX;
			y = 0.428212 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.1564236 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};

	};
		
};

class Sikoras_PartsCrafting_Dialog
{
	idd = 19872;
	movingEnabled = false;
	
	class controls
	{
		class Sikoras_rscPic: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.336938 * safezoneW + safezoneX;
			y = 0.28371 * safezoneH + safezoneY;
			w = 0.330532 * safezoneW;
			h = 0.441985 * safezoneH;
			colorText[] = {1,1,1,0.5};
		};
		class Sikoras_rscList: A3PIRscListbox
		{
			idc = 1500;
			x = 0.345752 * safezoneW + safezoneX;
			y = 0.302518 * safezoneH + safezoneY;
			w = 0.13662 * safezoneW;
			h = 0.404369 * safezoneH;
			onLBSelChanged = execVM "Scripts\PartsCraftingListing.sqf";
		};
		class Sikoras_rscButton_1: A3PIRscShortcutButtoneco
		{
			idc = 1600;
			text = "Craft Weapon"; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.594039 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.0470196 * safezoneH;
			action = execVM "Scripts\PartsCraftingLinkage.sqf";
		};
		class Sikoras_rscButton_2: A3PIRscShortcutButtoneco
		{
			idc = 1601;
			text = "Exit Menu"; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.669271 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.0470196 * safezoneH;
			action = "closeDialog 0";
		};
		class Sikoras_rscText_1: A3PIRscStructuredText
		{
			idc = 1100;
			text = ""; //--- ToDo: Localize;
			x = 0.54407 * safezoneW + safezoneX;
			y = 0.311922 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.106886 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};
		class Sikoras_rscText_2: A3PIRscStructuredText
		{
			idc = 1101;
			text = ""; //--- ToDo: Localize;
			x = 0.544071 * safezoneW + safezoneX;
			y = 0.428212 * safezoneH + safezoneY;
			w = 0.118992 * safezoneW;
			h = 0.1564236 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};

	};
		
};