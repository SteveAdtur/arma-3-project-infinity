class RscDisplay_Settings
{
	idd = 19855;
	movingEnabled = false;
	class controlsBackground {
		class RscPicture_1200: A3PIRscPicture
		{
			idc = 1200;
			text = "\Life_Client\Images\additional\menu_base.paa";
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.28 * safezoneH + safezoneY;
			w = 0.520781 * safezoneW;
			h = 0.319 * safezoneH;
		};
    };
	class controls
	{
		class RscCheckbox_2800: A3PIRscCheckBoxeco
		{
			idc = 2800;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.456 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2801: A3PIRscCheckBoxeco
		{
			idc = 2801;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2802: A3PIRscCheckBoxeco
		{
			idc = 2802;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2803: A3PIRscCheckBoxeco
		{
			idc = 2803;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2804: A3PIRscCheckBoxeco
		{
			idc = 2804;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1100: A3PIRscStructuredText
		{
			idc = 1100;
			text = "Disable Hud"; //--- ToDo: Localize;
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.456 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1101: A3PIRscStructuredText
		{
			idc = 1101;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1102: A3PIRscStructuredText
		{
			idc = 1102;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1103: A3PIRscStructuredText
		{
			idc = 1103;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1104: A3PIRscStructuredText
		{
			idc = 1104;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.324687 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2805: A3PIRscCheckBoxeco
		{
			idc = 2805;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.456 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1105: A3PIRscStructuredText
		{
			idc = 1105;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.463906 * safezoneW + safezoneX;
			y = 0.456 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2806: A3PIRscCheckBoxeco
		{
			idc = 2806;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1106: A3PIRscStructuredText
		{
			idc = 1106;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.463906 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2807: A3PIRscCheckBoxeco
		{
			idc = 2807;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2808: A3PIRscCheckBoxeco
		{
			idc = 2808;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscCheckbox_2809: A3PIRscCheckBoxeco
		{
			idc = 2809;
			x = 0.448438 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.0154688 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1107: A3PIRscStructuredText
		{
			idc = 1107;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.463906 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1108: A3PIRscStructuredText
		{
			idc = 1108;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.463906 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class RscStructuredText_1109: A3PIRscStructuredText
		{
			idc = 1109;
			text = "Show Name"; //--- ToDo: Localize;
			x = 0.463906 * safezoneW + safezoneX;
			y = 0.544 * safezoneH + safezoneY;
			w = 0.103125 * safezoneW;
			h = 0.022 * safezoneH;
		};
	
	};
};



