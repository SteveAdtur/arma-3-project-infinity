class RscDisplay_RefiningWindow
{
	idd = 19873;
	movingEnabled = false;
	
	class controls
	{
		class Sikoras_rscPic: A3PIRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.350159 * safezoneW + safezoneX;
			y = 0.321325 * safezoneH + safezoneY;
			w = 0.299682 * safezoneW;
			h = 0.206886 * safezoneH;
			colorText[] = {1,1,1,0.5};
		};
		class Sikoras_rscList: A3PIRscListbox
		{
			idc = 1500;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.340133 * safezoneH + safezoneY;
			w = 0.149841 * safezoneW;
			h = 0.169271 * safezoneH;
		};
		class Sikoras_rscButton_1: A3PIRscShortcutButtoneco
		{
			idc = 1600;
			text = "Refine"; //--- ToDo: Localize;
			x = 0.53085 * safezoneW + safezoneX;
			y = 0.358941 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0564236 * safezoneH;
			onButtonClick = "[] spawn A3PI_fnc_useRefinery";
		};
		class Sikoras_rscButton_2: A3PIRscShortcutButtoneco
		{
			idc = 1601;
			text = "Exit Menu"; //--- ToDo: Localize;
			x = 0.53085 * safezoneW + safezoneX;
			y = 0.434173 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0564236 * safezoneH;
			onButtonClick = "closeDialog 0";
		};

	};
		
};


