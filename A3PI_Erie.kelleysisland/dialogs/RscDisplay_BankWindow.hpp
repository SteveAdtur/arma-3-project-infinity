class RscDisplay_BankWindow
{
	idd = 19875;
	movingEnabled = false;
	
	class controls
	{
		class RscText_1000: A3PIRscText
		{
			idc = 1000;
			text = "Jersey Banking Union"; //--- ToDo: Localize;
			x = 0.422637 * safezoneW + safezoneX;
			y = 0.312942 * safezoneH + safezoneY;
			w = 0.159884 * safezoneW;
			h = 0.0220068 * safezoneH;
			colorBackground[] = {0.996,0,0.078,0.75};
		};
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			x = 0.422637 * safezoneW + safezoneX;
			y = 0.332748 * safezoneH + safezoneY;
			w = 0.159884 * safezoneW;
			h = 0.198061 * safezoneH;
			colorBackground[] = {0.102,0.102,0.102,1};
		};
		class RscStructuredText_1100: A3PIRscStructuredText
		{
			idc = 1100;
			text = "Bank Balance: $999999999"; //--- ToDo: Localize;
			x = 0.438109 * safezoneW + safezoneX;
			y = 0.356956 * safezoneH + safezoneY;
			w = 0.134097 * safezoneW;
			h = 0.0220068 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class RscStructuredText_1101: A3PIRscStructuredText
		{
			idc = 1101;
			text = "On Hand Cash: $999999999"; //--- ToDo: Localize;
			x = 0.438109 * safezoneW + safezoneX;
			y = 0.378962 * safezoneH + safezoneY;
			w = 0.134097 * safezoneW;
			h = 0.0220068 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class Sikoras_edit: A3PIRscEdit
		{
			idc = 1400;
			text = "0";
			x = 0.438109 * safezoneW + safezoneX;
			y = 0.411973 * safezoneH + safezoneY;
			w = 0.134097 * safezoneW;
			h = 0.0220068 * safezoneH;
		};
		class Sikoras_button1: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Withdraw Cash"; //--- ToDo: Localize;
			x = 0.438109 * safezoneW + safezoneX;
			y = 0.444983 * safezoneH + safezoneY;
			w = 0.134097 * safezoneW;
			h = 0.0330102 * safezoneH;
			onButtonClick = "call A3PI_fnc_Withdraw";
		};
		class Sikoras_button2: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Deposit Cash"; //--- ToDo: Localize;
			x = 0.438109 * safezoneW + safezoneX;
			y = 0.475792 * safezoneH + safezoneY;
			w = 0.134097 * safezoneW;
			h = 0.0330102 * safezoneH;
			onButtonClick = "call A3PI_fnc_Deposit";
		};


		
		
	};	
	
};