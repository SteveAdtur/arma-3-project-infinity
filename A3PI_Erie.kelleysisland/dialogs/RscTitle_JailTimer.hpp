class RscTitle_JailTimer {
    name = "RscTitle_JailTimer";
    idd = -1;
    fadein=0;
    duration = 99999999999;
    fadeout=0;
    movingEnable = 0;
    onLoad="uiNamespace setVariable ['RscTitle_JailTimer',_this select 0]";
    objects[]={};

    class controlsBackground {
		class RscStructuredText_1100: A3PIRscStructuredText
		{
			idc = 1100;
			text = ""; //--- ToDo: Localize;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.148 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.088 * safezoneH;
			colorBackground[] = {0,0,0,0};
			sizeEx = 4 * GUI_GRID_H;
		};
    };
};


class RscTitle_ShackledLegs {
    name = "RscTitle_ShackledLegs";
    idd = -1;
    fadein=0;
    duration = 99999999999;
    fadeout=0;
    movingEnable = 0;
    onLoad="uiNamespace setVariable ['RscTitle_ShackledLegs',_this select 0]";
    objects[]={};

    class controlsBackground {
		class RscStructuredText_1101: A3PIRscStructuredText 
		{
			idc = 1101;
			text = "* Your Legs are Shackled *"; //--- ToDo: Localize;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.775 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0};
			sizeEx = 4 * GUI_GRID_H;
		};
    };
};




