class RscDisplay_Factory {
	idd = 29874;
	duration = 1e+012;
	onLoad = "uiNamespace setVariable [""RscDisplay_Factory"", _this select 0];";
	class controls {
		class BG: A3PIRscText
		{
			idc = 1000;
			x = 0.293748 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.417656 * safezoneW;
			h = 0.693 * safezoneH;
			colorBackground[] = {0,0,0,0.5};
		};
		class RscShortcutButton_1700: A3PIRscShortcutButtoneco
		{
			idc = 1700;
			text = "Add to Factory"; //--- ToDo: Localize;
			onButtonClick = "[] spawn A3PI_fnc_addtoqueueFactory";
			x = 0.5 * safezoneW + safezoneX;
			y = 0.555 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class inv: A3PIRscListbox
		{
			idc = 1500;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.269 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.407 * safezoneH;
		};
		class process: A3PIRscListbox
		{
			idc = 1501;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.269 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.242 * safezoneH;
		};
		class RscShortcutButton_1701: A3PIRscShortcutButtoneco
		{
			idc = 1701;
			text = "Remove from Factory"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.599 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class tank: A3PIRscListbox
		{
			idc = 1502;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.709 * safezoneH + safezoneY;
			w = 0.386719 * safezoneW;
			h = 0.187 * safezoneH;
		};
		class RscShortcutButton_1702: A3PIRscShortcutButtoneco
		{
			idc = 1702;
			text = "Produce Product"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.643 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class RscText_1001: A3PIRscText
		{
			idc = 1001;
			text = "0/50"; //--- ToDo: Localize;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,1,0,0.1};
		};
		class RscText_1002: A3PIRscText
		{
			idc = 1002;
			text = "Process Lister"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.247 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,1,0,0.1};
		};
		class RscText_1003: A3PIRscText
		{
			idc = 1003;
			text = "0/200000"; //--- ToDo: Localize;
			x = 0.309219 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.386719 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,1,0,0.1};
		};
		class edit: A3PIRscEdit
		{
			idc = 1400;
			text = "1"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,0,0,0.5};
		};
		class RscText_1004: A3PIRscText
		{
			idc = 1004;
			text = "Factory"; //--- ToDo: Localize;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.203 * safezoneH + safezoneY;
			w = 0.417656 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = {0,1,0,0.1};
		};
	};
};
