class RscTitle_ProgressBar {
    name = "RscTitle_ProgressBar";
    idd = -1;
    fadein=0;
    duration = 99999999999;
    fadeout=0;
    movingEnable = 0;
    onLoad="uiNamespace setVariable ['RscTitle_ProgressBar',_this select 0]";
    objects[]={};

    class controlsBackground {
		class ProgressBarBackground: A3PIRscText
		{
			idc = 1000;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.775 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.033 * safezoneH;
			colorBackground[] = {0,0,0,0.7};
		};
		class ProgressBar: A3PIRscProgress
		{
			idc = 1001;
			x = 0.298905 * safezoneW + safezoneX;
			y = 0.786 * safezoneH + safezoneY;
			w = 0.402187 * safezoneW;
			h = 0.011 * safezoneH;
			colorBackground[] = {0.576,0.263,0.247,1};
		};
		class StructuredText: A3PIRscStructuredText
		{
			idc = 1100;
			text = "Processing Iron Ore (43%)"; //--- ToDo: Localize;
			x = 0.29375 * safezoneW + safezoneX;
			y = 0.753 * safezoneH + safezoneY;
			w = 0.4125 * safezoneW;
			h = 0.022 * safezoneH;
			colorText[] = {0.576,0.263,0.247,1};
			colorBackground[] = {0,0,0,0};
		};
    };
};
