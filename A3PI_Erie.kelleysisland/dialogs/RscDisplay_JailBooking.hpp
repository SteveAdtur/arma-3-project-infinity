class RscDisplay_JailBooking
{
	idd = 19881;
	movingEnabled = false;
	
	class controls
	{
		class Background: A3PIRscPicture
		{
			idc = 1200;
			text = "\A3PI_Textures\Dialogs\JailBooking.paa";
			x = 0.365937 * safezoneW + safezoneX;
			y = 0.313 * safezoneH + safezoneY;
			w = 0.257813 * safezoneW;
			h = 0.374 * safezoneH;
		};
		class Book: A3PIRscButtonHidden
		{
			idc = 1600;
			onButtonClick = "[cursorObject] call A3PI_fnc_sendJail";
			text = ""; //--- ToDo: Localize;
			x = 0.54125 * safezoneW + safezoneX;
			y = 0.6408 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class Hours: A3PIRscEdit
		{
			idc = 1400;
			borderSize = 0;
			colorBackground[] = {0,0,0,0};
			colorBorder[] = {0,0,0,0};
			text = ""; //--- ToDo: Localize;
			x = 0.382437 * safezoneW + safezoneX;
			y = 0.3966 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class Minutes: A3PIRscEdit
		{
			idc = 1401;
			borderSize = 0;
			colorBackground[] = {0,0,0,0};
			colorBorder[] = {0,0,0,0};
			text = ""; //--- ToDo: Localize;
			x = 0.461843 * safezoneW + safezoneX;
			y = 0.3966 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class Seconds: A3PIRscEdit
		{
			idc = 1402;
			borderSize = 0;
			colorBackground[] = {0,0,0,0};
			colorBorder[] = {0,0,0,0};
			text = ""; //--- ToDo: Localize;
			x = 0.54125 * safezoneW + safezoneX;
			y = 0.3966 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class Reasons: A3PIRscEdit
		{
			idc = 1403;
			borderSize = 0;
			style = 16;
			colorBackground[] = {0,0,0,0};
			colorBorder[] = {0,0,0,0};
			text = ""; //--- ToDo: Localize;
			x = 0.382438 * safezoneW + safezoneX;
			y = 0.478 * safezoneH + safezoneY;
			w = 0.226875 * safezoneW;
			h = 0.154 * safezoneH;
		};
	};	
	
};