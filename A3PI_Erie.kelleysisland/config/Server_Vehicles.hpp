class Server_Vehicles
{
	rotationSpeed = 2;
	
	class localization {
		msgParamEmpty = "Shop Parameter is Empty";
		msgInVehicle =						"You cannot be in a Vehicle!";
		msgShopExists =						"Shop doesn't Exist!";
		msgCondition =						"Not permitted to access this Shop!";
		msgCashOnHand =						"Cash on Hand - %1%2";
		msgCartTotal =						"Your Cart - %1%2";
		msgInfoTooltip =					"--> HOLD YOUR LEFT MOUSE BUTTON DOWN WHILE MOVING MOUSE TO ROTATE WEAPON.\n--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgInfoTooltip2 = 					"--> DOUBLE CLICK ON AN ITEM IN THE CART TO REMOVE IT.\n--> USE THE 'OVERRIDE GEAR' CHECKBOX TO REPLACE WEAPONS ON HAND WITH PURCHASED WEAPONS.";
		msgEmptyShop = 						"Nothing Found...";
		msgInfoText	=						"<t color='#FFFFFF'>Price:</t> <t color='%1'>%3%2</t>";
		msgCartFull	=						"Cart is Full";
		msgCartEmpty =						"Cart is Empty";
		msgNotEnoughCash =					"Not enough Cash for this Transaction";
		msgOverrideAlert =					"Use the override feature to override gear!";
		msgTransactionComplete =			"Purchase completed for %1%2";
		msgNotEnoughSpace =				 	"You didn't have enough space for all the items. You however only paid for those you had space for!";
		msgClear =							"Clear";
		msgSearch =							"Search";
		
		#define dialogCompleteBtn			"Purchase"
		#define dialogCloseBtn 				"Close"
	};
	
	class shops {
				
		class ems_veh_shop{
			title = "EMS Vehicle Garage";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "ems_veh_spawn";
			
			vehicles[] = {
				// ---- Veh Class Name , price, condition, custom display name
				{"Jonzie_Ambulance",1,"true","",100},
				{"ML63ERDblu",1,"true","",100},
				{"ML63ERDred",1,"true","",100}
			};
		};
		
		class patrol_veh_shop{
			title = "Marked Vehicle Garage";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "cop_veh_spawn";
			
			vehicles[] = {
				{"RS3JPD",1,"true","",25},
				{"RS6_AvantPlu",1,"true","",25},
				{"RS7JPD",1,"true","",25},
				{"M5JPD",1,"true","",25},
				{"CHRPlu",1,"true","",25},
				{"SRT8_12JPD",1,"true","",25},
				{"ExplorerJPD",1,"true","",45},
				{"Mk3JPD",1,"true","",25},
				{"MustangJPD",1,"true","",25},
				{"RaptorJPD",1,"true","",50},
				{"GT500JPDTTF",1,"true","",25},
				{"GT500JPDCMD",1,"true","",25},
				{"A45JPD",1,"true","",25},
				{"ML63JPD",1,"true","",25},
				{"R35JPD",1,"true","",25},
				{"Panameralu_P",1,"true","",25}
			};
		};
		
		class uc_veh_shop{
			title = "Undercover Vehicle Garage";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "cop_veh_spawn";
			
			vehicles[] = {
				{"ivory_rs4_unmarked",1,"true","",25},
				{"ivory_m3_unmarked",1,"true","",25},
				{"Explorerlu_US",1,"true","",25},
				{"AMG_TahoePolice",1,"true","",25}
			};
		};
		
		class audi_veh_shop{
			title = "Audi Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "audi_veh_spawn";
			
			vehicles[] = {
				{"AlessioR8",100000,"true","",25},
				{"AlessioR8_Chrome",120000,"true","",25},
				{"AlessioR8_Gold",120000,"true","",25},
				{"AlessioR8Mat",115000,"true","",25},
				{"RS3lu_civ",30000,"true","Audi RS3 2011 White",25},
				{"RS3lu_civ_bleu",30000,"true","Audi RS3 2011 Blue",25},
				{"RS3lu_civ_gris",30000,"true","Audi RS3 2011 Grey",25},
				{"RS3lu_civ_yellow",30000,"true","Audi RS3 2011 Yellow",25},
				{"RS3lu_civ_noir",30000,"true","Audi RS3 2011 Black",25},
				{"RS3lu_civ_rouge",30000,"true","Audi RS3 2011 Red",25},
				{"shounka_a3_rs5_civ_bleufonce",50000,"true","Audi RS5 Blue",25},
				{"shounka_a3_rs5_civ_grise",50000,"true","Audi RS5 Grey",25},
				{"shounka_a3_rs5_civ_jaune",50000,"true","Audi RS5 Yellow",25},
				{"shounka_a3_rs5_civ_noir",50000,"true","Audi RS5 Back",25},
				{"shounka_a3_rs5_civ_orange",50000,"true","Audi RS5 Orange",25},
				{"shounka_a3_rs5_civ_violet",50000,"true","Audi RS5 Purple",25},
				{"shounka_a3_rs5_civ_rouge",50000,"true","Audi RS5 Red",25},
				{"shounka_a3_rs5_civ_rose",50000,"true","Audi RS5 Pink",25},
				{"RS6_Avantlu_civ_bleu",65000,"true","Audi RS6 Avant Blue",25},
				{"RS6_Avantlu_civ_gris",65000,"true","Audi RS6 Avant Grey",25},
				{"RS6_Avantlu_civ_yellow",65000,"true","Audi RS6 Avant Yellow",25},
				{"RS6_Avantlu_civ_noir",65000,"true","Audi RS6 Avant Black",25},
				{"RS6_Avantlu_civ_rouge",65000,"true","Audi RS6 Avant Red",25},
				{"RS7lu_civ_rouge",75000,"true","Audi RS7 Red",25},
				{"RS7lu_civ_noir",75000,"true","Audi RS7 Black",25},
				{"RS7lu_civ_yellow",75000,"true","Audi RS7 Yellow",25},
				{"RS7lu_civ_gris",75000,"true","Audi RS7 Grey",25},
				{"RS7lu_civ_bleu",75000,"true","Audi RS7 Blue",25},
				{"Mrshounka_rs4_Rework_noir_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_bleu_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_verte_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_rose_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_rouge_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_blanche_f",40000,"true","",25},
				{"Mrshounka_rs4_Rework_jaune_f",40000,"true","",25}
			};
		};
		
		class bmw_veh_shop{
			title = "BMW Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "bmw_veh_spawn";
			
			vehicles[] = {
				{"Mrshounka_bmwm6_Rework_noir_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_bleu_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_verte_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_rouge_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_rose_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_blanche_f",120000,"true","",25},
				{"Mrshounka_bmwm6_Rework_jaune_f",120000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_noir_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_bleu_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_verte_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_rose_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_rouge_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_blanche_f",20000,"true","",25},
				{"Mrshounka_bmw_1series_Rework_jaune_f",20000,"true","",25},
				{"M5lu_civ",75000,"true","BMW M5 White",25},
				{"M5lu_civ_bleu",75000,"true","BMW M5 Blue",25},
				{"M5lu_civ_gris",75000,"true","BMW M5 Greyww",25},
				{"M5lu_civ_yellow",75000,"true","BMW M5 Yellow",25},
				{"M5lu_civ_noir",75000,"true","BMW M5 Black",25},
				{"M5lu_civ_rouge",75000,"true","BMW M5 Red",25}
			};
		};
		
		class cit_veh_shop{
			title = "Citroen Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "cit_veh_spawn";
			
			vehicles[] = {
				{"shounka_a3_ds4_bleufonce",15000,"true","Citroen DS4 Blue",35},
				{"shounka_a3_ds4_grise",15000,"true","Citroen DS4 Grey",35},
				{"shounka_a3_ds4_jaune",15000,"true","Citroen DS4 Yellow",35},
				{"shounka_a3_ds4_noir",15000,"true","Citroen DS4 Black",35},
				{"shounka_a3_ds4_orange",15000,"true","Citroen DS4 Orange",35},
				{"shounka_a3_ds4_rose",15000,"true","Citroen DS4 Pink",35},
				{"shounka_a3_ds4_rouge",15000,"true","Citroen DS4 Red",35},
				{"shounka_a3_ds4_violet",15000,"true","Citroen DS4 Purple",35},
				{"Mrshounka_c4_Rework_noir_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_bleu_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_verte_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_rose_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_rouge_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_blanche_f",12000,"true","",35},
				{"Mrshounka_c4_Rework_jaune_f",12000,"true","",35},
				{"Mrshounka_a3_ds3_rework_noir_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_bleu_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_verte_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_rose_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_rouge_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_blanche_f",12000,"true","",25},
				{"Mrshounka_a3_ds3_rework_jaune_f",12000,"true","",25}
			};
		};
		
		class ren_veh_shop{
			title = " Renault Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "ren_veh_spawn";
			
			vehicles[] = {
				{"shounka_a3_cliors_civ_bleufonce",12000,"true","Clio RS Blue",25},
				{"shounka_a3_cliors_civ_grise",12000,"true","Clio RS Grey",25},
				{"shounka_a3_cliors_civ_jaune",12000,"true","Clio RS Yellow",25},
				{"shounka_a3_cliors_civ_noir",12000,"true","Clio RS Black",25},
				{"shounka_a3_cliors_civ_orange",12000,"true","Clio RS Orange",25},
				{"shounka_a3_cliors_civ_rose",12000,"true","Clio RS Pink",25},
				{"shounka_a3_cliors_civ_rouge",12000,"true","Clio RS Red",25},
				{"shounka_a3_cliors_civ_violet",12000,"true","Clio RS Purple",25},
				{"Mrshounka_r5_Rework_noir_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_bleu_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_verte_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_rose_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_rouge_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_blanche_f",15000,"true","",25},
				{"Mrshounka_r5_Rework_jaune_f",15000,"true","",25},
				{"Mrshounka_megane_rs_2015_Rework_p_noir_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_bleu_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_verte_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_rose_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_rouge_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_blanche_f",32000,"true","",45},
				{"Mrshounka_megane_rs_2015_Rework_p_jaune_f",32000,"true","",45}
				
			};
		};
		
		class 4x4_veh_shop{
			title = "Offroaders Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "4x4_veh_spawn";
			
			vehicles[] = {
				{"ivory_wrx_acid",40000,"true","",25},
				{"ivory_wrx_aquaberry",40000,"true","",25},
				{"ivory_wrx_chrome",40000,"true","",25},
				{"ivory_wrx_gold",40000,"true","",25},
				{"ivory_wrx_Inferno",40000,"true","",25},
				{"ivory_evox_RoseGold",45000,"true","",25},
				{"ivory_evox_Midnight",45000,"true","",25},
				{"ivory_wrx_Magma",40000,"true","",25},
				{"ivory_evox_Gold",45000,"true","",25},
				{"ivory_evox_Acid",45000,"true","",25},
				{"ivory_evox_aquaberry",45000,"true","",25},
				{"ivory_evox_Chrome",45000,"true","",25},
				{"Mrshounka_Bowler_Rework_noir_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_bleu_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_verte_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_rose_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_rouge_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_blanche_f",35000,"true","",65},
				{"Mrshounka_Bowler_Rework_jaune_f",35000,"true","",65},
				{"Mrshounka_hummer_Rework_noir_f",50000,"true","",65},
				{"Mrshounka_hummer_Rework_bleu_f",50000,"true","",65},
				{"Mrshounka_hummer_Rework_verte_f",50000,"true","",65},
				{"Mrshounka_hummer_Rework_rose_f",50000,"true","",65},
				{"Mrshounka_hummer_Rework_rouge_f",50000,"true","",65},
				{"Mrshounka_hummer_Rework_blanche_f",50000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_noir_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_bleu_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_verte_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_rose_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_rouge_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_blanche_f",70000,"true","",65},
				{"Mrshounka_jeep_blinde_Rework_jaune_f",70000,"true","",65}
				
			};
		};
		
		class mercedes_veh_shop{
			title = "Mercedes Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "merc_veh_spawn";
			
			vehicles[] = {
				{"A45lu",25000,"true","A45 White",25},
				{"A45lu_civ_civilgris",25000,"true","A45 Grey",25},
				{"A45lu_civ_civilbleu",25000,"true","A45 Blue",25},
				{"A45lu_civ_civilyellow",25000,"true","A45 Yellow",25},
				{"A45lu_civ_civilnoir",25000,"true","A45 Black",25},
				{"A45lu_civ_civilrouge",25000,"true","A45 Red",25},
				{"ML63lu_civ",100000,"true","ML63 White",75},
				{"ML63lu_civ_gris",100000,"true","ML63 Grey",75},
				{"ML63lu_civ_bleu",100000,"true","ML63 Blue",75},
				{"ML63lu_civ_yellow",100000,"true","ML63 Yellow",75},
				{"ML63lu_civ_noir",100000,"true","ML63 Black",75},
				{"ML63lu_civ_rouge",100000,"true","ML63 Red",75},
				{"shounka_a3_spr_civ_rouge",22000,"true","Sprinter Red",140},
				{"shounka_a3_spr_civ_bleufonce",22000,"true","Sprinter Blue",140},
				{"shounka_a3_spr_civ_jaune",22000,"true","Sprinter Yellow",140},
				{"shounka_a3_spr_civ_noir",22000,"true","Sprinter Black",140},
				{"shounka_a3_spr_civ_orange",22000,"true","Sprinter Orange",140},
				{"shounka_a3_spr_civ_rose",22000,"true","Sprinter Pink",140},
				{"shounka_a3_spr_civ_violet",22000,"true","Sprinter Purple",140}
				
			};
		};
		
		class ford_veh_shop{
			title = "Ford Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "ford_veh_spawn";
			
			vehicles[] = {
				{"Explorerlu_civ",32000,"true","Explorer White",50},
				{"Explorerlu_civ_bleu",32000,"true","Explorer Blue",50},
				{"Explorerlu_civ_gris",32000,"true","Explorer Grey",50},
				{"Explorerlu_civ_yellow",32000,"true","Explorer Yellow",50},
				{"Explorerlu_civ_noir",32000,"true","Explorer Black",50},
				{"Explorerlu_civ_rouge",32000,"true","Explorer Red",50},
				{"Mk3lu_civ",18000,"true","Focus MK3 White",25},
				{"Mk3lu_civ_bleu",18000,"true","Focus MK3 Blue",25},
				{"Mk3lu_civ_gris",18000,"true","Focus MK3 Grey",25},
				{"Mk3lu_civ_yellow",18000,"true","Focus MK3 Yellow",25},
				{"Mk3lu_civ_noir",18000,"true","Focus MK3 Black",25},
				{"Mk3lu_civ_rouge",18000,"true","Focus MK3 Red",25},
				{"Mustanglu_civ",45000,"true","Mustang GTR White",25},
				{"Mustanglu_civ_bleu",45000,"true","Mustang GTR Blue",25},
				{"Mustanglu_civ_gris",45000,"true","Mustang GTR Grey",25},
				{"Mustanglu_civ_yellow",45000,"true","Mustang GTR Yellow",25},
				{"Mustanglu_civ_noir",45000,"true","Mustang GTR Black",25},
				{"Mustanglu_civ_rouge",45000,"true","Mustang GTR Red",25},
				{"Raptorlu_civ",37500,"true","Raptor White",80},
				{"Raptorlu_civ_bleu",37500,"true","Raptor Blue",80},
				{"Raptorlu_civ_gris",37500,"true","Raptor Grey",80},
				{"Raptorlu_civ_yellow",37500,"true","Raptor Yellow",80},
				{"Raptorlu_civ_noir",37500,"true","Raptor Black",80},
				{"Raptorlu_civ_rouge",37500,"true","Raptor Red",80},
				{"GT500lu_civ",60000,"true","Shelby GT500 White",20},
				{"GT500lu_civ_bleu",60000,"true","Shelby GT500 Blue",20},
				{"GT500lu_civ_gris",60000,"true","Shelby GT500 Grey",20},
				{"GT500lu_civ_yellow",60000,"true","Shelby GT500 Yellow",20},
				{"GT500lu_civ_noir",60000,"true","Shelby GT500 Black",20},
				{"GT500lu_civ_rouge",60000,"true","Shelby GT500 Red",20}
				
			};
		};
		
		class dodge_veh_shop{
			title = "Dodge Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "dodge_veh_spawn";
			
			vehicles[] = {
				{"SRT8_12lu_civ",45000,"true","",25},
				{"SRT8_12lu_civ_bleu",45000,"true","",25},
				{"SRT8_12lu_civ_gris",45000,"true","",25},
				{"SRT8_12lu_civ_yellow",45000,"true","",25},
				{"SRT8_12lu_civ_noir",45000,"true","",25},
				{"SRT8_12lu_civ_rouge",45000,"true","",25},
				{"Charger_1969lu_Race",37500,"true","",25},
				{"Charger_1969lu_Race_Bleu",37500,"true","",25},
				{"Charger_1969lu_Race_gris",37500,"true","",25},
				{"Charger_1969lu_Race_jaune",37500,"true","",25},
				{"Charger_1969lu_Race_noir",37500,"true","",25},
				{"Charger_1969lu_Race_rouge",37500,"true","",25},
				{"Charger_1969lu_white",15000,"true","",25},
				{"Charger_1969lu_bleu",15000,"true","",25},
				{"Charger_1969lu_gris",15000,"true","",25},
				{"Charger_1969lu_yellow",15000,"true","",25},
				{"Charger_1969lu_noir",15000,"true","",25},
				{"Charger_1969lu_rouge",15000,"true","",25}
			};
		};
		
		class second_veh_shop{
			title = "Second Hand Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "second_veh_spawn";
			
			vehicles[] = {
				{"Jonzie_Mini_Cooper",1500,"true","",25},
				{"Jonzie_Mini_Cooper_R_spec",5000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_noir_f",15000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_bleu_f",15000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_verte_f",15000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_rose_f",15000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_rouge_f",15000,"true","",25},
				{"Mrshounka_cayenne_c_Rework_blanche_f",15000,"true","",25},
				{"Mrshounka_Volkswagen_Touareg_Rework_jaune_f",15000,"true","",25},
				{"Mrshounka_golfvi_Rework_noir_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_bleu_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_verte_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_rose_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_rouge_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_blanche_f",10000,"true","",25},
				{"Mrshounka_golfvi_Rework_jaune_f",10000,"true","",25},
			};
		};
		
		class lux_veh_shop{
			title = "Luxury Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "lux_veh_spawn";
			
			vehicles[] = {
				{"Model_Slu_civ",125000,"true","",25},
				{"Model_Slu_civ_bleu",125000,"true","",25},
				{"Model_Slu_civ_gris",125000,"true","",25},
				{"Model_Slu_civ_yellow",125000,"true","",25},
				{"Model_Slu_civ_noir",125000,"true","",25},
				{"Model_Slu_civ_rouge",125000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_noir_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_bleu_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_verte_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_rose_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_rouge_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_blanche_f",80000,"true","",25},
				{"Mrshounka_Alfa_Romeo_Brera_Rework_jaune_f",80000,"true","",25},
				{"CHRlu_civ",70000,"true","",25},
				{"CHRlu_civ_bleu",70000,"true","",25},
				{"CHRlu_civ_gris",70000,"true","",25},
				{"CHRlu_civ_yellow",70000,"true","",25},
				{"CHRlu_civ_noir",70000,"true","",25},
				{"CHRlu_civ_rouge",70000,"true","",25},
				{"Wraithlu_civ",450000,"true","",25},
				{"Wraithlu_civ_bleu",450000,"true","",25},
				{"Wraithlu_civ_gris",450000,"true","",25},
				{"Wraithlu_civ_yellow",450000,"true","",25},
				{"Wraithlu_civ_noir",450000,"true","",25},
				{"Wraithlu_civ_rouge",450000,"true","",25},
				{"Panameralu",375000,"true","",25},
				{"Panameralu_bleu",375000,"true","",25},
				{"Panameralu_gris",375000,"true","",25},
				{"Panameralu_yellow",375000,"true","",25},
				{"Panameralu_noir",375000,"true","",25},
				{"Panameralu_rouge",375000,"true","",25},
			};
		};
		
		class sports_veh_shop{
			title = "Sports Car Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "sport_veh_spawn";
			
			vehicles[] = {
				{"Alessio918",500000,"true","",25},
				{"Alessio918C",500000,"true","",25},
				{"Alessio918C1",500000,"true","",25},
				{"Alessio918C2",500000,"true","",25},
				{"Alessio918C3",500000,"true","",25},
				{"Alessio918C4",500000,"true","",25},
				{"Alessio918G",500000,"true","",25},
				{"Alessio918M",500000,"true","",25},
				{"AlessioSuperfast",475000,"true","",25},
				{"AlessioSuperfastC",475000,"true","",25},
				{"AlessioSuperfastG",475000,"true","",25},
				{"AlessioSuperfastMat",475000,"true","",25},
				{"Alessio718",405000,"true","",25},
				{"Alessio718Chrome",405000,"true","",25},
				{"Alessio718_Gold",405000,"true","",25},
				{"Alessio718M",405000,"true","",25},
				{"R35lu_civ",375000,"true","",25},
				{"R35lu_civ_bleu",375000,"true","",25},
				{"R35lu_civ_gris",375000,"true","",25},
				{"R35lu_civ_yellow",375000,"true","",25},
				{"R35lu_civ_noir",375000,"true","",25},
				{"R35lu_civ_rouge",375000,"true","",25},
				{"FFlu_civ",315000,"true","",25},
				{"FFlu_civ_bleu",315000,"true","",25},
				{"FFlu_civ_gris",315000,"true","",25},
				{"FFlu_civ_yellow",315000,"true","",25},
				{"FFlu_civ_noir",315000,"true","",25},
				{"FFlu_civ_rouge",315000,"true","",25},
				{"mrshounka_92_civ",275000,"true","",25},
				{"mrshounka_92_civ_bleufonce",275000,"true","",25},
				{"mrshounka_92_civ_jaune",275000,"true","",25},
				{"mrshounka_92_civ_noir",275000,"true","",25},
				{"mrshounka_92_civ_orange",275000,"true","",25},
				{"mrshounka_92_civ_rose",275000,"true","",25},
				{"mrshounka_92_civ_rouge",275000,"true","",25},
				{"mrshounka_92_civ_violet",275000,"true","",25}
			};
		};
		
		class super_veh_shop{
			title = "Supercar Shop";
			condition = "true";
			simple = 0;
			maxCart = 1;
			spawnPoint = "super_veh_spawn";
			
			vehicles[] = {
				{"AlessioLaFerrari",750000,"true","",25},
				{"AlessioLaFerrariC",750000,"true","",25},
				{"AlessioLaFerrariG",750000,"true","",25},
				{"AlessioLaFerrariMat",750000,"true","",25},
				{"AlessioAventador",725000,"true","",25},
				{"AlessioAventadorC",725000,"true","",25},
				{"AlessioAventadorG",725000,"true","",25},
				{"AlessioAventadorM",725000,"true","",25},
				{"Mrshounka_agera_Rework_p_noir_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_bleu_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_verte_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_rose_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_rouge_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_blanche_f",675000,"true","",25},
				{"Mrshounka_agera_Rework_p_jaune_f",675000,"true","",25},
				{"ivory_lp560_Acid",650000,"true","",25},
				{"ivory_lp560_aquaberry",650000,"true","",25},
				{"ivory_lp560_Chrome",650000,"true","",25},
				{"ivory_lp560_Gold",650000,"true","",25},
				{"ivory_lp560_Magma",650000,"true","",25},
				{"Gallardolu_civ",615000,"true","",25},
				{"Gallardolu_civ_bleu",615000,"true","",25},
				{"Gallardolu_civ_gris",615000,"true","",25},
				{"Gallardolu_civ_yellow",615000,"true","",25},
				{"Gallardolu_civ_noir",615000,"true","",25},
				{"Gallardolu_civ_rouge",615000,"true","",25},
				{"mrshounka_huracan_c",525000,"true","",25},
				{"mrshounka_huracan_c_bleufonce",525000,"true","",25},
				{"mrshounka_huracan_c_grise",525000,"true","",25},
				{"mrshounka_huracan_c_jaune",525000,"true","",25},
				{"mrshounka_huracan_c_noir",525000,"true","",25},
				{"mrshounka_huracan_c_orange",525000,"true","",25},
				{"mrshounka_huracan_c_rose",525000,"true","",25},
				{"mrshounka_huracan_c_rouge",525000,"true","",25}

			};
		};
	};
	
};