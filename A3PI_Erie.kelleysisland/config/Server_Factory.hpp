

class Server_Factory
{
	
	class Recipes
	{
		
		class Items
		{
			
			class Item_SteelIngot
			{
				DisplayName = "Steel Ingot";
				Variable = "Item_SteelIngot";
				Materials[] = {{"Item_IronIngot",2},{"Item_RefinedCoal",1}};
				Products[] = {{"Item_SteelIngot",1}};
			};	
			
			class Item_IronIngot // test
			{
				DisplayName = "Iron Ingot";
				Variable = "Item_IronIngot";
				Materials[] = {{"Item_IronIngot",2},{"Item_RefinedCoal",1}};
				Products[] = {{"Item_SteelIngot",1}};
			};	
			
		};
		
	};
	
};