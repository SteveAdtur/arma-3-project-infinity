//Credit: Steve Adtur / Heisen - Systems,  Simon Bond - Config/Organisation -
#define CADET 1
#define DEPUTY 2
#define CORPORAL 3
#define SERGEANT 4
#define LIEUTENANT 5
#define CAPTAIN 6
#define SERT 7
#define SERTCOMMAND 8
#define UNDERSHERIFF 9
#define SHERIFF 10
#define FBI 11
#define FBICOMMAND 12


#define UNIFORM_TYPE "cfgWeapons"
#define WEAPON_TYPE "cfgWeapons"
#define MAGAZINE_TYPE "cfgMagazines"
#define HEADGEARLINKITEM "linkItem"


#define ITEMSCLASS "Items"
#define UNIFORMS "Uniforms"
#define VESTS "Vests"
#define BACKPACKS "Backpacks"
#define HEADGEARCLASS "Headgear"
#define WEAPONSMAGAZINES "Weapons/Magazines"

class Server_Items
{
	#include "shops\Item_Shops_Police.hpp"
	#include "shops\Item_Shops_GeneralStore.hpp"
	
	Server_Items_Thirst[] = 
	{
		"Item_WaterBottle",
		"Item_CocaCola",
		"Item_Redgull",
		"Item_Coffee"
	};
	Server_Items_Hunger[] = 
	{
		"Item_SixFaggots",
		"Item_Sandwich",
		"Item_Donuts",
		"Item_CookedFox"
	};
	Server_Items_Crafting[] = 
	{
		"Item_A3Rail",
		"Item_A3Reciever",
		"Item_A4Rail",
		"Item_A4Reciever",
		"Item_AK47Barrel",
		"Item_AK47Folding",
		"Item_AK47Reciever",
		"Item_AK74Barrel",
		"Item_AK74Folding",
		"Item_AK74Reciever",
		"Item_AK74Stock",
		"Item_ColtBarrel",
		"Item_ColtReciever",
		"Item_Foregrip",
		"Item_GreenPaint",
		"Item_HK10Rail",
		"Item_HK14Rail",
		"Item_HKReciever",
		"Item_M4Rail",
		"Item_M4Reciever",
		"Item_TanPaint"
	};
	Server_Items_Unrefined[] = 
	{
		"Item_IronOre",
		"Item_CoalOre",
		"Item_WoodLog",
		"Item_Oil",
		"Item_CannabisBud",
		"Item_Psuedo",
		"Item_OpiumFlower",
		"Item_UnpressedBenzo",
		"Item_Sand",
		"Item_GHB"
	};
	Server_Items_Refined[] = 
	{
		"Item_IronIngot",
		"Item_RefinedCoal",
		"Item_TreatedWood",
		"Item_RefinedOil",
		"Item_Cannabis",
		"Item_Amphet",
		"Item_Benzo",
		"Item_Heroin",
		"Item_Glass",
		"Item_GHBWater"
	};
	Server_Items_Seeds[] = 
	{
		"Item_CannabisSeed",
		"Item_OpiumSeed",
		"Item_CottonSeed",
		"Item_BeanSeed",
		"Item_PumpkinSeed",
		"Item_CornSeed",
		"Item_SunflowerSeed",
		"Item_WheatSeed"
	};
	Server_Items_HarvestedPlants[] = 
	{
		"Item_CannabisBud",
		"Item_OpiumFlower",
		"Item_Cotton",
		"Item_Beans",
		"Item_Pumpkin",
		"Item_Corn",
		"Item_Sunflower",
		"Item_Wheat"
	};
	Server_Items_FarmMarket[] = 
	{
		"Item_CottonSeed",
		"Item_Cotton",
		"Item_BeanSeed",
		"Item_Beans",
		"Item_PumpkinSeed",
		"Item_Pumpkin",
		"Item_CornSeed",
		"Item_Corn",
		"Item_SunflowerSeed",
		"Item_Sunflower",
		"Item_WheatSeed",
		"Item_Wheat"
	};
	Server_Items_ResourceTrader[] = 
	{
		"Item_IronIngot",
		"Item_RefinedCoal",
		"Item_TreatedWood",
		"Item_RefinedOil",
		"Item_Glass"
	};
	Server_Items_DrugDealer[] = 
	{
		"Item_CannabisSeed",
		"Item_Cannabis",
		"Item_Benzo",
		"Item_Amphet",
		"Item_OpiumSeed",
		"Item_Heroin",
		"Item_GHBWater"
	};
	Server_Items_Drugs[] = 
	{
		"Item_Heroin",
		"Item_Amphet",
		"Item_Benzo",
		"Item_Redgull",
		"Item_GHBWater"
	};
	
	// START TEST-
	class Item_A3PI_Patrol_Cadet_Uni
	{
		displayName = "ECSO Patrol Cadet Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Cadet_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = CADET;
	};
		
	class Item_A3PI_Patrol_Deputy_Uni
	{
		displayName = "ECSO Patrol Deputy Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Deputy_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = DEPUTY;
	};
	
	class Item_A3PI_Patrol_Corporal_Uni
	{
		displayName = "ECSO Patrol Corporal Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Corporal_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = CORPORAL;
	};
	
	class Item_A3PI_Patrol_Sergeant_Uni
	{
		displayName = "ECSO Patrol Sergeant Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Sergeant_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERGEANT;
	};
	
	class Item_A3PI_Patrol_Lieutenant
	{
		displayName = "ECSO Patrol Lieutenant Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Lieutenant_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = LIEUTENANT;
	};
	
	class Item_A3PI_Patrol_Captain_Uni
	{
		displayName = "ECSO Patrol Captain Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Captain_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = CAPTAIN;
	};
	
	// SERT
	class Item_A3PI_SERT_Urban
	{
		displayName = "ECSO SERT Urban Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Urban";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERT;
	};

	class Item_A3PI_SERT_Uni
	{
		displayName = "ECSO SERT Cadet Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERT;
	};

	class Item_A3PI_SERT_Uni_Ofc
	{
		displayName = "ECSO SERT Officer Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni_Ofc";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERT;
	};

	class Item_A3PI_SERT_Uni_Cpl
	{
		displayName = "ECSO SERT Corporal Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni_Cpl";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERT;
	};

	class Item_A3PI_SERT_Uni_Sgt
	{
		displayName = "ECSO SERT Sergeant Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni_Sgt";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERT;
	};

	class Item_A3PI_SERT_Uni_Lt
	{
		displayName = "ECSO SERT Lieutenant Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni_Lt";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERTCOMMAND;
	}; 

	class Item_A3PI_SERT_Uni_Cpt
	{
		displayName = "ECSO SERT Captain Duty Uniform";
		buyValue = 100;
		sellValue = 50;
		physicalClassName = "A3PI_SERT_Uni_Cpt";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SERTCOMMAND;
	}; 
	// SERT END
	
	class Item_A3PI_Patrol_UnderSheriff_Uni
	{
		displayName = "ECSO UnderSheriff Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_UnderSheriff_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = UNDERSHERIFF;
	};
	
	class Item_A3PI_Patrol_Sheriff_Uni
	{
		displayName = "ECSO Sheriff Duty Uniform";
		buyValue = 110;
		sellValue = 50;
		physicalClassName = "A3PI_Patrol_Sheriff_Uni";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = UNIFORMS;
		whitelistedLevel = SHERIFF;
	};
		
	// ECSO Vests
	class Item_TRYK_V_Bulletproof_ECSO
	{
		displayName = "ECSO Duty Vest";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "TRYK_V_Bulletproof_ECSO";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = VESTS;
		whitelistedLevel = CADET;
	};	
	
	// ECSO Backpacks
	class Item_TRYK_B_Belt_BLK
	{
		displayName = "ECSO Duty Belt";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "TRYK_B_Belt_BLK";
		physicalClassType = UNIFORM_TYPE;
		physicalClassSpecificBracket = BACKPACKS;
		whitelistedLevel = CADET;
	};	
	
	// ECSO Hats
	class Item_pmc_earpiece
	{
		displayName = "ECSO Duty Earpiece";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "pmc_earpiece";
		physicalClassType = HEADGEARLINKITEM;
		physicalClassSpecificBracket = HEADGEARCLASS;
		whitelistedLevel = CADET;
	};	

	class Item_TRYK_H_woolhat_tan
	{
		displayName = "ECSO Woolhat Tan";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "TRYK_H_woolhat_tan";
		physicalClassType = MAGAZINE_TYPE;
		physicalClassSpecificBracket = HEADGEARCLASS;
		whitelistedLevel = CADET;
	};	
	
	// ECSO Weapons & Magazines
	class Item_RH_M4SBR 
	{
		displayName = "";
		buyValue = 250;
		sellValue = 150;
		physicalClassName = "RH_M4SBR";
		physicalClassType = WEAPON_TYPE;
		physicalClassSpecificBracket = WEAPONSMAGAZINES;
		whitelistedLevel = CADET;
	};
		
	class Item_RH_30Rnd_556x45_M855A1
	{
		displayName = "";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "RH_30Rnd_556x45_M855A1";
		physicalClassType = MAGAZINE_TYPE;
		physicalClassSpecificBracket = WEAPONSMAGAZINES;
		whitelistedLevel = CADET;
	};
	
	// Linked Items
	class Item_ItemCompass 
	{
		displayName = "Compass";
		buyValue = 250;
		sellValue = 150;
		physicalClassName = "ItemCompass";
		physicalClassType = HEADGEARLINKITEM;
		physicalClassSpecificBracket = ITEMSCLASS;

	};
		
	class Item_ItemMap
	{
		displayName = "Map";
		buyValue = 25;
		sellValue = 12;
		physicalClassName = "ItemMap";
		physicalClassType = HEADGEARLINKITEM;
		physicalClassSpecificBracket = ITEMSCLASS;
	};
	
	// END TEST=
	
	//Advanced Med 
	class Item_Bandage
	{
		displayName = "STR_Items_Bandage";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Bandage_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Splint
	{
		displayName = "STR_Items_Splint";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_Cast
	{
		displayName = "STR_Items_Cast";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_SutureKit
	{
		displayName = "STR_Items_SutureKit";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_Plaster
	{
		displayName = "STR_Items_Plaster";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.1;
		buyValue = 1;
		sellValue = 0;
	};
	class Item_SurgeryKit
	{
		displayName = "STR_Items_SurgeryKit";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_BroadSpec
	{
		displayName = "STR_Items_BroadSpec";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_Gloves
	{
		displayName = "STR_Items_Gloves";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_Defib
	{
		displayName = "STR_Items_Defib";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Defibrillator_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
	};
	class Item_BloodBagAPos
	{
		displayName = "STR_Items_BloodBagAPos";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "A+";
		validBlood[] = {0,1,4,5};
	};
	class Item_BloodBagOPos
	{
		displayName = "STR_Items_BloodBagOPos";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "O+";
		validBlood[] = {1,5};
	};
	class Item_BloodBagBPos
	{
		displayName = "STR_Items_BloodBagBPos";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "B+";
		validBlood[] = {1,2,5,6};
	};
	class Item_BloodBagABPos
	{
		displayName = "STR_Items_BloodBagABPos";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "AB+";
		validBlood[] = {0,1,2,3,4,5,6,7};
	};
	class Item_BloodBagANeg
	{
		displayName = "STR_Items_BloodBagANeg";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "A-";
		validBlood[] = {4,5};
	};
	class Item_BloodBagONeg
	{
		displayName = "STR_Items_BloodBagONeg";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "O-";
		validBlood[] = {5};
	};
	class Item_BloodBagBNeg
	{
		displayName = "STR_Items_BloodBagBNeg";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "B-";
		validBlood[] = {5,6};
	};
	class Item_BloodBagABNeg
	{
		displayName = "STR_Items_BloodBagABNeg";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_BloodBag_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 5;
		sellValue = 0;
		bloodType = "AB-";
		validBlood[] = {4,5,6,7};
	};
	
	//Advanced Med End
	class Item_Cash
 	{
 	displayName = "STR_Items_Cash";
 	displayIcon = "\A3PI_Textures\Icons\cash_co.paa";
 	displayModel = "Land_Money_F";
 	weightValue = 0;
 	};
	
	class Item_Pickaxe
	{
		displayName = "physicalItem";
		buyValue = 50;
		physicalClass = "A3PI_Pickaxe2017";
		physicalType = "Rifle";
	};
	class Item_Hatchet
	{
		displayName = "physicalItem";
		buyValue = 50;
		physicalClass = "A3PI_Hatchet2017";
		physicalType = "Rifle";
	};
		
	class Item_Spade
	{
		displayName = "physicalItem";
		buyValue = 50;
		physicalClass = "A3PI_Spade2017";
		physicalType = "Rifle";
	};
		
	class Item_Swing
	{
		displayName = "physicalItem";
		buyValue = 50;
		physicalClass = "Pickaxe_Swing";
		physicalType = "Mag";
	};
		
		
	
 	class Item_SpareWheel
	{
		displayName = "STR_Items_SpareWheel";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Tyre_F";
		weightValue = 20;
		buyValue = 100;
		sellValue = 0;
	};
 	class Item_Sand
	{
		displayName = "STR_Items_Sand";
		displayIcon = "\Life_Client\Icons\Default\ico_sand.paa";
		displayModel = "Land_BarrelSand_F";
		weightValue = 2;
		refineryStats[] = {2,1};
		refined = "Item_Glass";
		RefineryType = "Smelt";
	};
	class Item_Glass
	{
		displayName = "STR_Items_Glass";
		displayIcon = "\Life_Client\Icons\Default\ico_sand.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		sellValue = 200;
		buyValue = 0;
	};
	
	class Item_WaterBottle
	{
		displayName = "STR_Items_WaterBottle";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_BottlePlastic_V2_F";
		buyValue = 1.31;
		sellValue = 1.00;
		weightValue = 0.5;
		thirst = 10;
	};
	class Item_Redgull
	{
		displayName = "STR_Items_Redgull";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Can_V3_F";
		buyValue = 5;
		sellValue = 1;
		weightValue = 0.5;
		thirst = 20;
	};
	class Item_CocaCola
	{
		displayName = "STR_Items_CocaCola";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Can_V2_F";
		buyValue = 2;
		sellValue = 1;
		weightValue = 0.5;
		thirst = 10;
	};
	class Item_Coffee
	{
		displayName = "STR_Items_Coffee";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 10;
		sellValue = 1;
		weightValue = 0.5;
		thirst = 50;
	};
	class Item_SixFaggots
	{
		displayName = "STR_Items_SixFaggots";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 2;
		sellValue = 1;
		weightValue = 0.5;
		hunger = 10;
	};
	class Item_CookedFox
	{
		displayName = "STR_Items_CFox";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 5;
		sellValue = 1;
		weightValue = 0.5;
		hunger = 20;
	};
	class Item_Sandwich
	{
		displayName = "STR_Items_Sandwich";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 3;
		sellValue = 1;
		weightValue = 0.5;
		hunger = 25;
	};
	class Item_Donuts
	{
		displayName = "STR_Items_Donut";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 10;
		sellValue = 1;
		weightValue = 0.5;
		hunger = 50;
	};
	class Item_IronOre
	{
		displayName = "STR_Items_IronOre";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_Bits_Rock_S_Iron";
		weightValue = 1;
		refineryStats[] = {3,1};
		refined = "Item_IronIngot";
		RefineryType = "Smelt";
	};
	class Item_CoalOre
	{
		displayName = "STR_Items_CoalOre";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_Bits_Rock_S_Coal";
		weightValue = 1;
		refineryStats[] = {2,1};
		refined = "Item_RefinedCoal";
		RefineryType = "Smelt";
	};
	class Item_IronIngot
	{
		displayName = "STR_Items_IronIngot";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; 
		weightValue = 1;
		sellValue = 240;
		buyValue = 0;
	};
	class Item_SteelIngot
	{
		displayName = "STR_Items_SteelIngot";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; 
		weightValue = 1;
		sellValue = 240;
		buyValue = 0;
	};
	class Item_RefinedCoal
	{
		displayName = "STR_Items_RefinedCoal";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_RefinedCoal"; // NEED MODEL
		weightValue = 1;
		sellValue = 150;
		buyValue = 0;
	};
	class Item_TreatedWood
	{
		displayName = "STR_Items_TreatedWood";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_TreatedWood"; // NEED MODEL
		weightValue = 3;
		sellValue = 200;
		buyValue = 0;
	};
	class Item_WoodLog
	{
		displayName = "STR_Items_WoodLog";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_WoodLog";
		refineryStats[] = {2,1};
		refined = "Item_TreatedWood";
		RefineryType = "LumberFac";
		weightValue = 6;
	};
	class Item_Oil
	{
		displayName = "STR_Items_Oil";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_RefinedOil"; // NEED MODEL
		refineryStats[] = {5,1};
		refined = "Item_RefinedOil";
		RefineryType = "ChemPlant";
		weightValue = 0.5;
		
	};
	class Item_RefinedOil
	{
		displayName = "STR_Items_RefinedOil";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_RefinedOil"; // NEED MODEL
		weightValue = 0.5;
		sellValue = 200;
		buyValue = 0;
	};
	
	class Item_CottonSeed
	{
		displayName = "STR_Items_CottonSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 0.5;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Cotton
	{
		displayName = "STR_Items_Cotton";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 75;
	};
	
	class Item_RifleBag
	{
		displayName = "STR_Items_RifleBag";
		displayIcon = "\A3PI_Textures\Icons\riflebag.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 250;
		sellValue = 0;
	};
	
	class Item_BeanSeed
	{
		displayName = "STR_Items_BeanSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 1;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Beans
	{
		displayName = "STR_Items_Beans";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 75;
	};
	class Item_PumpkinSeed
	{
		displayName = "STR_Items_PumpkinSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 1;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Pumpkin
	{
		displayName = "STR_Items_Pumpkin";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 2;
		buyValue = 0;
		sellValue = 75;
	};
	class Item_CornSeed
	{
		displayName = "STR_Items_CornSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 1;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Corn
	{
		displayName = "STR_Items_Corn";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 75;
	};
	class Item_SunflowerSeed
	{
		displayName = "STR_Items_SunflowerSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 1;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Sunflower
	{
		displayName = "STR_Items_Sunflower";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 75;
	};
	class Item_WheatSeed
	{
		displayName = "STR_Items_WheatSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 1;
		buyValue = 10;
		sellValue = 0;
	};
	class Item_Wheat
	{
		displayName = "STR_Items_Wheat";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 65;
	};
	
	class Item_CannabisSeed
	{
		displayName = "STR_Items_CannabisSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 2;
		buyValue = 85;
		sellValue = 0;
	};
	class Item_CannabisBud
	{
		displayName = "STR_Items_CannabisBud";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		refineryStats[] = {1,1};
		refined = "Item_Cannabis";
		RefineryType = "DrugProc";
		weightValue = 1;
	};
	class Item_Cannabis
	{
		//Sellable Prodcut
		displayName = "STR_Items_Cannabis";
		displayIcon = "\A3PI_Textures\Icons\weed.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 300;
	};
	class Item_OpiumSeed
	{
		displayName = "STR_Items_OpiumSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3PI_SeedBox_Cannabis"; 
		weightValue = 2;
		buyValue = 85;
		sellValue = 0;
	};
	class Item_OpiumFlower
	{
		displayName = "STR_Items_OpiumFlower";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		refineryStats[] = {1,1};
		refined = "Item_Heroin";
		RefineryType = "DrugProc";
		weightValue = 1;
	};
	class Item_Heroin
	{
		//Sellable Prodcut
		displayName = "STR_Items_Heroin";
		displayIcon = "\A3PI_Textures\Icons\heroin_bag.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 350;
	};
	class Item_Psuedo
	{
		displayName = "STR_Items_Psuedo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 2;
		refineryStats[] = {2,1};
		refined = "Item_Amphet";
		RefineryType = "DrugProc";
	};
	class Item_Amphet
	{
		displayName = "STR_Items_Amphet";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 450;
	};
	class Item_UnpressedBenzo
	{
		displayName = "STR_Items_UnBenzo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 2;
		refineryStats[] = {2,1};
		refined = "Item_Benzo";
		RefineryType = "DrugProc";
	};
	class Item_Benzo
	{
		displayName = "STR_Items_Benzo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 450;
	};
	class Item_GHB
	{
		displayName = "STR_Items_UnGHB";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 2;
		refineryStats[] = {1,1};
		refined = "Item_GHBWater";
		RefineryType = "DrugProc";
	};
	class Item_GHBWater
	{
		displayName = "STR_Items_GHB";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 1;
		buyValue = 0;
		sellValue = 255;
	};
	class Item_Handcuff_Normal
	{
		displayName = "STR_Items_HandCuff_Normal";
		displayIcon = "\A3PI_Textures\Icons\handcuffs.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
		physicalType = "VirtualItem";
	};
	class Item_Handcuff_Kinky
	{
		displayName = "STR_Items_HandCuff_Kinky";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Zipties
	{
		displayName = "STR_Items_Zipties";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Toolkit
	{
		displayName = "STR_Items_Toolkit";
		displayIcon = "\A3PI_Textures\Icons\repairkit.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 10;
		buyValue = 50;
		sellValue = 0;
	};
	
	
	// Pistols - RH Weapons
class Item_Taser_26_tw
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "Taser_26_tw";
    physicalType = "Pistol";
    copRank = "Cadet";
};

class Item_RH_Glock18
{
    displayName = "physicalItem";
    buyValue = 2000;
    physicalClass = "RH_g18";
    physicalType = "Pistol";
    
};
class Item_RH_Glock22
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_g22";
    physicalType = "Pistol";
	copRank = "Cadet";
};
class Item_RH_p220
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_p220";
    physicalType = "Pistol";
	copRank = "Cadet";
};
class Item_RH_m9
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_m9";
    physicalType = "Pistol";
	copRank = "Cadet";
};
class Item_RH_m1911
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_m1911";
    physicalType = "Pistol";
	copRank = "Cadet";
};
class Item_RH_ttracker
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_ttracker";
    physicalType = "Pistol";
    copRank = "FBI";
};
class Item_RH_ttracker_g
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_ttracker_g";
    physicalType = "Pistol";
    copRank = "FBI";
};
class Item_RH_gsh18
{
    displayName = "physicalItem";
    buyValue = 1900;
    physicalClass = "RH_gsh18";
    physicalType = "Pistol";
    copRank = "FBI";
};
class Item_RH_kimber
{
    displayName = "physicalItem";
    buyValue = 2500;
    physicalClass = "RH_kimber";
    physicalType = "Pistol";
    copRank = "Sergeant";
};
class Item_RH_kimber_nw
{
    displayName = "physicalItem";
    buyValue = 2500;
    physicalClass = "RH_kimber_nw";
    physicalType = "Pistol";
    copRank = "SERT";
};
class Item_RH_bull
{
    displayName = "physicalItem";
    buyValue = 5000;
    physicalClass = "RH_bull";
    physicalType = "Pistol";
    copRank = "Deputy";
};
class Item_RH_bull_black
{
    displayName = "physicalItem";
    buyValue = 5000;
    physicalClass = "RH_bullb";
    physicalType = "Pistol";
    copRank = "SERT Command";
};
class Item_RH_fn57
{
    displayName = "physicalItem";
    buyValue = 2800;
    physicalClass = "RH_fn57";
    physicalType = "Pistol";
    copRank = "Sergeant";
};
class Item_RH_fnp45
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_fnp45";
    physicalType = "Pistol";
    copRank = "Sergeant";
};
class Item_RH_tec9
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_tec9";
    physicalType = "Pistol";
    copRank = "FBI";
};
class Item_RH_vz61
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_vz61";
    physicalType = "Pistol";
    copRank = "FBI";
};
class Item_RH_deagle
{
    displayName = "physicalItem";
    buyValue = 4000;
    physicalClass = "RH_deagle";
    physicalType = "Pistol";
    copRank = "SERT";
};
class Item_RH_deagle_gold
{
    displayName = "physicalItem";
    buyValue = 4500;
    physicalClass = "RH_deagleg";
    physicalType = "Pistol";
    copRank = "FBI Command";
};
class Item_RH_deagle_silver
{
    displayName = "physicalItem";
    buyValue = 4000;
    physicalClass = "RH_deagles";
    physicalType = "Pistol";
    copRank = "FBI Command";
};
class Item_RH_deagle_black
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_deaglem";
    physicalType = "Pistol";
    copRank = "SERT Command";
};
class Item_RH_usp
{
    displayName = "physicalItem";
    buyValue = 4000;
    physicalClass = "RH_usp";
    physicalType = "Pistol";
    copRank = "SERT Command";
};
class Item_RH_mateba
{
    displayName = "physicalItem";
    buyValue = 4700;
    physicalClass = "RH_mateba";
    physicalType = "Pistol";
    copRank = "SERT Command";
};
// Rifles - RH Weapons
class Item_RH_M16A3
{
    displayName = "physicalItem";
    buyValue = 28000;
    physicalClass = "RH_M16A3";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M16A4
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "RH_M16A4_m";
    physicalType = "Rifle";
    copRank = "Corporal";
};
class Item_RH_M16A4_des
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M16A4_des";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M16A4_wdl
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M16A4_wdl";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4A1_ris
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "RH_M4A1_ris";
    physicalType = "Rifle";
	copRank = "Deputy";
};
class Item_hlc_m4
{
	displayName = "physicalItem";
	buyValue = 1000;
	physicalClass = "hlc_rifle_m4";
	physicalType = "Rifle";
	copRank = "Deputy";
};
class Item_RH_M4A1_ris_des
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M4A1_ris_des";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4A1_ris_wdl
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M4A1_ris_wdl";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_HK416
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_Hk416";
    physicalType = "Rifle";
    copRank = "SERT Command";
};
class Item_RH_HK416s
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_Hk416s";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_HK416c
{
	displayName = "physicalItem";
	buyValue = 500;
	physicalClass = "RH_Hk416c";
	physicalType = "Rifle";
	copRank = "Sergeant";
};
class Item_RH_M4_moe
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M4_moe";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4_moe_green
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M4_moe_g";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4_moe_black
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M4_moe_b";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4_moe_Gold
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "RH_M4_moe_Gold";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_RH_M4_moe_Chrome
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "RH_M4_moe_Chrome";
    physicalType = "Rifle";
    copRank = "FBI";
};
/*
class Item_RH_M4SBR
{
	displayName = "physicalItem";
	buyValue = 1000;
	physicalClass = "RH_M4sbr";
	physicalType = "Rifle";
	copRank = "Cadet";
};
*/
class Item_RH_SBR9
{
	displayName = "physicalItem";
	buyValue = 500;
	physicalClass = "RH_sbr9";
	physicalType = "Rifle";
	copRank = "Cadet";
};
class Item_RH_Mk12mod1
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "RH_Mk12mod1";
    physicalType = "Rifle";
    copRank = "SERT Command";
};
// Rifles - HLC Weapons
class Item_hlc_ak12
{
    displayName = "physicalItem";
    buyValue = 34500;
    physicalClass = "hlc_rifle_ak12";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_colt
{
	displayName = "physicalItem";
	buyValue = 1000;
	physicalClass = "hlc_rifle_Colt727";
	physicalType = "Rifle";
	copRank = "Deputy";
};
class Item_hlc_ak74
{
    displayName = "physicalItem";
    buyValue = 36000;
    physicalClass = "hlc_rifle_ak74";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_aks74
{
    displayName = "physicalItem";
    buyValue = 25000;
    physicalClass = "hlc_rifle_aks74";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_akm
{
    displayName = "physicalItem";
    buyValue = 40000;
    physicalClass = "hlc_rifle_akm";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_mp510
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "hlc_smg_mp510";
    physicalType = "Rifle";
    copRank = "SERT";
};
class Item_hlc_mp5a4
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "hlc_smg_mp5a4";
    physicalType = "Rifle";
    copRank = "SERT";
};
class Item_hlc_bcmjack
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "hlc_rifle_bcmjack";
    physicalType = "Rifle";
    copRank = "SERT Command";
};
class Item_hlc_bcmblackjack
{
    displayName = "physicalItem";
    buyValue = 1000;
    physicalClass = "hlc_rifle_bcmblackjack";
    physicalType = "Rifle";
    copRank = "SERT Command";
};
class Item_hlc_CQBR
{
    displayName = "physicalItem";
    buyValue = 30000;
    physicalClass = "hlc_rifle_CQBR";
    physicalType = "Rifle";
    copRank = "Cadet";
};
class Item_hlc_CQBR_Gold
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_Gold";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_Chrome
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_Chrome";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_Supreme
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_Supreme";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_Gucci
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_Gucci";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_LV
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_LV";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_aqua
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_aqua";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_blue
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_blue";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_MTP
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_MTP";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_navy
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_navy";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_pink
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_pink";
    physicalType = "Rifle";
    copRank = "FBI";
};
class Item_hlc_CQBR_white
{
    displayName = "physicalItem";
    buyValue = 33000;
    physicalClass = "hlc_rifle_CQBR_white";
    physicalType = "Rifle";
    copRank = "FBI";
};

// BP Weapons
class Item_bp_AR15
{
    displayName = "physicalItem";
    buyValue = 15000;
    physicalClass = "BP_M16OLD";
    physicalType = "Rifle";
    copRank = "Sergeant";
};

class Item_bp_CZ452
{
    displayName = "physicalItem";
    buyValue = 12500;
    physicalClass = "BP_CZ455s";
    physicalType = "Rifle";
    copRank = "SERT";
};

class Item_bp_Enfield
{
    displayName = "physicalItem";
    buyValue = 1400;
    physicalClass = "BP_LeeEnfield2";
    physicalType = "Rifle";
    copRank = "FBI";
};

class Item_bp_Kar
{
    displayName = "physicalItem";
    buyValue = 15000;
    physicalClass = "BP_Kar98";
    physicalType = "Rifle";
    copRank = "FBI";
};

class Item_bp_Lupara
{
    displayName = "physicalItem";
    buyValue = 13000;
    physicalClass = "BP_Lupara";
    physicalType = "Rifle";
    copRank = "FBI";
};

class Item_bp_Garand
{
    displayName = "physicalItem";
    buyValue = 14600;
    physicalClass = "BP_GarandU";
    physicalType = "Rifle";
    copRank = "FBI";
};

class Item_bp_Crossbow
{
    displayName = "physicalItem";
    buyValue = 7500;
    physicalClass = "BP_Crossbow";
    physicalType = "Rifle";
    copRank = "FBI Command";
};

class Item_bp_Benelli
{
    displayName = "physicalItem";
    buyValue = 5550;
    physicalClass = "BP_Benelli";
    physicalType = "Rifle";
    copRank = "Cadet";
};

class Item_bp_Remington
{
    displayName = "physicalItem";
    buyValue = 5550;
    physicalClass = "BP_Rem870";
    physicalType = "Rifle";
    copRank = "Cadet";
};

class Item_bp_M4MK
{
    displayName = "physicalItem";
    buyValue = 1250;
    physicalClass = "BP_M4_300MK";
    physicalType = "Rifle";
    copRank = "SERT Command";
};

class Item_bp_M9Training
{
    displayName= "physicalItem";
    buyValue = 1000;
    physicalClass = "BP_m9Training";
    physicalType = "Pistol";
    copRank = "Sergeant";
};

class Item_bp_RugerTraining
{
    displayName = "physicalItem";
    buyValue = 700;
    physicalClass = "BP_RugerTraining";
    physicalType = "Rifle";
    copRank = "Sergeant";
};

class Item_bp_Ruger
{
    displayName = "physicalItem";
    buyValue = 7000;
    physicalClass = "BP_Ruger";
    physicalType = "Rifle";
};

class Item_bp_Springfield
{
    displayName = "physicalItem";
    buyValue = 27500;
    physicalClass = "BP_M1903";
    physicalType = "Rifle";
};

class Item_bp_SVT
{
    displayName = "physicalItem";
    buyValue = 14000;
    physicalClass = "BP_SVT40";
    physicalType = "Rifle";
};

class Item_bp_1866
{
    displayName = "physicalItem";
    buyValue = 7001;
    physicalClass = "BP_1866";
    physicalType = "Rifle";
};

class Item_bp_1866Collector
{
    displayName = "physicalItem";
    buyValue = 9012;
    physicalClass = "BP_1866C";
    physicalType = "Rifle";
};

class Item_BP_R700
{
    displayName = "physicalItem";
    buyValue = 7000;
    physicalClass = "BP_R700";
    physicalType = "Rifle";
    copRank = "SERT Command";
};

class Item_bp_M93R
{
    displayName = "physicalItem";
    buyValue = 2400;
    physicalClass = "BP_m9Tac";
    physicalType = "Pistol";
    copRank = "FBI";
};

class Item_bp_Uzi
{
    displayName = "physicalItem";
    buyValue = 3532;
    physicalClass = "BP_Uzi";
    physicalType = "Pistol";
    copRank = "FBI";
};

class Item_bp_SA61
{
    displayName = "physicalItem";
    buyValue = 4444;
    physicalClass = "BP_SA61";
    physicalType = "Pistol";
    copRank = "FBI";
};

class Item_bp_SW45
{
    displayName = "physicalItem";
    buyValue = 2312;
    physicalClass = "BP_SW45";
    physicalType = "Pistol";
    copRank = "FBI";
};

//A3PI Rifles
class Item_A3PI_AK47sgold
{
    displayName = "physicalItem";
    buyValue = 50000;
    physicalClass = "A3PI_AK47sgold";
    physicalType = "Rifle";
    copRank = "FBI";
};

//RH Mags
class Item_RH_Deagle_Mag
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "RH_7Rnd_50_AE";
    physicalType = "Mag";
    copRank = "SERT";
};

class Item_RH_USP40_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_16Rnd_40cal_usp";
    physicalType = "Mag";
    copRank = "SERT";
};

class Item_RH_USP45_Mag
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "RH_12Rnd_45cal_usp";
    physicalType = "Mag";
    copRank = "SERT";
};

class Item_RH_FNP45_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_15Rnd_45cal_fnp";
    physicalType = "Mag";
    copRank = "Sergeant";
};

class Item_RH_Glock17_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_17Rnd_9x19_g17";
    physicalType = "Mag";
    copRank = "Cadet";
};

class Item_RH_Glock18_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_33Rnd_9x19_g18";
    physicalType = "Mag";
};

class Item_RH_Glock22_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_17Rnd_40SW_g22";
    physicalType = "Mag";
	copRank = "Cadet";
};

class Item_RH_M9Baretta_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_15Rnd_9x19_M9";
    physicalType = "Mag";
	copRank = "Cadet";
};

class Item_A3PI_1911_Mag
{
	displayName = "physicalItem";
	buyValue = 150;
	physicalClass = "A3PI_1911Mag";
	physicalType = "Mag";
	copRank = "Cadet";
};

class Item_RH_M1911_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_7Rnd_45cal_m1911";
    physicalType = "Mag";
	copRank = "Cadet";
};

class Item_RH_454Pistol_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_6Rnd_454_Mag";
    physicalType = "Mag";
    copRank = "Deputy";
};

class Item_RH_GSH_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_18Rnd_9x19_gsh";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_RH_VP_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_18Rnd_9x19_VP";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_RH_Sig45_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_15Rnd_45ACP_SIG";
    physicalType = "Mag";
    copRank = "Cadet";
};

class Item_RH_45ACP_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_6Rnd_45ACP_Mag";
    physicalType = "Mag";
    copRank = "Deputy";
};

class Item_RH_FN57_Mag
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RH_20Rnd_57x28_FN";
    physicalType = "Mag";
    copRank = "Sergeant";
};

class Item_RH_9mmM822_Mag
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "RH_32Rnd_9mm_M822";
    physicalType = "Mag";
	copRank = "Cadet";
};

class Item_RH_Tec9_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_32Rnd_9x19_tec";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_RH_VZScorpion_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "RH_20Rnd_32cal_vz61";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_RH_556Mk318_Mag
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "RH_30Rnd_556x45_Mk318";
    physicalType = "Mag";
    copRank = "Cadet";
};

class Item_RH_556M855A1_Mag
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "RH_30Rnd_556x45_M855A1";
    physicalType = "Mag";
    copRank = "Cadet";
};

//HLC Mags
class Item_hlc_AK545_Mag
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hlc_30Rnd_545x39_B_AK";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_hlc_AK762_Mag
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hlc_30Rnd_762x39_b_ak";
    physicalType = "Mag";
    copRank = "FBI";
};

class Item_hlc_556Stanag_Mag
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "30Rnd_556x45_Stanag";
    physicalType = "Mag";
    copRank = "Cadet";
};

class Item_hlc_556M855A1_Mag
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "RH_30Rnd_556x45_M855A1";
    physicalType = "Mag";
	copRank = "Cadet";
};

class Item_hlc_556EPR_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "hlc_30rnd_556x45_EPR";
    physicalType = "Mag";
    copRank = "Deputy";
};

class Item_hlc_MP510_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "hlc_30Rnd_10mm_B_MP5";
    physicalType = "Mag";
    copRank = "SERT";
};

class Item_hlc_MP5a4_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "hlc_30Rnd_9x19_GD_MP5";
    physicalType = "Mag";
    copRank = "SERT";
};

class Item_hlc_300BLK_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "29rnd_300BLK_STANAG";
    physicalType = "Mag";
    copRank = "SERT Command";
};

//A3PI Mags
class Item_A3PI_26Taser_Mag
{
    displayName = "physicalItem";
    buyValue = 30;
    physicalClass = "26_cartridge_b";
    physicalType = "Mag";
	copRank = "Cadet";
};

//BP Mags
class Item_bp_AKM_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_762x39_AKM";
    physicalType = "Mag";
};
class Item_bp_CZ452NL_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_5Rnd_762Rubber_Mag";
    physicalType = "Mag";
	copRank = "SERT";
};
class Item_bp_AR15_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_556x45_Stanag";
    physicalType = "Mag";
};
class Item_bp_CZ452_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_5Rnd_22_Mag";
    physicalType = "Mag";
};
class Item_bp_Enfield_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_10Rnd_303_Mag";
    physicalType = "Mag";
};
class Item_bp_Kar_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_5Rnd_Mauser_Mag";
    physicalType = "Mag";
};
class Item_bp_2Shot_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_2Rnd_Buckshot";
    physicalType = "Mag";
};
class Item_bp_M1_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_8Rnd_3006_Mag";
    physicalType = "Mag";
};

class Item_BP_5Rnd_762x51_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_5Rnd_762x51_Mag";
    physicalType = "Mag";
    copRank = "SERT Command";
};

class Item_bp_Crossbow_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_Arrow_Mag";
    physicalType = "Mag";
    copRank = "SERT Command";
};
class Item_bp_8Shot_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_8Rnd_Buckshot";
    physicalType = "Mag";
	copRank = "Corporal";
};
class Item_bp_4Shot_Mag
{
	displayName = "physicalItem";
	buyValue = 200;
	physicalClass = "BP_4Rnd_Buckshot";
	physicalType = "Mag";
	copRank = "Deputy";
};
class Item_bp_4ShotBeanBag_Mag
{
	displayName = "physicalItem";
	buyValue = 20;
	physicalClass = "BP_4Rnd_NL";
	physicalType = "Mag";
	copRank = "Cadet";
};
class Item_bp_Ruger_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_25Rnd_22_Mag";
    physicalType = "Mag";
};
class Item_bp_Spring_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_5Rnd_3006_Mag";
    physicalType = "Mag";
};
class Item_bp_SVT_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_762x54_SVD";
    physicalType = "Mag";
};
class Item_bp_Winchester_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_7Rnd_45acp";
    physicalType = "Mag";
};
class Item_bp_M93R_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_33Rnd_9x19OVP";
    physicalType = "Mag";
};
class Item_bp_Uzi_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_30Rnd_9x21_Mag";
    physicalType = "Mag";
};
class Item_bp_Skorpion_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_20Rnd_765x17";
    physicalType = "Mag";
};

class Item_bp_SW_Mag
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_6Rnd_45_Mag";
    physicalType = "Mag";
};

// Weapon Accessories

class Item_BP_762Muzzle
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "BP_762Muzzle";
    clothingType = "Item";
	copRank = "SERT Command";
};
class Item_BP_Harris
{
	displayName = "physicalItem";
	buyValue = 50;
	physicalClass = "BP_Harris";
	clothingType = "Item";
	copRank = "SERT Command";
};

//Weapon Parts

class Item_A4Rail
{
    displayName = "STR_Items_A4RailSystem";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "A3PI_A4Rail";
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"}};
};

class Item_A4Reciever
{
    displayName = "STR_Items_A4Reciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "A3PI_A4Reciever";
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"}};
};

class Item_A3Rail
{
    displayName = "STR_Items_A3Rail";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",25,"STR_Items_IronIngot"},{"Item_RefinedCoal",12,"STR_Items_RefinedCoal"}};
};

class Item_A3Reciever
{
    displayName = "STR_Items_A3Reciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",20,"STR_Items_IronIngot"},{"Item_RefinedCoal",10,"STR_Items_RefinedCoal"}};
};

class Item_M4Rail
{
    displayName = "STR_Items_M4Rail";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"}};
};

class Item_M4Reciever
{
    displayName ="STR_Items_M4Reciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"}};
};

class Item_ColtBarrel
{
    displayName ="STR_Items_ColtBarrel";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",20,"STR_Items_IronIngot"},{"Item_RefinedCoal",10,"STR_Items_RefinedCoal"}};
};

class Item_ColtReciever
{
    displayName ="STR_Items_ColtReciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",20,"STR_Items_IronIngot"},{"Item_RefinedCoal",10,"STR_Items_RefinedCoal"}};
};

class Item_Foregrip
{
    displayName ="STR_Items_Foregrip";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"}};
};

class Item_HKReciever
{
    displayName ="STR_Items_HKReciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",60,"STR_Items_IronIngot"},{"Item_RefinedCoal",30,"STR_Items_RefinedCoal"}};
};

class Item_HK10Rail
{
    displayName ="STR_Items_HK10";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",50,"STR_Items_IronIngot"},{"Item_RefinedCoal",25,"STR_Items_RefinedCoal"}};
};

class Item_HK14Rail
{
    displayName ="STR_Items_HK14";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",60,"STR_Items_IronIngot"},{"Item_RefinedCoal",30,"STR_Items_RefinedCoal"}};
};

class Item_GreenPaint
{
    displayName ="STR_Items_GreenPaint";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    buyValue = 10;
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_RefinedOil",10,"STR_Items_RefinedOil"}};
};

class Item_TanPaint
{
    displayName ="STR_Items_TanPaint";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    buyValue = 10;
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_RefinedOil",10,"STR_Items_RefinedOil"}};
};

class Item_AK74Reciever
{
    displayName ="STR_Items_AK74Reciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",20,"STR_Items_RefinedCoal"}};
};

class Item_AK74Barrel
{
    displayName ="STR_Items_AK74Barrel";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"},{"Item_TreatedWood",15,"STR_Items_TreatedWood"}};
};

class Item_AK74Stock
{
    displayName ="STR_Items_AK74Stock";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"},{"Item_TreatedWood",20,"STR_Items_TreatedWood"}};
};

class Item_AK74Folding
{
    displayName ="STR_Items_AK74Folding";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",30,"STR_Items_IronIngot"},{"Item_RefinedCoal",15,"STR_Items_RefinedCoal"}};
};

class Item_AK47Reciever
{
    displayName ="STR_Items_AK47Reciever";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",40,"STR_Items_IronIngot"},{"Item_RefinedCoal",20,"STR_Items_RefinedCoal"}};
};

class Item_AK47Barrel
{
    displayName ="STR_Items_AK47Barrel";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",40,"STR_Items_IronIngot"},{"Item_RefinedCoal",20,"STR_Items_RefinedCoal"},{"Item_TreatedWood",20,"STR_Items_TreatedWood"}};
};

class Item_AK47Folding
{
    displayName ="STR_Items_AK47Folding";
    displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
    displayModel = "Land_Pillow_grey_F"; // NEED MODEL
    weightValue = 0.5;
    materialsNeeded[] = {{"Item_IronIngot",40,"STR_Items_IronIngot"},{"Item_RefinedCoal",20,"STR_Items_RefinedCoal"}};
};

//Blackmarket Clothing
//Uniforms
class Item_TRYK_U_B_PCUHsW3nh
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUHsW3nh";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUHsW3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUHsW3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKBLK_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKBLK_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKBLK_R_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKBLK_R_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKOCP_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKOCP_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKOCP_R_CombatUniformTshirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKOCP_R_CombatUniformTshirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKTAN_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKTAN_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLKTANR_CombatUniformTshirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_BLKTANR_CombatUniformTshirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_ODTAN_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_ODTAN_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_GRYOCP_CombatUniform
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_GRYOCP_CombatUniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_GRYOCP_R_CombatUniformTshirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_GRYOCP_R_CombatUniformTshirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_Bts_PCUs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_Bts_PCUs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_Bts_PCUODs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_Bts_PCUODs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_denim_hood_blk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_denim_hood_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_denim_hood_3c
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_denim_hood_3c";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_denim_hood_mc
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_denim_hood_mc";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_denim_jersey_blu
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_denim_jersey_blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_denim_jersey_blk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_denim_jersey_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUHs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUHs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUGHs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUGHs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUODHs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUODHs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_Wood_PCUs_R
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_Wood_PCUs_R";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUODs
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUODs";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_pad_hood_CSATBlk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_pad_hood_CSATBlk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_nohoodPcu_gry
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_nohoodPcu_gry";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUHsW6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUHsW6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUHsW5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_B_PCUHsW5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_pad_hood_Blk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_pad_hood_Blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_BLK_PAD_BLU3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_shirts_BLK_PAD_BLU3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_BLK_PAD_BL
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_shirts_BLK_PAD_BL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_BLK_PAD_RED2
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_shirts_BLK_PAD_RED2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_BLK_PAD_YEL
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_shirts_BLK_PAD_YEL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_pad_hood_odBK
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "TRYK_U_pad_hood_odBK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_U_I_G_Story_Protagonist_F
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "U_I_G_Story_Protagonist_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_U_I_G_resistanceLeader_F
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "U_I_G_resistanceLeader_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};


//Headgear
class Item_H_Beret_blk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "H_Beret_blk";
    physicalType = "Cloth";
    clothingType = "Item";
};


// Civilian Casual Clothing

//Uniforms
class Item_Journalist
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "U_C_Journalist";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_MiloShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtmilo";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Vegemite
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtvegimite";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Nautica
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtnautica";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_RalphLauren
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtralph";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Starguts
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtstarguts";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_URBlk
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtURBlk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_URWhite
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtURWhite";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_VBShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVB";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Instinct
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtInstinct";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Mystic
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtMystic";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Valor
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtValor";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Harmony
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtHarmony";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CincinZoo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CincinZoo";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_BasedHarambe
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_BasedHarambe";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DicksOut
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_Dicksout";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_GreenPolo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_GreenPoloShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_BluePolo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_BluePoloShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PurplePolo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_PurplePoloShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_BrownPolo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_BrownPoloShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PinkPolo
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_PinkPoloShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DudeOutfit
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_Dude_Outfit";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_FarmerOutfit
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_Farmer_Outfit";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_WorkerOutfit
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_Worker_Outfit";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Shirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Poop2dayShirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Poop2day";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_LogoPants
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_A3PIogoPants";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PervertShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "pervt_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_FlowerShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PIFlowerShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CSGOShirt
{
    displayName = "physicalItem";
    buyValue = 2250;
    physicalClass = "A3PICSGOShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_FuckPopoShirt
{
    displayName = "physicalItem";
    buyValue = 2250;
    physicalClass = "A3PIFuckPopoShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_HempShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIHempShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_KhaledShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIKhaledShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_LeanShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PILeanShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_OGFloralShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIOGFloralShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_RainbowShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIRainbowShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_SkullShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PISkullShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_MericaShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIMericaShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_THCShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PITHCShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_WhiteFloralShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PIWhiteFloral";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CatShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PICatShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CloudShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PICloudShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_HandShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIHandShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PikaShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIPikaShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CokeShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PICokeShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_TigerShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PITigerShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_WolfShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PIWolfShirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_AdidasMakeShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_CivShirtAdidasMake";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_AdidasShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_CivShirtAdidas";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Badoodlenoodleshirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_Badoodlenoodleshirt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_AnotherCatShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtAnotherCat";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Billabong
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_CivShirtBillabong";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Billabong2
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_CivShirtBillabong2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DiamondShirt
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_CivShirtDiamond";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DJShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtDJ";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_GetHighShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtGetHigh";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_HollisterShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtHollister";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_KeepCalmShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtKeepCalm";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_NikeCamoShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtNikeCamo";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_NikeDoIt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtNikeDoIt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_NikeDoIt2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtNikeDoIt2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_NikeFeetShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtNikeFeet";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ObeyShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtObey";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_OhBoyShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtOhBoy";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ProbeShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtProbe";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_RelationshipShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtRelationships";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ThanksObamaShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtThanksObama";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_VansShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVans";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Vans2Shirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVans2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_VolcomLogoShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVolcomLogo";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_VolcomMonsterShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVolcomMonster";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_VolcomShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_CivShirtVolcom";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_OrangePlaid
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "OrangePlaid_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PinkPlaid
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "PinkPlaid_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_RedPlaid
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "RedPlaid_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_BluePlaid
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "BluePlaid_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_YellowPlaid
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "YellowPlaid_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_CheckeredUniform
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "checkered_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_KingfishUniform
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "kingfish_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_MotherTruckerUniform
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "mothertrucker_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_RacerUniform
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "racer_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_TaxiUniform
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "taxi_uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ArmA3White
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "arma3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ArmA3Black
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "arma3black";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_ATIEnthusiast
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "ATI";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_TacoBellShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "tacobell";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DutchShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "dutch";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_HannesAcademyShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "hanacd";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_NvidiaShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "nvidia";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_SFGShirt
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "SFG";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_walkingdeadUniform
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "walkingdead";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_FormalShirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_CivShirtFormal";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Formal2Shirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_CivShirtFormal2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_PrincessShirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_CivShirtPrincess";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Princess2Shirt
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_CivShirtPrincess2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

//TRYK Clothing

class Item_TRYK_OVERALL_SAGE_BLKboots_nk
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_OVERALL_SAGE_BLKboots_nk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_OVERALL_nok_flesh
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_OVERALL_nok_flesh";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_OVERALL_SAGE_BLKboots_nk_blk
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_OVERALL_SAGE_BLKboots_nk_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUGs_BLK
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_PCUGs_BLK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUGs_gry
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_PCUGs_gry";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_PCUGs_OD
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_PCUGs_OD";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLK_T_BK
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_BLK_T_BK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_BLK_T_WH
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_BLK_T_WH";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_Denim_T_WH
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_Denim_T_WH";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_U_B_Denim_T_BK
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_U_B_Denim_T_BK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_BK
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_BK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_BL
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_BL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_BWH
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_BWH";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_od
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_od";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_R
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_R";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_RED2
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_RED2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_ylb
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_ylb";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_BLK_PAD
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_BLK_PAD";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_od_Sleeve
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_od_Sleeve";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_ylb_Sleeve
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_ylb_Sleeve";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_BL_Sleeve
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_BL_Sleeve";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_BWH_Sleeve
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_BWH_Sleeve";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_DENIM_R_Sleeve
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_DENIM_R_Sleeve";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_PAD_BL
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_PAD_BL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_shirts_PAD_RED2
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_shirts_PAD_RED2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

//EF Clothing

class Item_EF_FEM_2BK
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_2BK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_2BK";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPB
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPB";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBL
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBP
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBP";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBPS
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBPS";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBR
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBR";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBW
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBW";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3B
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3B";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3BL
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3BL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3SG
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3SG";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3PP
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3SG";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPB2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPB2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBL2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBL2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBP2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBP2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBPS2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBPS2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBR2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBR2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3LPBW2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3LPBW2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3B2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3B2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3BL2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3BL2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3SG2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3SG2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_3_3PP2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_3_3PP2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_EF_FEM_4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPB
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPB";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBL
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBP
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBP";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBPS
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBPS";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBR
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBR";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBW
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBW";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2B
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2B";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2BL
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2BL";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2SG
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2SG";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2PP
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2PP";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPB2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPB2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBL2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBL2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBP2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBL2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBPS2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBPS2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBR2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBR2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2LPBW2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2LPBW2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2B2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2B2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2BL2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2BL2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2SG2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2SG2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_4_2PP2
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_4_2PP2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_5
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_6
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_FEM_62
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "EF_FEM_62";
    physicalType = "Cloth";
    clothingType = "Uniform";
};


//Civilian Fancy Clothing

class Item_TRYK_SUITS_BLK_F
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_SUITS_BLK_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_TRYK_SUITS_BR_F
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "TRYK_SUITS_BR_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F11
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F11";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F4
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F12
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F12";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F13
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F13";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F14
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F14";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F10
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F10";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F9
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F9";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F8
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F8";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BLK_F
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BLK_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F7
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F7";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F15
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F15";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F16
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F16";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F17
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F17";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F18
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F18";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F19
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F19";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F20
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F20";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F21
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F21";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F22
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F22";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F23
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F23";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F24
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F24";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F25
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F25";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F26
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F26";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F27
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F27";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F28
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F28";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F29
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F29";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F30
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F30";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F31
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F31";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F32
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F32";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F33
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F33";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F34
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F34";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_KAEL_SUITS_BR_F35
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "KAEL_SUITS_BR_F35";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_Priest_Uniform
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "A3PI_Priest_Uniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_1
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_1";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_2
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_3
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_4
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_5
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_suit_6
{
    displayName = "physicalItem";
    buyValue = 400;
    physicalClass = "EF_suit_6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt22
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt22";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2_2
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2_2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2_22
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2_22";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2_3
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2_3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2_32
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2_32";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt2_4
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt2_4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt3
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt32
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "EF_M_jkt32";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt32_2
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt32_2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt4
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_EF_M_jkt42
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "EF_M_jkt42";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_gucci_blk
{
    displayName = "physicalItem";
    buyValue = 350;
    physicalClass = "hood_gucci_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_gucci_beg
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_gucci_beg";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_gucci_brr
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_gucci_brr";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_riffraff
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_riffraff";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_supreme
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_supreme";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_tri
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_tri";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_space
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "hood_space";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_blk
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_ylw
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_ylw";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_red
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_red";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_org
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_org";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_grn
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_grn";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_gr
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_gr";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_blu
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_hood_nike_wh
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "hood_nike_wh";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral1
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral1";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral2
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral4
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral7
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral7";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Floral8
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Floral8";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid1
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid1";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid2
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid4
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid7
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid7";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Denim_Plaid8
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Denim_Plaid8";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral1
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral1";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral2
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_Item_B_Shirts_Tan_Floral4
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral7
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral7";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Floral8
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Floral8";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid1
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid1";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid2
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid2";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid3
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid3";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid4
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid4";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid5
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid5";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid6
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid6";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid7
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid7";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_B_Shirts_Tan_Plaid8
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "B_Shirts_Tan_Plaid8";
    physicalType = "Cloth";
    clothingType = "Uniform";
};


//Headgear
class Item_Bandanna_khk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Bandanna_khk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_EF_wig_DHB
{
    displayName = "physicalItem";
    buyValue = 75;
    physicalClass = "EF_wig_DHB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_Legoman
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Legoman";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_soa_helmet
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_soa_helmet";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_Ash_Cap
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Ash_Cap";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_party_hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "party_hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_rangehat
{
    displayName = "physicalItem";
    buyValue = 75;
    physicalClass = "rangehat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_rangehatpimp
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "rangehatpimp";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_gangster_hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_gangster_hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_Mask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Mask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_longhairblack
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_longhairblack";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_longhairblond
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_longhairblond";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_longhairbrown
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_longhairbrown";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_Crown
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_Crown";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_cowboyhat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "cowboyhat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_mexicanhat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_mexicanhat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_santahat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "santahat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_party_hatrb
{
    displayName = "physicalItem";
    buyValue = 25;
    physicalClass = "party_hatrb";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_elfhat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "elfhat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_Sombrero
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Sombrero";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_turban
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "turban";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_russianhat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_russianhat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_tan";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_blk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_blk_CMMG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_blk_CMMG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_brn_SPECOPS
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_brn_SPECOPS";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_tan_specops_US
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_tan_specops_US";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_khaki_specops_UK
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_khaki_specops_UK";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_grn
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_grn";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_grn_BI
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_grn_BI";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_blk_Raven
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_blk_Raven";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_Bandana_H
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_Bandana_H";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_Bandana_wig
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_Bandana_wig";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_Bandana_wig_g
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_Bandana_wig";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_R_CAP_BLK
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_R_CAP_BLK";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_R_CAP_TAN
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_R_CAP_TAN";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_R_CAP_OD_US
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_R_CAP_OD_US";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_woolhat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_woolhat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_woolhat_br
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_woolhat_br";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_woolhat_cu
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_woolhat_cu";
    physicalType = "Cloth";
    clothingType = "Item";
};

/*
class Item_TRYK_H_woolhat_tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_woolhat_tan";
    physicalType = "Cloth";
    clothingType = "Item";
};
*/
class Item_TRYK_H_wig
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_wig";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_H_woolhat_WH
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_woolhat_WH";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_blk_ION
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_blk_ION";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_white100
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_white100";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_black100
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_black100";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_floral1
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_floral1";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_floral2
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_floral2";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_bluecrack
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_bluecrack";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_boonie_galaxy
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "boonie_galaxy";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_woolhat_louisv
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "woolhat_louisv";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_JM_Goat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "JM_Goat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_JM_Party
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "JM_Party";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_JM_sheep
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "JM_Sheep";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Afro_Hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Kio_Afro_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Capital_Hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Kio_Capital_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Kio_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_No1_Hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Kio_No1_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Pirate_Hat
{
    displayName = "physicalItem";
    buyValue = 75;
    physicalClass = "Kio_Pirate_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Santa_Hat
{
    displayName = "physicalItem";
    buyValue = 75;
    physicalClass = "Kio_Santa_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Kio_Spinning_Hat
{
    displayName = "physicalItem";
    buyValue = 75;
    physicalClass = "Kio_Spinning_Hat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_kio_vfv_mask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "kio_vfv_mask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_ShrekMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_ShrekMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_MickeyMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_MickeyMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_UltronMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_UltronMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_PatriotMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_PatriotMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_AntmanMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_AntmanMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_BobaMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_BobaMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_AugustusMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_AugustusMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_RAAMMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_RAAMMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_HulkMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_HulkMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_VenomMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_VenomMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_SpongebobMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_SpongebobMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_SquidwardMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_SquidwardMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_JasonMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_JasonMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_DeadpoolMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_DeadpoolMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_GingerbreadMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_GingerbreadMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_KermitMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_KermitMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_SpidermanMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_SpidermanMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_StarFoxMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_StarFoxMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_BatmanMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_BatmanMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_ScarecrowMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_ScarecrowMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_GuyFawkesMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_GuyFawkesMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_WashingtonMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_WashingtonMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_LincolnMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_LincolnMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_GrantMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_GrantMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_NixonMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_NixonMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_ClintonMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_ClintonMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_BushMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_BushMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_ObamaMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_ObamaMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_FranklinMask
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_FranklinMask";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_FDNY_Cap_Tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "FDNY_Cap_Tan";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_FDNY_Cap_Black
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "FDNY_Cap_Black";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_FDNY_Cap_White
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "FDNY_Cap_White";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_FDNY_Cap_Grey
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "FDNY_Cap_Grey";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_FDNY_Cap_Navy
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "FDNY_Cap_Navy";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_Blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_Blk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_Tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_Tan";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_White
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_White";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_Navy
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_Navy";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_LBlue
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_LBlue";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_Pink
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_Pink";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Polo_Cap_Red
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Polo_Cap_Red";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Navy
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Navy";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Blk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_White
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_White";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Orange
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Orange";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Grey
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Grey";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Blue
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Blue";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Green
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Green";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Red
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Red";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_Nike_Cap_Yellow
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Nike_Cap_Yellow";
    physicalType = "Cloth";
    clothingType = "Item";
};

//Goggles
class Item_A3PI_Balaclava
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Balaclava";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Shades_Black
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Shades_Black";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Shades_Blue
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Shades_Blue";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Sport_Blackred
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Sport_Blackred";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Sport_Checkered
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Sport_Checkered";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Sport_Blackyellow
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Sport_Blackyellow";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Sport_BlackWhite
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Sport_BlackWhite";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Squares
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Squares";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Aviator
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "G_Aviator";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Lady_Mirror
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "G_Lady_Mirror";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Lady_Dark
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "G_Lady_Dark";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Lady_Blue
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "G_Lady_Blue";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Lowprofile
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "G_Lowprofile";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Combat
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "G_Combat";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_smallBeardB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_smallBeardB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_smallBeardD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_smallBeardD";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_smallBeardG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_smallBeardG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_smallBeardO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_smallBeardO";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_BeardB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_BeardB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_BeardD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_BeardD";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_BeardG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_BeardG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_BeardO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_BeardO";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_chinlessbB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_chinlessbB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_chinlessbD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_chinlessbD";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_chinlessbG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_chinlessbG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_chinlessbO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_chinlessbO";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_moustacheB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_moustacheB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_moustacheD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_moustacheD";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_moustacheG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_moustacheG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_moustacheO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_moustacheO";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_ChopsB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_ChopsB";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_ChopsD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_ChopsD";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_ChopsG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_ChopsG";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_SFG_Tac_ChopsO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SFG_Tac_ChopsO";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_Shemagh_mesh
{
    displayName = "TRYK Shemagh Mesh";
    buyValue = 50;
    physicalClass = "TRYK_Shemagh_mesh";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_Shemagh_G
{
    displayName = "TRYK Shemagh OD";
    buyValue = 50;
    physicalClass = "TRYK_Shemagh_G";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_Shemagh
{
    displayName = "TRYK Shemagh Tan";
    buyValue = 50;
    physicalClass = "TRYK_Shemagh";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_TRYK_Shemagh_WH
{
    displayName = "TRYK Shemagh White";
    buyValue = 50;
    physicalClass = "TRYK_Shemagh_WH";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Balaclava_blk
{
    displayName = "Balaclava (Black)";
    buyValue = 50;
    physicalClass = "G_Balaclava_blk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Bandanna_khk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_Bandanna_khk";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Bandanna_tan
{
    displayName = "Bandana (Khaki)";
    buyValue = 50;
    physicalClass = "G_Bandanna_tan";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Bandanna_oli
{
    displayName = "Bandana (Olive)";
    buyValue = 50;
    physicalClass = "G_Bandanna_oli";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Bandanna_aviator
{
    displayName = "Bandana (Shades)";
    buyValue = 50;
    physicalClass = "G_Bandanna_aviator";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_G_Bandanna_beast
{
    displayName = "Bandana (Beast)";
    buyValue = 50;
    physicalClass = "G_Bandanna_beast";
    physicalType = "Cloth";
    clothingType = "Item";
};
	//Vests
class Item_TRYK_V_Bulletproof_BL
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "TRYK_V_Bulletproof_BL";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Black
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Black";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Grey
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Grey";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Tan
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Tan";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Crimson
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Crimson";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Green
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Green";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_DON_Vest_Blue
{
    displayName = "physicalItem";
    buyValue = 800;
    physicalClass = "DON_Vest_Blue";
    physicalType = "Cloth";
    clothingType = "Vest";
};
	
	class Item_DON_Vest_Warriors
	{
		displayName = "physicalItem";
		buyValue = 800;
		physicalClass = "DON_Vest_TheWarriors";
		physicalType = "Cloth";
		clothingType = "Vest";
	};
	
	class Item_DON_Vest_YOCD
	{
		displayName = "physicalItem";
		buyValue = 800;
		physicalClass = "DON_Vest_YOCD";
		physicalType = "Cloth";
		clothingType = "Vest";
	};

   class Item_TRYK_V_Bulletproof_BLK
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "TRYK_V_Bulletproof_BLK";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof
{
    displayName = "physicalItem";
    buyValue = 300;
    physicalClass = "TRYK_V_Bulletproof";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_V_TacVest_blk
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "V_TacVest_blk";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_A3PI_potato
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_potato";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_A3PI_camovest
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_camovest";
    physicalType = "Cloth";
    clothingType = "Vest";
};


class Item_tacvest_blue
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_blue";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_cyan
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_cyan";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_green
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_green";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_orange
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_orange";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_pink
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_pink";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_purple
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_purple";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_red
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_red";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_yellow
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_yellow";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_abstract
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_abstract";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_rainbow
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_rainbow";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_rainbowswirl
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_rainbowswirl";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_bluedigi
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_bluedigi";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_christmasdigi
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_christmasdigi";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_digi
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_digi";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_greendigi
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_greendigi";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_gucci
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_gucci";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_louisv
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_louisv";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_mericacamo
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_mericacamo";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_navydigi
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_navydigi";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_orangecamo
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_orangecamo";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_redcamo
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_redcamo";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_space
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_space";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_space2
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_space2";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_stencil
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_stencil";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_stickerbomb
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_stickerbomb";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_sunsetpoly
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_sunsetpoly";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_tacvest_yellowabstract
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "tacvest_yellowabstract";
    physicalType = "Cloth";
    clothingType = "Vest";
};

//Backpacks
class Item_lscarryall_black
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_black";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_blue
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_blue";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_cyan
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_cyan";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_gray
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_gray";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_green
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_green";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_hotpink
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_hotpink";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_orange
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_orange";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_purple
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_purple";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_red
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_red";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_yellow
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_yellow";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_digi
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_digi";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_louisv
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_louisv";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_rainbow
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_rainbow";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_rainbowtile
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_rainbowtile";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_redcamo
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_redcamo";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_space
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_space";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_lscarryall_fade
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_fade";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

class Item_TRYK_B_COYOTEBACKPACK_BLK
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "TRYK_B_COYOTEBACKPACK_BLK";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_COYOTEBACKPACK
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "lscarryall_fade";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_COYOTEBACKPACK_WH
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "TRYK_B_COYOTEBACKPACK_WH";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_COYOTEBACKPACK_OD
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "TRYK_B_COYOTEBACKPACK_OD";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
/*
class Item_TRYK_B_BELT_BLK
{
    displayName = "Black TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_BLK";
    physicalType = "Cloth";
    clothingType = "Backpack";
	copRank = "Cadet";
};
*/
class Item_TRYK_B_BELT_BR
{
    displayName = "Brown TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_BR";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_BELT_AOR1
{
    displayName = "AOR1 TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_AOR1";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_BELT_AOR2
{
    displayName = "AOR2 TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_AOR2";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_BELT_CYT
{
    displayName = "CYT TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_CYT";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_BELT_JR
{
    displayName = "JR TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_JR";
    physicalType = "Cloth";
    clothingType = "Backpack";
};
class Item_TRYK_B_BELT_TAN
{
    displayName = "Tan TRYK Belt";
    buyValue = 250;
    physicalClass = "TRYK_B_BELT_TAN";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

//Cop Clothing
//Uniforms
/*
class Item_A3PI_Patrol_Cadet_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Cadet_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Cadet";
};

class Item_A3PI_Patrol_Deputy_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Deputy_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Deputy";
};

class Item_A3PI_Patrol_Corporal_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Corporal_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

class Item_A3PI_Patrol_Sergeant_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Sergeant_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Sergeant";
};

class Item_A3PI_Patrol_Lieutenant_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Lieutenant_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Command";
};

class Item_A3PI_Patrol_Captain_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Captain_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Command";
};


class Item_A3PI_Patrol_UnderSheriff_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_UnderSheriff_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "High Command";
};

class Item_A3PI_Patrol_Sheriff_Uni
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_Patrol_Sheriff_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "High Command";
};

class Item_A3PI_ECSO_Patrol_Jacket
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "A3PI_ECSO_Patrol_Jacket";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Cadet";
};

class Item_U_B_HeliPilotCoveralls
{
    displayName = "physicalItem";
    buyValue = 100;
    physicalClass = "U_B_HeliPilotCoveralls";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};


//Headgear

class Item_Campaign_Hat_Sheriff
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

class Item_Campaign_Hat_Washedout
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Corporal";
};

class Item_max_sheriff_Hat
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Command";
};

class Item_H_PilotHelmetHeli_B
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Corporal";
};

class Item_TRYK_H_DELTAHELM_NV
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Command";
};

class Item_TRYK_H_PASGT_BLK
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

class Item_TRYK_H_PASGT_OD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Campaign_Hat_Sheriff";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

//Goggles
class Item_pmc_earpiece
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "pmc_earpiece";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

class A3PI_GasMask_Black
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_GasMask_Black";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

//Vests
class Item_TRYK_V_Bulletproof_JSCOG
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_JSCOG";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Cadet";
};

class Item_TRYK_V_Bulletproof_ECSO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_ECSO";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Cadet";
};

class Item_LS_Sheriff
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "LS_Sheriff";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Command";
};

class Item_A3PI_ttfvest
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ttfvest";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Corporal";
};

class Item_JCSO_Ridealong_Vest
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "JCSO_Ridealong_Vest";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Corporal";
};

class Item_A3PI_EC_PATROL_VEST
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EC_PATROL_VEST";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Command";
};

class Item_V_RebreatherIR
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "V_RebreatherIR";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Corporal";
};

class Item_V_RebreatherB
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "V_RebreatherB";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Corporal";
};

// DoC Clothing
//Tan Clothing
class Item_A3PI_Prisoner_Outfit
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Prisoner_Outfit";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Cadet";
};

class Item_A3PI_Corrections_Cadet_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Cadet_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Cadet";
};

class Item_A3PI_Corrections_Deputy_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Deputy_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

class Item_A3PI_Corrections_Coproral_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Coproral_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

class Item_A3PI_Corrections_Sergeant_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Sergeant_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Sergeant";
};

class Item_A3PI_Corrections_Lieutenant_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Lieutenant_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Command";
};

class Item_A3PI_Corrections_Wardan_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Corrections_Wardan_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Command";
};

//Vests
class Item_A3PI_SPolice_Pen_CO_V
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_A3PI_SPolice_Pen_CO_V";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Cadet";
};

class Item_A3PI_SPolice_Pen_SCO_V
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_A3PI_SPolice_Pen_SCO_V";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Sergeant";
};

class Item_A3PI_SPolice_Pen_DW_V
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_A3PI_SPolice_Pen_DW_V";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Command";
};

class Item_A3PI_SPolice_Pen_W_V
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_A3PI_SPolice_Pen_W_V";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Command";
};

//SERT
//Uniforms
class Item_Fox_Sert
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Fox_Sert";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_SERT_Tac_OD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SERT_Tac_OD";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};



class Item_A3PI_SERT_Patrol
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SERT_Patrol";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_SERT_Tac_blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SERT_Tac_blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_B_PCU_G_SERT
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "B_PCU_G_SERT";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_EC_SERTM
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EC_SERTM";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_SERT_Uni
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SERT_Uni";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_SERT_Urban
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SERT_Urban";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_ECSO_CID_Jacket1
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ECSO_CID_Jacket1";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_ECSO_CID_Jacket2
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ECSO_CID_Jacket2";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};

class Item_A3PI_ECSO_CID_Jacket3
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ECSO_CID_Jacket3";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "SERT";
};


//Headgear

class Item_SO_SERTCAPWH
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SO_SERTCAPWH";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "SERT";
};
class Item_SO_B_SERTCAPWH
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SO_B_SERTCAPWH";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "SERT";
};

class Item_SO_SERTCAPYLW
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SO_SERTCAPYLW";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "SERT";
};


class Item_TRYK_H_CIDW
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_CIDW";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "SERT";
};

class Item_TRYK_H_CIDO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_H_CIDO";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "SERT";
};

//Vests
class Item_SWAT_FULL_VEST
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_FULL_VEST";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SWAT_FULL_VEST_DIGI
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SWAT_BALLSACK_VEST";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_FULL_VEST_GREEN
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_FULL_VEST_GREEN";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_VEST_BLACK
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_VEST_BLACK";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_VEST_DIGI
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_VEST_DIGI";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_VEST_GREEN
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_VEST_GREEN";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_Vest_base
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_Vest_base";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};

class Item_SERT_RIDEALONGBLK
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "SERT_RIDEALONGBLK";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "SERT";
};


//FBI Clothing
//Uniforms

class Item_EF_HMFBI_1
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "EF_HMFBI_1";
    physicalType = "Cloth";
    clothingType = "Uniform";
	copRank = "FBI";
};

class Item_EF_HMFBI_2
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "EF_HMFBI_2";
    physicalType = "Cloth";
    clothingType = "Uniform";
	copRank = "FBI";
};

class Item_A3PI_EC_FBI
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EC_FBI";
    physicalType = "Cloth";
    clothingType = "Uniform";
	copRank = "FBI";
};

class Item_A3PI_DTU_Urban
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_Urban";
    physicalType = "Cloth";
    clothingType = "Uniform";
	copRank = "FBI";
};

class Item_A3PI_DTU_redtiger
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_redtiger";
    physicalType = "Cloth";
    clothingType = "Uniform";
	copRank = "FBI";
};

class Item_A3PI_DTU_fallcamo
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_fallcamo";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_tiger
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_tiger";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_arctic
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_arctic";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_bloodshot
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_bloodshot";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_bluetiger
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_bluetiger";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_green
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_green";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_red
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_red";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_tan";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_Purple
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_Purple";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_blue
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_blue";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_white
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_white";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DTU_yellow
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DTU_yellow";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

//Vests
class Item_fbi_chestrig_blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_blk";
    physicalType = "Cloth";
    clothingType = "Vest";
	copRank = "FBI";
};

class Item_fbi_chestrig_tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_tan";
    physicalType = "Cloth";
    clothingType = "Vest";
	copRank = "FBI";
};

class Item_fbi_chestrig_grn
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_grn";
    physicalType = "Cloth";
    clothingType = "Vest";
	copRank = "FBI";
};

class Item_fbi_chestrig_blk_cmd
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_blk_cmd";
    physicalType = "Cloth";
    clothingType = "Vest";
	copRank = "FBI";
};

class Item_fbi_chestrig_tan_cmd
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_tan_cmd";
    physicalType = "Cloth";
    clothingType = "Vest";
	copRank = "FBI";
};

class Item_fbi_chestrig_grn_cmd
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "fbi_chestrig_grn_cmd";
    physicalType = "Cloth";
    clothingType = "Vest";
};
*/
//DOJ Clothing
//Uniforms
class Item_A3PI_DOJ_JACKET_CJ_Blu
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_CJ_Blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_JACKET_CJ_Blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_CJ_Blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_JACKET_Justice_Blu
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_Justice_Blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_JACKET_Justice_Blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_Justice_Blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_JACKET_Judge_Blu
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_Judge_Blu";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_JACKET_Judge_Blk
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_JACKET_Judge_Blk";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Gray
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Gray";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_White
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_White";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Black
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Black";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Tan
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Tan";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Green
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Green";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Red
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Red";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Jumper_Blue
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Jumper_Blue";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_CJ_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_CJ_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Justice_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Justice_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Judge_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Judge_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_StateAttorney_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_StateAttorney_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_ASA_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_ASA_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_DOJ_Pro_Casual
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_DOJ_Pro_Casual";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

//Vests
class Item_A3PI_deptjvest1
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_deptjvest1";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_Judge
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_Judge";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_Justice
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_Justice";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_CJ
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_CJ";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_Prosecutor
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_Prosecutor";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_ASA
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_DAO_ASA";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_TRYK_V_Bulletproof_SA
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "TRYK_V_Bulletproof_SA";
    physicalType = "Cloth";
    clothingType = "Vest";
};

//Dive Clothing
//Uniforms
class Item_U_B_Wetsuit
{
    displayName = "physicalItem";
    buyValue = 2000;
    physicalClass = "U_B_Wetsuit";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

class Item_U_O_Wetsuit
{
    displayName = "physicalItem";
    buyValue = 2000;
    physicalClass = "U_O_Wetsuit";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

class Item_U_I_Wetsuit
{
    displayName = "physicalItem";
    buyValue = 2000;
    physicalClass = "U_I_Wetsuit";
    physicalType = "Cloth";
    clothingType = "Uniform";
    copRank = "Corporal";
};

//Goggles
class Item_G_Diving
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "G_B_Diving";
    physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Corporal";
};

//Vests
class Item_A3PI_Rebreather
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_Rebreather";
    physicalType = "Cloth";
    clothingType = "Vest";
    copRank = "Corporal";
};

//ERD Clothing
//Uniforms
class Item_A3PI_EMS_Probie
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Probie";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_EMT
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_EMT";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_AEMT
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_AEMT";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Para
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Para";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_D
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_D";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_Chief
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_Chief";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_Ass
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_Ass";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_FDC
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_FDC";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_SRC
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_SRC";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_Cpt
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_Cpt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_FDCpt
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_FDCpt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_LT
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_LT";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_FDLt
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_FDLt";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_EMS_Off_FD
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Off_FD";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

class Item_A3PI_SARTrooper_Uniform
{
    displayName = "physicalItem";
    buyValue = 200;
    physicalClass = "A3PI_SARTrooper_Uniform";
    physicalType = "Cloth";
    clothingType = "Uniform";
};

//Headgear

class Item_A3PI_ERD_Cap
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ERD_Cap";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_medic_helmet
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_medic_helmet";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_SARTrooper_Cap
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SARTrooper_Cap";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_A3PI_SARTrooper_Helmet
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SARTrooper_Helmet";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_CrewHelmetHeli_B
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_CrewHelmetHeli_B";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_blu
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_blu";
    physicalType = "Cloth";
    clothingType = "Item";
};

class Item_H_Cap_red
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "H_Cap_red";
    physicalType = "Cloth";
    clothingType = "Item";
};

//Vests
class Item_DON_Vest_invis
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "DON_Vest_invis";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_A3PI_EMS_Vest_FTO
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_EMS_Vest_FTO";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_A3PI_EMS_Vest_RA
{
    displayName = "physicalItem";
    buyValue = 150;
    physicalClass = "A3PI_EMS_Vest_RA";
    physicalType = "Cloth";
    clothingType = "Vest";
};

class Item_A3PI_SARTrooper_Rebreather
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_SARTrooper_Rebreather";
    physicalType = "Cloth";
    clothingType = "Vest";
};

//Backpacks
class Item_A3PI_ANPRC155_Invisible
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "A3PI_ANPRC155_Invisible";
    physicalType = "Cloth";
    clothingType = "Backpack";
};

//Weapons - Accessories
class Item_BP_MRT
{
	displayName = "physicalItem";
	buyValue = 500;
	physicalClass = "BP_MRT";
	clothingType = "Item";
	copRank = "SERT";
};
class Item_RH_compM2
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_compM2";
    clothingType = "Item";
    copRank = "Sergeant";
};

class Item_RH_compm4s
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_compm4s";
    clothingType = "Item";
    copRank = "SERT";
};
class Item_FHQ_optic_HWS
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "FHQ_optic_HWS";
    clothingType = "Item";
    copRank = "Sergeant";
};
class Item_RH_docter
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_docter";
    clothingType = "Item";
    copRank = "Cadet";
};
class Item_RH_LTdocter
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_LTdocter";
    clothingType = "Item";
    copRank = "Cadet";
};
class Item_RH_LTdocterl
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_LTdocterl";
    clothingType = "Item";
    copRank = "Cadet";
};
class Item_hlc_optic_kobra
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "hlc_optic_kobra";
    clothingType = "Item";
    copRank = "FBI";
};
class Item_FHQ_optic_AC12136
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "FHQ_optic_AC12136";
    clothingType = "Item";
    copRank = "Cadet";
};
class Item_RH_SFM952V
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_SFM952V";
    clothingType = "Item";
    copRank = "SERT";
};
class Item_RH_M6X
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_M6X";
    clothingType = "Item";
    copRank = "SERT";
};
class Item_RH_X300
{
    displayName = "physicalItem";
    buyValue = 500;
    physicalClass = "RH_X300";
    clothingType = "Item";
    copRank = "SERT";
};


//PhysItems

class Item_GPS
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "ItemGPS";
	physicalType = "Cloth";
    clothingType = "Item";
};
class Item_Map
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "ItemMap";
	physicalType = "Cloth";
    clothingType = "Item";
};
class Item_Compass
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "ItemCompass";
	physicalType = "Cloth";
    clothingType = "Item";
};
class Item_Radio
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "tf_anprc152";
	physicalType = "Cloth";
    clothingType = "Item";
	copRank = "Cadet";
};
class Item_Binoc
{
    displayName = "physicalItem";
    buyValue = 50;
    physicalClass = "Binocular";
	physicalType = "Cloth";
    clothingType = "Item";
};
class Item_NVG
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "NVGoggles";
	physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

class Item_NVG_OPFOR
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "NVGoggles_OPFOR";
	physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

class Item_NVG_INDEP
{
    displayName = "physicalItem";
    buyValue = 250;
    physicalClass = "NVGoggles_INDEP";
	physicalType = "Cloth";
    clothingType = "Item";
    copRank = "Cadet";
};

};