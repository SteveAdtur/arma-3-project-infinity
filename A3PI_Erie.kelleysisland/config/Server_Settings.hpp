class Server_Settings
{
	serverName = "";
	serverVersion = "0.6.0";

	class Save_Settings
	{
		VehicleInventorySaveLoad = 0;
	};
	
	class Player_Settings
	{
		maxCarry = 50;

		class Player_Survival
		{
			class Hunger
			{
				decrease = 1.5;
				decreaseSleep = 154; // 0 Minute, 54 seconds
			};
			class Thirst
			{
				decrease = 1.5;
				decreaseSleep = 154; // 0 Minute, 54 seconds
			};
			class Bloodloss
			{
				minor = 2;
				major = 10;
				internal = 25;
				bleedrate = 3;
			};
			class Bloodregain
			{
				amount = 2;
				timer = 3;
			};
		};
		
		class Player_Income
		{
			incomeTimer = 300;
			baseIncome = 25;
		};

		class Player_Refining
		{
			refineTimer = 100; 
		};
		
		class Player_Medical
		{
			reviveTimer = 30;
			respawnTimer = 600;
		};
		
		class Player_Weapons
		{
			meleeWeapons[] = {"A3PI_Pickaxe2017","A3PI_Hatchet2017","A3PI_Spade2017","MeleeHatchet","MeleeScythe"};
			nonLethalRounds[] = {"26_taser","BP_762x51_Ball_Rubber","BP_12Gauge_NL"};
			tranqRounds[] = {"BP_762x51_Ball_Rubber"};
		};

		class Player_Mining
		{
			mineableRocks[] = {"Land_W_sharpStone_03","Land_Limestone_01_03_F"};
			mineableLoot[] = {"A3PI_Bits_Rock_S_Coal","A3PI_Bits_Rock_S_Iron"};
			sandSurfaceTypes[] = {"#kelleysisland_beach_Surface"}; // need to get surface type names in dev private testing via surfaceType (getPos player)
		};
		
		class Player_WoodCutting
		{
			woodCutting[] = {"Paper_Mulberry"};
			woodCuttingLoot[] = {"A3PI_WoodLog"};
		};
		
		class Player_Farming
		{
			farmingPlants[] = {"A3PI_Cannabis","Oleander2","A3PI_Cotton","A3PI_Beans","A3PI_Pumpkin","A3PI_Corn","A3PI_SunFlower","A3PI_Wheat"};
			farmingSurfaces[] = {"#kelleysisland_soil_Surface"}; // need to get surface type names in dev private testing via surfaceType (getPos player)
			farmingGround[] = {1.22,1.5,1.18,1.27,1,1.47,1.25,1.2};
			farmingTooClose = 3;
			farmingTimer[] = {800,600,350,350,400,350,300,350};
		};
		
		class Player_Antagonistic
		{
			robberyTimer = 120;
			robberyBase = 4000;
		};
		
	};
	class Player_Garages
	{
		garages[] = {"A3PI_Garage_1","A3PI_Garage_2","A3PI_Garage_3","A3PI_Garage_4","A3PI_Garage_5","A3PI_Garage_6","A3PI_Garage_7","A3PI_Garage_8","A3PI_Garage_9","A3PI_Garage_10","A3PI_Garage_11","A3PI_Garage_12"};
	};
	class Player_atm
	{ 
		atms[] = {"A3PI_ATM_1","A3PI_ATM_2","A3PI_ATM_3","A3PI_ATM_4","A3PI_ATM_5","A3PI_ATM_6","A3PI_ATM_7","A3PI_ATM_8","A3PI_ATM_9","A3PI_ATM_10"};
	};
	class Player_Spawns
	{
		spawnPoints[] = {"Union City","Falls Church","Van Der Ville","Watkins Glen"};
	};
};


#include "Server_Items.hpp"
#include "Server_Vehicles.hpp"
#include "Server_Interactions.h"
#include "Server_Crafting.hpp"
#include "Server_Medical.hpp"
#include "Server_Jobs.hpp"
#include "Server_Sounds.hpp"
#include "Server_Factory.hpp"