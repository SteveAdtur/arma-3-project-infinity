class Server_Crafting
{
	
	Server_Crafting_Guns[] = {"Craft_M16A4","Craft_M16A3","Craft_RISColt","Craft_ColtRIS","Craft_M4Colt","Craft_HK10Colt","Craft_HK14Colt","Craft_ColtHK10","Craft_ColtHK14","Craft_M4WdlColt","Craft_M4TanColt","Craft_ColtM4","Craft_ColtPaintWdl","Craft_ColtPaintTan","Craft_BasicM16A3","Craft_4774","Craft_7447","Craft_74U74","Craft_7474U","Craft_ColtBasic","Craft_AK74Basic"};
	
	
	class Craft_M16A4
	{
		displayName = "STR_Items_M16A4Craft";
		partsNeeded[] = {{"Item_A4Rail",1,"STR_Items_A4RailSystem"},{"Item_A4Reciever",1,"STR_Items_A4Reciever"}};
		rifleNeeded = "RH_M16A3";
		rifleNeededName = "M16A3";
		partsReturned[] = {{"Item_A3Rail",1,"STR_Items_A3Rail"},{"Item_A3Reciever",1,"STR_Items_A3Reciever"}};
		outputGun = "RH_M16A4";
	};
	class Craft_M16A3
	{
		displayName = "STR_Items_M16A3Craft";
		partsNeeded[] = {{"Item_A3Rail",1,"STR_Items_A3Rail"},{"Item_A3Reciever",1,"STR_Items_A3Reciever"}};
		rifleNeeded = "RH_M16A4";
		rifleNeededName = "M16A4";
		partsReturned[] = {{"Item_A4Rail",1,"STR_Items_A4RailSystem"},{"Item_A4Reciever",1,"STR_Items_A4Reciever"}};
		outputGun = "RH_M16A3";
	};
	class Craft_RISColt
	{
		displayName = "STR_Items_RISColtCraft";
		partsNeeded[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"}};
		outputGun = "RH_M4_ris";
	};
	class Craft_ColtRIS
	{
		displayName = "STR_Items_ColtRISCraft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"}};
		rifleNeeded = "RH_M4_ris";
		rifleNeededName = "M4 RIS";
		partsReturned[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_M4Colt
	{
		displayName = "STR_Items_M4ColtCraft";
		partsNeeded[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_Foregrip",1,"STR_Items_Foregrip"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"}};
		outputGun = "hlc_rifle_M4";
	};
	class Craft_HK10Colt
	{
		displayName = "STR_Items_HK10Craft";
		partsNeeded[] = {{"Item_HKReciever",1,"STR_Items_HKReciever"},{"Item_Foregrip",1,"STR_Items_Foregrip"},{"Item_HK10Rail",1,"STR_Items_HK10"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		outputGun = "RH_Hk416s";
	};
	class Craft_HK14Colt
	{
		displayName = "STR_Items_HK14Craft";
		partsNeeded[] = {{"Item_HKReciever",1,"STR_Items_HKReciever"},{"Item_Foregrip",1,"STR_Items_Foregrip"},{"Item_HK14Rail",1,"STR_Items_HK14"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		outputGun = "RH_Hk416";
	};
	class Craft_ColtHK10
	{
		displayName = "STR_Items_ColtHK10Craft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		rifleNeeded = "RH_Hk416s";
		rifleNeededName = "HK416 D10";
		partsReturned[] = {{"Item_HKReciever",1,"STR_Items_HKReciever"},{"Item_Foregrip",1,"STR_Items_Foregrip"},{"Item_HK10Rail",1,"STR_Items_HK10"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_ColtHK14
	{
		displayName = "STR_Items_ColtHK14Craft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		rifleNeeded = "RH_Hk416";
		rifleNeededName = "HK416 D14";
		partsReturned[] = {{"Item_HKReciever",1,"STR_Items_HKReciever"},{"Item_Foregrip",1,"STR_Items_Foregrip"},{"Item_HK14Rail",1,"STR_Items_HK14"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_M4WdlColt
	{
		displayName = "STR_Items_M4WoodlandCraft";
		partsNeeded[] = {{"Item_M4Reciever",1,"STR_Items_M4Reciever"},{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_GreenPaint",1,"STR_Items_GreenPaint"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		outputGun = "RH_m4_wdl";
	};
	class Craft_M4TanColt
	{
		displayName = "STR_Items_M4TanCraft";
		partsNeeded[] = {{"Item_M4Reciever",1,"STR_Items_M4Reciever"},{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_TanPaint",1,"STR_Items_TanPaint"}};
		rifleNeeded = "hlc_rifle_Colt727";
		rifleNeededName = "Colt Carbine";
		partsReturned[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		outputGun = "RH_m4_des";
	};
	class Craft_ColtM4
	{
		displayName = "STR_Items_ColtM4Craft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"}};
		rifleNeeded = "hlc_rifle_M4";
		rifleNeededName = "Colt M4 Carbine";
		partsReturned[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_Foregrip",1,"STR_Items_Foregrip"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_ColtPaintWdl
	{
		displayName = "STR_Items_ColtPaintCraft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		rifleNeeded = "RH_m4_wdl";
		rifleNeededName = "M4 Woodland";
		partsReturned[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_M4Reciever",1,"STR_Items_M4Reciever"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_ColtPaintTan
	{
		displayName = "STR_Items_ColtPaintCraft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		rifleNeeded = "RH_m4_des";
		rifleNeededName = "M4 Tan";
		partsReturned[] = {{"Item_M4Rail",1,"STR_Items_M4Rail"},{"Item_M4Reciever",1,"STR_Items_M4Reciever"}};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_BasicM16A3
	{
		displayName = "STR_Items_BasicM16A3Craft";
		partsNeeded[] = {{"Item_A3Rail",1,"STR_Items_A3Rail"},{"Item_A3Reciever",1,"STR_Items_A3Reciever"}};
		rifleNeeded = "";
		partsReturned[] = {};
		outputGun = "RH_M16A3";
	};
	class Craft_4774
	{
		displayName = "STR_Items_4774Craft";
		partsNeeded[] = {{"Item_AK47Reciever",1,"STR_Items_AK47Reciever"},{"Item_AK47Barrel",1,"STR_Items_AK47Barrel"}};
		rifleNeeded = "hlc_rifle_ak74";
		rifleNeededName = "AK74";
		partsReturned[] = {{"Item_AK74Reciever",1,"STR_Items_AK74Reciever"},{"Item_AK74Barrel",1,"STR_Items_AK74Barrel"}};
		outputGun = "hlc_rifle_ak47";
	};
	class Craft_4747U
	{
		displayName = "STR_Items_4747UCraft";
		partsNeeded[] = {{"Item_AK74Folding",1,"STR_Items_AK74Folding"}};
		rifleNeeded = "hlc_rifle_ak47";
		rifleNeededName = "AK47";
		partsReturned[] = {{"Item_AK74Stock",1,"STR_Items_AK74Stock"}};
		outputGun = "BP_AKM";
	};
	class Craft_47U47
	{
		displayName = "STR_Items_4747UCraft";
		partsNeeded[] = {{"Item_AK74Stock",1,"STR_Items_AK74Stock"}};
		rifleNeeded = "BP_AKM";
		rifleNeededName = "AK47";
		partsReturned[] = {{"Item_AK74Folding",1,"STR_Items_AK74Folding"}};
		outputGun = "hlc_rifle_ak47";
	};
	class Craft_7447
	{
		displayName = "STR_Items_7447Craft";
		partsNeeded[] = {{"Item_AK74Reciever",1,"STR_Items_AK74Reciever"},{"Item_AK74Barrel",1,"STR_Items_AK74Barrel"}};
		rifleNeeded = "hlc_rifle_ak47";
		rifleNeededName = "AK47";
		partsReturned[] = {{"Item_AK47Reciever",1,"STR_Items_AK47Reciever"},{"Item_AK47Barrel",1,"STR_Items_AK47Barrel"}};
		outputGun = "hlc_rifle_ak74";
	};
	class Craft_74U74
	{
		displayName = "STR_Items_74U74Craft";
		partsNeeded[] = {{"Item_AK74Folding",1,"STR_Items_AK74Folding"}};
		rifleNeeded = "hlc_rifle_ak74";
		rifleNeededName = "AK74";
		partsReturned[] = {{"Item_AK74Stock",1,"STR_Items_AK74Stock"}};
		outputGun = "hlc_rifle_aks74u";
	};
	class Craft_7474U
	{
		displayName = "STR_Items_7474UCraft";
		partsNeeded[] = {{"Item_AK74Stock",1,"STR_Items_AK74Stock"}};
		rifleNeeded = "hlc_rifle_aks74u";
		rifleNeededName = "AKS74U";
		partsReturned[] = {{"Item_AK74Folding",1,"STR_Items_AK74Folding"}};
		outputGun = "hlc_rifle_ak74";
	};
	class Craft_ColtBasic
	{
		displayName = "STR_Items_ColtBasicCraft";
		partsNeeded[] = {{"Item_ColtBarrel",1,"STR_Items_ColtBarrel"},{"Item_ColtReciever",1,"STR_Items_ColtReciever"}};
		rifleNeeded = "";
		partsReturned[] = {};
		outputGun = "hlc_rifle_Colt727";
	};
	class Craft_AK74Basic
	{
		displayName = "STR_Items_AK74BasicCraft";
		partsNeeded[] = {{"Item_AK74Reciever",1,"STR_Items_AK74Reciever"},{"Item_AK74Barrel",1,"STR_Items_AK74Barrel"},{"Item_AK74Stock",1,"STR_Items_AK74Stock"}};
		rifleNeeded = "";
		partsReturned[] = {};
		outputGun = "hlc_rifle_ak74";
	};
		
};





