/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_openInventory.sqf
Author: Heisen http://heisen.pw
Description: Handle Inventory Menu
Parameter(s): N/A
************************************************************/


private ["_countNum"];
_countNum = -1;

if (A3PI_antiSpam) exitWith {
	["You're currently performing an action!",10,"red"] call A3PI_fnc_msg;
};

createDialog "RscDisplay_Inventory";

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	_varName = configName _x;
	_varAmount = call compile _varName;
	
	_varOut = [_varAmount] call BIS_fnc_numberText;
	


	if (_varAmount >= 1) then {
		_countNum = _countNum + 1;
		_varDisplayName = getText (_x >> "displayName");
		_varWeightValue = getNumber (_x >> "weightValue");
		lbAdd [1500,format["%2 - (%1x %3.kg)",_varOut,localize(_varDisplayName),(_varWeightValue * _varAmount)]];
		lbSetPicture [1500, _countNum,(getText (_x >> "displayIcon"))];
		lbSetPictureColor [1500,_countNum, [0, 0, 0, 0]];
		lbSetData [1500,_countNum,_varName];

	};
} forEach _items;

lbClear 1502;
lbAdd [1502,(format["Total Weight: %1/%2kg",A3PI_Weight,A3PI_MaxCarry])];