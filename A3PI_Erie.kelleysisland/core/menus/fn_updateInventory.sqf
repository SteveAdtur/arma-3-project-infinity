/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_updateInventory.sqf
Author: Heisen http://heisen.pw
Description: Handle Update Inventory Menu
Parameter(s): N/A
************************************************************/


lbClear 1500;

private ["_countNum","_itemsArray"];
_countNum = -1;
_itemsArray = [];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	_varName = configName _x;
	_varAmount = call compile _varName;
	_varOut = [_varAmount] call BIS_fnc_numberText;
	//systemChat format["%1 = %2",_varName,_varAmount];

	if (_varAmount >= 1) then {
		_countNum = _countNum + 1;
		_varDisplayName = getText (_x >> "displayName");
		_varWeightValue = getNumber (_x >> "weightValue");
		lbAdd [1500,format["%2 - (%1x %3.kg)",_varOut,localize(_varDisplayName),(_varWeightValue * _varAmount)]];
		lbSetPicture [1500, _countNum,(getText (_x >> "displayIcon"))];
		lbSetPictureColor [1500,_countNum, [0, 0, 0, 0]];
		lbSetData [1500,_countNum,_varName];
		systemChat str(_countNum);
		_itemsArray pushBack [_varName,_varAmount];
	};
	
} forEach _items;

player setVariable["A3PI_itemsCheck",_itemsArray,true];

lbClear 1502;
lbAdd [1502,(format["Total Weight: %1/%2kg",A3PI_Weight,A3PI_MaxCarry])];