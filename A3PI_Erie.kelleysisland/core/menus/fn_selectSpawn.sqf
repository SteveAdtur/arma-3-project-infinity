//Steve

if ((lbCurSel 1500) isEqualTo -1) exitWith {hint "Nothing Selected!";};

_spawnLocation = lbData [1500,(lbCurSel 1500)];

_pos = getMarkerPos _spawnLocation;
closeDialog 0;
closeDialog 0;

player setPos _pos;
player enableSimulation true;
player allowDamage true;
A3PI_Spawning = false;
