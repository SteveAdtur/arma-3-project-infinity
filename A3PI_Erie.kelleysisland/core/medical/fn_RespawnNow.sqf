//Steve

disableSerialization;

A3PI_RespawnReady = false;
closeDialog 0;
player setDamage 1;
sleep 1;
A3PI_Spawning = true;

player enableSimulation false;
A3PI_SyncRecent = false;

[] spawn A3PI_fnc_updateRequest;
[player,"","",2] remoteExec ["A3PIsys_fnc_insertJail",2];
_onDutyMedics = [];
{ 
	if(_x getVariable "A3PI_MedicOnDuty") then 
	{
	  _onDutyMedics pushBack _x;

	}; 
} forEach(allPlayers); 
{
	[getPlayerUID player] remoteExec ["A3PI_fnc_clearMarker",_x];
}forEach _onDutyMedics;
_mName = player getVariable "Marker";
deleteMarker _mName;
call A3PI_fnc_spawnMenu;