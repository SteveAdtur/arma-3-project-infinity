//Steve

disableSerialization;

params[
	"_timer",
	"_type",
	"_commID",
	"_selWound"
	];
	
	closeDialog 0;
	
	162 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
		
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = _type;
		_pos = getPos player;
		for "_i" from 0 to _timer step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _timer));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			uiSleep 1;
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};
			sleep 1;
			if (player distance _pos > 10) exitWith {
				162 cutText ["","PLAIN"];
				_exit = true;
			};
		};
		if(_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		[_type,_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_commID];
		
		162 cutText ["","PLAIN"]; 
	
		