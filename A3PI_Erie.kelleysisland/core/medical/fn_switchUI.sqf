//Steve

params[
	"_chosenLoc"
];


_currentPatient = player getVariable "currentPatient"; 

_wounds = []; 
if(_currentPatient isEqualTo "Self")then{
	_wounds = player getVariable "A3PI_CurrentWounds";
}else{
	_wounds = cursorObject getVariable "A3PI_CurrentWounds";
};

lbClear 1500;
lbClear 1501;
_push = [];
_countNum = 0;
_count = 0;
if(_wounds isEqualTo [])exitWith{};
{

	_xLoc = _x select 0;
	if(_xLoc isEqualTo _chosenLoc)then{
		_push pushback _x;
		_isBleeding = "";
		if(_x select 2 == 1)then{
			_isBleeding = "Bleeding";
		}else{
			_isBleeding = "Not Bleeding";
		};
		lbAdd [1500, (format["%1 - %2", _x select 1, _isBleeding])];
		lbSetData [1500, _countNum, str(_count)];
		_countNum = _countNum +1;
	};
	_count = _count +1;
}forEach _wounds;

