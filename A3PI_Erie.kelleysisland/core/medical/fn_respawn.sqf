//Steve
disableSerialization;


_respawnTimer = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Medical" >> "respawnTimer");
_isDone = false;
_exit = false;
_onDutyMedics = [];
{ 
	if(_x getVariable "A3PI_MedicOnDuty") then 
	{
	  _onDutyMedics pushBack _x;

	}; 
} forEach(allPlayers); 
{
[getPlayerUID player,player] remoteExec ["A3PI_fnc_downedMarker",_x];
}forEach _onDutyMedics;
72 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
_text = "Waiting to Respawn";
player setVariable ["revived",false,true];
_mCount = -1;
{
if(lifeState _x == "INCAPACITATED")then
{
 _mCount = _mCount +1;
};
}forEach (allPlayers);



for "_i" from _respawnTimer to 0 step -1 do {
	_current = progressPosition ((_display)displayCtrl 1001);
	((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _respawnTimer));
	((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2 seconds remaining!",_text,_i];
	sleep 1;
	if(player getVariable "revived")then{
		hint format ["%1 its fucked",player getVariable "revived"];
		72 cutText ["","PLAIN"];
		_exit = true;
	};

};
if (_exit) exitWith {
	["You were revived",10,"green"] call A3PI_fnc_msg;
	player setVariable ["revived",false,true];
	{
	[getPlayerUID player] remoteExec ["A3PI_fnc_clearMarker",_x];
	}forEach _onDutyMedics;
};
_isDone = true;
waitUntil {_isDone}; // Wait till progress is done
72 cutText ["","PLAIN"]; 

for "_i" from -1 to 11 step +1 do {
	_hitOut = format["hitIndex%1",_i];
	player setVariable[_hitOut,0,true];
};

_items = "true" configClasses (missionConfigFile >> "Server_Items");
{
	_varName = configName _x;
	_varAmount = call compile _varName;


	if (_varAmount >= 1) then {
		[_varName, _varAmount, false] call A3PI_fnc_handleItem;
	};
} forEach _items;

A3PI_RespawnReady = true;
createDialog "RscDisplay_Respawn";