// Steve

disableSerialization;

params[
	"_curOb"
];

_items = items player;
_isDone = false;
_exit = false;
_reviveTimer = getNumber(missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Medical" >> "reviveTimer");

_medCount = call compile "Item_Defib";
	
if(_medCount == 0)exitWith{
["No Defib",10,"red"]call A3PI_fnc_msg;
};



		32 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Reviving";
		_pos = getPos player;
		
		for "_i" from 0 to _reviveTimer step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _reviveTimer));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			uiSleep 1;
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};
			sleep 1;
			if (player distance _pos > 10) exitWith {
				32 cutText ["","PLAIN"];
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		

		[false] remoteExec ["A3PI_fnc_setUnconcious",(_curOb getVariable "communicationID")];
		[_curOb,""] remoteExec ["switchMove",0,true];
		
		32 cutText ["","PLAIN"]; 
