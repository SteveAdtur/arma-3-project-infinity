//Steve

_curSel = lbCurSel 1501;
_curSelF = lbCurSel 1500;
if (_curSel isEqualTo -1) exitWith {hint "No Limb Selected.";};
if (_curSelF isEqualTo -1) exitWith {hint "No Treatment Selected.";};


_curData = lbText[1501,_curSel];


_data = lbText [1500,_curSelF];

_currentPatient = player getVariable "currentPatient"; 

_patientWounds = [];



if(_currentPatient isEqualTo "Self")then{
	_patientWounds = player getVariable "A3PI_CurrentWounds";
	_dataNum = parseNumber _data;

	_selWound = _patientWounds select _dataNum;
	if(_curData isEqualTo "Bandage")exitWith{
		[10,_curData]spawn A3PI_fnc_treatBar;
	
		_patientWounds set[_dataNum,[_selWound select 0, _selWound select 1, 0,_selWound select 3,_selWound select 4]];
		["Item_Bandage",1,false]call A3PI_fnc_handleItem;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	};
	if(_curData isEqualTo "Sutures")exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
	if(_curData isEqualTo "Surgery")exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	
	};
	if(_curData isEqualTo "Splint")exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
	if(_curData isEqualTo "Cast")exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
	if(_curData isEqualTo "Plaster")exitWith{
		[10,_curData]spawn A3PI_fnc_treatBar;
	
		_patientWounds deleteAt _dataNum;
		["Item_Plaster",1,false]call A3PI_fnc_handleItem;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	};
}else{
	_curOb = cursorObject;
	_patientWounds = _curOb getVariable "A3PI_CurrentWounds";
	_commID = _curOb getVariable "CommunicationID";
	_dataNum = parseNumber _data;

	_selWound = _patientWounds select _dataNum;
	if(_curData isEqualTo "Bandage")exitWith{
		[10,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
	
		
		["Item_Bandage",1,false]call A3PI_fnc_handleItem;
		["Item_Cash",20,true]call A3PI_fnc_handleItem;
	
	};
	if(_curData isEqualTo "Plaster")exitWith{
	
		[10,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		
		["Item_Plaster",1,false]call A3PI_fnc_handleItem;
		["Item_Cash",10,true]call A3PI_fnc_handleItem;
	};
	//if(player distance getMarkerPos 'a3pi_hospital' < 23)exitWith{["You need to be at the hospital",10,"red"]call A3PI_fnc_msg;};
	if(_curData isEqualTo "Sutures")exitWith{
		
		[15,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		
		["Item_SutureKit",1,false]call A3PI_fnc_handleItem;
		["Item_Cash",40,true]call A3PI_fnc_handleItem;
	
	};
	if(_curData isEqualTo "Surgery")exitWith{
		
		[100,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		
		["Item_Cash",100,true]call A3PI_fnc_handleItem;

	
	};
	if(_curData isEqualTo "Splint")exitWith{
	
	
		[30,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		
		["Item_Splint",1,false]call A3PI_fnc_handleItem;
		["Item_Cash",40,true]call A3PI_fnc_handleItem;
	};
	if(_curData isEqualTo "Cast")exitWith{
	
		[60,_curData,_commID,_selWound]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		
		["Item_Cast",1,false]call A3PI_fnc_handleItem;
		["Item_Cash",60,true]call A3PI_fnc_handleItem;
	};
	
};



/*
hint format["CurrentPatient = %1",_currentPatient];

if(_currentPatient isEqualTo "Self")then{
	hint format["CurSel = %1  Player",_curData];
	if(_curData == 0)exitWith{
		[10]spawn A3PI_fnc_treatBar;
	
		_selWound select 2 = false;
		["Item_Bandage",1,false]call A3PI_fnc_handleItem;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	};
	if(_curData == 1)exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
	if(_curData == 2)exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	
	};
	if(_curData == 3)exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
	if(_curData == 4)exitWith{
	["You need to be treated by EMS",10,"red"]call A3PI_fnc_msg;
	
	};
}else{

	hint format["CurSel = %1 Not Player",_curData];
	if(_curData == 0)exitWith{
		[10]spawn A3PI_fnc_treatBar;
	
		["Bandage",_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_currentPatient getVariable "CommunicationID"];
		["Item_Bandage",1,false]call A3PI_fnc_handleItem;
	
	};
	if(_curData == 1)exitWith{
		
		[15]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		["Suture",_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_currentPatient getVariable "CommunicationID"];
		["Item_SutureKit",1,false]call A3PI_fnc_handleItem;
	
	};
	if(_curData == 2)exitWith{
		
		[100]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		["Surgery",_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_currentPatient getVariable "CommunicationID"];

	
	};
	if(_curData == 3)exitWith{
	
	
		[30]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		["Splint",_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_currentPatient getVariable "CommunicationID"];
		["Item_Splint",1,false]call A3PI_fnc_handleItem;
	};
	if(_curData == 4)exitWith{
	
		[60]spawn A3PI_fnc_treatBar;
		if(player getVariable "_haveGloves")then{
		
		}else{
			["infection"] call A3PI_fnc_illness;
		};
		["Cast",_selWound,0]remoteExec[A3PI_fnc_recieveTreatment,_currentPatient getVariable "CommunicationID"];
		["Item_Cast",1,false]call A3PI_fnc_handleItem;
	};
};*/