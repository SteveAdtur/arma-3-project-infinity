//Steve

params[
	"_playerID",
	"_player"
];


_markerName = format ["Marker%1",_playerID]; 
_markerStr = createMarkerLocal [_markerName,position _player]; 
_markerName setMarkerColorLocal "ColorRed"; 
_markerName setMarkerTextLocal "Injured Person"; 
_markerName setMarkerShapeLocal "ICON";
_markerName setMarkerTypeLocal "mil_objective";
_mcountOut = _mcountOut +1;