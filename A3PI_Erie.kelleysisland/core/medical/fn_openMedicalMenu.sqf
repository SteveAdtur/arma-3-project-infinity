// Heisen and Steve


private ["_push"];

params [
	"_patient"
];

createdialog "RscDisplay_MedicalMenu"; 

if (_patient == player)then{
	player setVariable ["currentPatient","Self",true];
}else{
	player setVariable ["currentPatient","NotSelf",true];
};

_bloodLevel = _patient getVariable "_bloodLevel";

_bloodType = _patient getVariable "_bloodType";

((findDisplay 19824)displayCtrl 1100) ctrlSetStructuredText parseText format ["<t align='center'>%1: Blood level:%2 %3</t>",name _patient, _bloodLevel, _bloodType];

