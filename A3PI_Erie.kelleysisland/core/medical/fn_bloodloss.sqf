//Steve
disableSerialization;
_survivalConfig = (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Survival");

_bloodLossMinor = getNumber (_survivalConfig >> "Bloodloss" >> "minor");
_bloodLossMajor = getNumber (_survivalConfig >> "Bloodloss" >> "major");
_bloodLossInternal = getNumber (_survivalConfig >> "Bloodloss" >> "internal");
_bleedrate = getNumber (_survivalConfig >> "Bloodloss" >> "bleedrate");

_bloodGain = getNumber (_survivalConfig >> "Bloodregain" >> "amount");
_bloodrate = getNumber (_survivalConfig >> "Bloodregain" >> "timer");

for "_i" from 0 to 1 step 0 do {
_curBlood = A3PI_Blood;
if(A3PI_Wounds isEqualTo [])then {

if(_curBlood < 5000)then{
_curBlood = _curBlood + _bloodGain;

if(_curBlood > 5000) then {
	_curBlood = 5000;
	};
};
A3PI_Blood = _curBlood;
player setVariable ["_BloodLevel",A3PI_Blood,true];
sleep _bloodrate;
}else{
{
_woundSeverity = _x select 1;
_isBleeding = _x select 2;

if(_isBleeding == 1)then{
	if(_curBlood > 250)then{
		if(_woundSeverity isEqualTo "Minor")then{
		
		if(_curBlood < _bloodLossMinor)then	{
			_curBlood = 200;
		}else{
			_curBlood = _curBlood - _bloodLossMinor;
		};
		
		}else{
			if(_woundSeverity isEqualTo "Major") then {
				if(_curBlood < _bloodLossMajor)then	{
					_curBlood = 200;
				}else{
					_curBlood = _curBlood - _bloodLossMajor;
				};
			}else{
				if(_curBlood < _bloodLossInternal)then	{
					_curBlood = 200;
				}else{
					_curBlood = _curBlood - _bloodLossInternal;
				};
			};
		};
		
	}else{
		
		if(player getVariable "_revived")then{
		
		
		}else{
			if(player getVariable "Uncon")then{
			
			}else{
			player allowDamage false;
				if(vehicle player != player)then{player action ["Eject", vehicle player]};
				player setVariable ['A3PI_Cuffed',0,true]; 
				[1] call A3PI_fnc_restrainAdditions;
				player setUnconscious true;
				player setVariable["tf_voiceVolume",0,true];
				player setVariable["tf_unable_to_use_radio",true];
				player setVariable["Uncon", true,true];
			};
		};
	
	};

};

}forEach A3PI_Wounds;
A3PI_Blood = _curBlood;
player setVariable ["_BloodLevel",A3PI_Blood,true];
sleep _bleedrate;
};
};