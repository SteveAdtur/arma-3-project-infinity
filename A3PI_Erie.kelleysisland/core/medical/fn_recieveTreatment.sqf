//Steve


params[
	"_treatment",
	"_loc",
	"_bagType"
	];
	_patientWounds = player getVariable "A3PI_CurrentWounds";
	if(_treatment isEqualTo "Blood")exitWith{
		["You have been given a blood bag",10,"green"]call A3PI_fnc_msg;
		A3PI_Blood = 5000;
		player setVariable ["_bloodLevel",A3PI_Blood,true];
		
		_pType = player getVariable "_bloodTypeNumber";
		
		if !(_pType in _bagType)then{
		["wrongBlood"] call A3PI_fnc_illness;		
		};
		player setVariable["_revived",false,true];
	};
	if(_treatment isEqualTo "Bandage")exitWith{
	
		_selWound = _patientWounds select _loc;
		_patientWounds set[_loc,[_selWound select 0, _selWound select 1, 0,_selWound select 3,_selWound select 4]];
		["You have been bandaged",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	};
	if(_treatment isEqualTo "Surgery")exitWith{
	
	
		_patientWounds deleteAt _loc;
		["You have been treated",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	};
	if(_treatment isEqualTo "Splint")exitWith{
	
		_patientWounds deleteAt _loc;
			["You have been treated",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	};
	if(_treatment isEqualTo "Cast")exitWith{
	
		_patientWounds deleteAt _loc;
			["You have been treated",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	
	};
	if(_treatment isEqualTo "Sutures")exitWith{
	
		_patientWounds deleteAt _loc;
			["You have been treated",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	};
	if(_treatment isEqualTo "Plaster")exitWith{
	
		_patientWounds deleteAt _loc;
		["You have been treated",10,"green"]call A3PI_fnc_msg;
		player setVariable ["A3PI_CurrentWounds",_patientWounds,true];
	
	
	};