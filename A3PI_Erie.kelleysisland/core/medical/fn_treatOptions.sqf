//Steve
lbClear 1501;
_curSel = lbCurSel 1500;
_data = lbData [1500,_curSel];

_currentPatient = player getVariable "currentPatient"; 

_patientWounds = [];


if(_currentPatient isEqualTo "Self")then{
	_patientWounds = player getVariable "A3PI_CurrentWounds";
}else{
	_patientWounds = cursorObject getVariable "A3PI_CurrentWounds";
};
_count = 0;

_dataNum = parseNumber _data;

_selWound = _patientWounds select _dataNum;

if(_selWound select 2 == 1)then{
	if(_selWound select 1 isEqualTo "Scrape")then{
		if(call compile "Item_Plaster" > 0)then{
			lbAdd [1501, "Plaster"];
			lbSetData [1501, _count, 5];
			_count = _count +1;
		};
	}else{
		if(call compile "Item_Bandage" > 0)then{
			lbAdd [1501, "Bandage"];
			lbSetData [1501, _count, 0];
			_count = _count +1;
		};
	};
};
if(_selWound select 1 isEqualTo "Minor")then{
	if(_selWound select 3 isEqualTo "Piercing")then{
		if(call compile "Item_SutureKit" > 0)then{
			lbAdd [1501, "Sutures"];
			lbSetData [1501, _count, 1];
			_count = _count +1;
		};
	};
	if(_selWound select 3 isEqualTo "Impact")then{
		if(call compile "Item_Splint" > 0)then{
			lbAdd [1501, "Splint"];
			lbSetData [1501, _count, 3];
			_count = _count +1;
		};
	};
}else{
	if(_selWound select 1 isEqualTo "Major")then{
		if(_selWound select 3 isEqualTo "Piercing")then{
			if(call compile "Item_SurgeryKit" > 0)then{
				lbAdd [1501, "Surgery"];
				lbSetData [1501, _count, 2];
				_count = _count +1;
			};
		};
		if(_selWound select 3 isEqualTo "Impact")then{
			if(call compile "Item_Cast" > 0)then{
				lbAdd [1501, "Cast"];
				lbSetData [1501, _count, 4];
				_count = _count +1;
			};
		};
	}else{
		if(call compile "Item_SurgeryKit" > 0)then{
			lbAdd [1501, "Surgery"];
			lbSetData [1501, _count, 2];
			_count = _count +1;
			_count = _count +1;
		};
	};
};