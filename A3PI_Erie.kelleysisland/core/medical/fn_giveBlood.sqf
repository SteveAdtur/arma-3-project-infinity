//Steve
disableSerialization;
params[
	"_patient"
	];
	closeDialog 0;
	if((lbCurSel 1500) isEqualTo -1) exitWith { call A3PI_fnc_openMedicalMenu;};
	_curSel = 0;
	_curSel = lbCurSel 1500;
	
	_bloodbag = lbData[1500,_curSel];
	_reviveTimer = 30;
	132 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Giving Blood";
		_pos = getPos player;
		
		for "_i" from 0 to _reviveTimer step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _reviveTimer));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			uiSleep 1;
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};
			sleep 1;
			if (player distance _pos > 10) exitWith {
				132 cutText ["","PLAIN"];
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		
		_outputStr = format["Blood given to %1",name _patient];
		
		[_outputStr,10,"green"] call A3PI_fnc_msg;
		//treatment, loc, bloodtype
		_bType = getText (missionConfigFile >> "Server_Items" >> _bloodbag >> "validBloood");
		["Blood", 0, _bType] remoteExec [A3PI_recieveTreatment, _patient getVariable "CommunicationID"];
		
		132 cutText ["","PLAIN"]; 