//Created by Steve Adtur
disableSerialization;
params[
	"_woundLoc",
	"_woundType",
	"_proj"
];
// Wound Location ex rightArm, Wound Severity true = piercing, false = impact and False = untreated.
//2 = Head ; 5 = Body ; Left Arm = 8, Right Arm = 9, Left Leg = 12, Right Leg = 13;
_A3PI_Blood = A3PI_Blood;
_list = [2,5,8,9,12,13];
if !(_woundLoc IN _list)exitWith{};
_A3PI_Wounds = player getVariable "A3PI_CurrentWounds";
if(_woundType == 1)then{
	_woundDmg = getNumber(configFile >> "CfgAmmo" >> _proj >> "hit");

	if(_woundDmg <3)then{
		_woundSev = "Scrape";
		_newWound = [_woundLoc, _woundSev, 1, "Piercing", false];
		_A3PI_Wounds pushback _newWound;
	
	}else{
		if(_woundDmg < 8)then {
			_woundSev = "Minor";
			_newWound = [_woundLoc, _woundSev, 1, "Piercing", false];
			_A3PI_Wounds pushback _newWound;
			if(_woundLoc == 2)then{
				
				_A3PI_Blood = _A3PI_Blood - 1000;
				
			}else{
				
				_A3PI_Blood = _A3PI_Blood - 500;
			};
		}else{
			_woundSev = "Major";
			_newWound = [_woundLoc, _woundSev, 1, "Piercing", false];
			_A3PI_Wounds pushback _newWound;
			if(_woundLoc == 2)then{
				
					_A3PI_Blood = _A3PI_Blood - 2000;
				
			}else{
				
					_A3PI_Blood = _A3PI_Blood - 1000;
				
			};
		};
	};
}else{
	if(_proj < 25) then {
		_woundSev = "Minor";
		_newWound = [_woundLoc, _woundSev, 0, "Impact", false];
		_A3PI_Wounds pushback _newWound;
		
			_A3PI_Blood = _A3PI_Blood - 250;
		
	}else{
		if(_proj < 50) then {
		_woundSev = "Internal";
		_newWound = [5, _woundSev, 1, "Impact", false];
		_A3PI_Wounds pushback _newWound;
		
			_A3PI_Blood = _A3PI_Blood - 700;
		
		}else{
		_woundSev = "Major";
		_newWound = [_woundLoc, _woundSev, 0, "Impact", false];
		_A3PI_Wounds pushback _newWound;
		
			_A3PI_Blood = _A3PI_Blood - 500;
		
		};
	};
	
};

if(_A3PI_Blood < 250)then{
	_A3PI_Blood = 249;
	player allowDamage false;
	if(vehicle player != player)then{player action ["Eject", vehicle player]};
				player setVariable ['A3PI_Cuffed',0,true]; 
				[1] call A3PI_fnc_restrainAdditions;
				player setUnconscious true;
				
				player setVariable["tf_voiceVolume",0,true];
				player setVariable["tf_unable_to_use_radio",true];
				player setVariable["Uncon", true,true];
};
A3PI_Blood = _A3PI_Blood;
//WoundSev based off of Ammo Hit (Greater than x)

//Wound Location , Wound Severity, Wound Bleeding, Wound Type, Wound Untreated.



//_woundLocation = format["A3PI_WoundPos%1",

player setVariable ["A3PI_CurrentWounds",_A3PI_Wounds,true];

//
//open Med Menu
//remoteExec wound treated
//gets from array and removes to reduce blood loss.
//
//
//
//