//Steve
disableSerialization;
params[
	"_illness"
];

if(_illness isEqualTo "wrongBlood")exitWith{

	enableCamShake true;
	addCamShake [2, 240, 10];

};
if(_illness isEqualTo "infection")exitWith{

	enableCamShake true;
	addCamShake [4, 120, 20];

};