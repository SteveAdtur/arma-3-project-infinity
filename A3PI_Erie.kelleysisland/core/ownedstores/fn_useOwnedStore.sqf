// Heisen


params [
	"_cursorObject"
];

_storeData = _cursorObject getVariable "storeData";
_storeDataSelling = _storeData select 5;

createDialog "RscDisplay_ShopUse"; 
 
((findDisplay 19854) displayCtrl 1102) ctrlSetText format ["Owner: %1",_storeData select 1 select 0];
((findDisplay 19854) displayCtrl 1100) ctrlSetText format ["Stock: %1",_storeData select 2];

if ((getPlayerUID player) isEqualTo (_storeData select 1 select 1)) then {
	if (_storeDataSelling) then {
		((findDisplay 19854) displayCtrl 1702) ctrlSetText "Stop Sell";
		((findDisplay 19854) displayCtrl 1702) buttonSetAction "(cursorObject getVariable 'storeData') set [5,false]; closeDialog 0;";
	} else {
		_storeData set [5,true];
	};
} else {
	if !(_storeDataSelling) then {
		((findDisplay 19854) displayCtrl 1702) ctrlShow false;
	};
};

//_storeData = [storeid,[name player,getplayeruid player],10,100421,1,true]; 
// 1702