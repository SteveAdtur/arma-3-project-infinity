// Bohemia Interactive, Heisen


if (player getVariable "A3PI_Tazed") exitWith {};
player setVariable ["A3PI_Tazed",true,true];

[localize"STR_Notification_Tazed",10,"red"] call A3PI_fnc_msg;

55 cutRsc ["taser_hit_fx","PLAIN",2];

disableUserInput true;
enableCamShake true;
addCamShake [5, 55, 75];
sleep 5;
55 cutText ["","PLAIN",1.5];
0 = ["ColorCorrections", 1500, [1, 0.4, 0, [0, 0, 0, 0], [1, 1, 1, 0], [1, 1, 1, 0]]] spawn 
{
	params ["_name", "_priority", "_effect", "_handle"];
	while {
		_handle = ppEffectCreate [_name, _priority];
		_handle < 0
	} do {
		_priority = _priority + 1;
	};
	_handle ppEffectEnable true;
	_handle ppEffectAdjust _effect;
	_handle ppEffectCommit 5;
	sleep 10; 
	enableCamShake false;
	player setFatigue 1;
	_handle ppEffectEnable false;
	ppEffectDestroy _handle;
};

sleep 10;
player setVariable ["A3PI_Tazed",false,true];
disableUserInput false;
[localize"STR_Notification_TazedRecovered",10,"green"] call A3PI_fnc_msg;