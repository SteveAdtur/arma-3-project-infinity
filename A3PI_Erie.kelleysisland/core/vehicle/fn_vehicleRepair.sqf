//Steve

params[
	"_curOb"
	];
	
_toolCount = call compile "Item_Toolkit";
	
if(_toolCount == 0)exitWith{
["No tool kit",10,"red"]call A3PI_fnc_msg;
};

_refineTime = 20;
	
	["Repairing Vehicle",10,"blue"] call A3PI_fnc_msg;
		
		32 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Repairing vehicle";
		_pos = getPos player;
		
		for "_i" from 0 to _refineTime step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / _refineTime));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			sleep 1;
			if (player distance _pos > 10) exitWith {
				32 cutText ["","PLAIN"];
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3PI_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		
		
		["Repairing Completed",10,"green"] call A3PI_fnc_msg;
		32 cutText ["","PLAIN"]; // Remove Progress Bar
	
	
_curOb setDamage 0;