//Steve
private ["_itemsSave"];
_bankInput = parseNumber(ctrlText 1400);
_bankBalance = player getVariable "bankBalance";
if(_bankBalance >= _bankInput) then {

["Item_Cash",_bankInput,true] call A3PI_fnc_handleItem;
_itemsSave = [];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	if ((call compile(configName _x)) >= 1) then {
		_itemsSave pushback [configName _x,call compile(configName _x)];
	};
} forEach _items;
[1,_bankInput,_bankBalance,_itemsSave] call A3PI_fnc_updateBank;
closeDialog 0;

}else{
	[localize "STR_Notification_NoBank",10,red] call A3PI_fnc_msg;
};