// Heisen


private ["_countNum","_countWeightFactory"];
_countWeightFactory = 0;
_countNum = -1;

lbClear 1500;
lbClear 1502;

//--- Player inv
_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	_varName = configName _x;
	_varAmount = call compile _varName;
	
	_varOut = [_varAmount] call BIS_fnc_numberText;

	if (_varAmount >= 1) then {
		_countNum = _countNum + 1;
		_varDisplayName = getText (_x >> "displayName");
		_varWeightValue = getNumber (_x >> "weightValue");
		lbAdd [1500,format["%2 - (%1x %3.kg)",_varOut,localize(_varDisplayName),(_varWeightValue * _varAmount)]];
		lbSetPicture [1500, _countNum,(getText (_x >> "displayIcon"))];
		lbSetData [1500,_countNum,(configName _x)];
	};
} forEach _items;

//--- Factory storage
{
	_configName = _x select 0;
	_amount = _x select 1;
	
	_displayName = getText (missionConfigFile >> "Server_Items" >> _configName >> "displayName");
	_displayIcon = getText (missionConfigFile >> "Server_Items" >> _configName >> "displayIcon");
	
	_weight = getNumber (missionConfigFile >> "Server_Items" >> _configName >> "weightValue");
	_countWeightFactory = _countWeightFactory + (_weight * _amount);
	
	lbAdd [1502,format["%2 - (%1x %3.kg)",([_amount] call BIS_fnc_numberText),localize(_displayName),(_weight * _amount)]];
	lbSetPicture [1502,_forEachIndex,_displayIcon];
	lbSetData [1502,_forEachIndex,_configName];
} forEach A3PI_FactoryInventory;

ctrlSetText [1001,format["%1/%2kg",A3PI_Weight,A3PI_MaxCarry]];
ctrlSetText [1003,format["%1/%2kg",_countWeightFactory,A3PI_FactoryMaxCarry]];

[] spawn A3PI_fnc_updateCraftFactory;