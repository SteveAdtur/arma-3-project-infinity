// Heisen


if (isNil "A3PI_FactoryInventory") exitWith {
	hint "something went wrong.. factory inventory is nil";
};


//--- Open factory dialog
createDialog "RscDisplay_Factory";


//--- Update Player inv & factory contents
call A3PI_fnc_updateFactory;
