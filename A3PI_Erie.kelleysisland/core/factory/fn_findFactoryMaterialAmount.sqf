// Heisen

private ["_return_number"];
params [
	"_material_name"
];
_return_number = 0;
{
	_find = _x find _material_name;
	if !(_find isEqualTo -1) then {
		_return_number = _x select 1;
	};
} forEach A3PI_FactoryInventory;
_return_number;