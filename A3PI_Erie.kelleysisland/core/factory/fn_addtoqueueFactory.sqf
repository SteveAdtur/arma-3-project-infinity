// Heisen


private ["_countNum","_exit"];
_countNum = -1;
_exit = false;

_selected = lbCurSel 1500;
if (_selected isEqualTo -1) exitWith { hint "Nothing Selected."; };


_configName = lbData [1500,_selected];
_rscEdit = ctrlText 1400;


if (A3PI_antiSpam) exitWith { hint "In recent action, please wait.."; };

if (parsenumber _rscEdit isEqualTo 0) exitWith { hint "Invalid Quanity."; };
if ((parseNumber _rscEdit) > (call compile _configName)) exitWith { hint "Invalid Quanity."; };

A3PI_antiSpam = true;

[_configName,(parseNumber _rscEdit),false] call A3PI_fnc_handleItem;
hint str _rscEdit;

{
	_countNum = _countNum + 1;
	if !(((_x select 0) find _configName) isEqualTo -1) exitWith {
	
		_new = (_x select 1) + (parseNumber _rscEdit);
		(A3PI_FactoryInventory select _countNum) set [1,_new];
		
		call A3PI_fnc_updateFactory;
		_exit = true;
		sleep 5;
		A3PI_antiSpam = false;
		systemchat "debug: gothere : exitwith scope";
	};
} forEach A3PI_FactoryInventory;

if (_exit) exitWith {};

systemchat "debug: gothere2: past scope";

A3PI_FactoryInventory pushback [_configName,parseNumber _rscEdit];


comment "//--- Update Player inv & factory contents";
call A3PI_fnc_updateFactory;

lbSetCurSel[1500,-1];

sleep 5;
A3PI_antiSpam = false;
