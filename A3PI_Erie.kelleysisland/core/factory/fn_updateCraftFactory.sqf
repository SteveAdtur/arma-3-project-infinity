// Heisen
// W.I.P SYSTEM

hintSilent "0";

lbAdd [1501,"Loading.."];
waitUntil {!A3PI_antiSpam}; 

lbClear 1501;

hintSilent "1";

private ["_product_can_build","_inv_material_amount_actual_array"];
_product_can_build = 0;
_inv_material_amount_actual_array = [];

_missionConfigFile_Recipes = (missionConfigFile >> "Server_Factory" >> "Recipes");

{
	hintSilent "2";
	_configName = _x;
	_materials_needed_Array = getArray (_configName >> "Materials");
	hintSilent "3";
	{
	
		_material_name = _x select 0;
		_material_amount = _x select 1;
		
		_inv_material_amount = [_material_name] call A3PI_fnc_findFactoryMaterialAmount;
		hintSilent "4";
		if (_inv_material_amount >= _material_amount)  then {
			hintSilent "5";
			_inv_material_amount_actual = floor (_inv_material_amount / _material_amount);
			_inv_material_amount_actual_array pushback _inv_material_amount_actual;
		};
		
	} forEach _materials_needed_Array;
	
	_inv_material_amount_creatable = [_inv_material_amount_actual_array] call A3PI_fnc_arrayLowestNumber;
	
	if ((count _inv_material_amount_actual_array) isEqualTo (count _materials_needed_Array)) then {
		_weight = getNumber (missionConfigFile >> "Server_Items" >> (getText(_configName >> "Variable")) >> "weightValue");
		lbAdd [1501,(format["%1 (%2x %3.kg)",(getText(_configName >> "DisplayName")),_inv_material_amount_creatable,(_weight * _inv_material_amount_creatable)])];
	};
	systemChat str _inv_material_amount_actual_array;
	
} forEach ("true" configClasses (_missionConfigFile_Recipes >> "Items"));