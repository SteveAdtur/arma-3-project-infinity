//Steve

params[
	"_target",
	"_newState",
	"_chosenLicense"
];

_newLicense = _target getVariable "A3PI_License";

systemChat format["Old License = %1",_newLicense];
_newLicense select _chosenLicense set[1,_newState];
systemChat format["New License = %1",_newLicense];

player setVariable["A3PI_Driver_License",_newLicense select 0 select 1,true];
player setVariable["A3PI_Truck_License",_newLicense select 1 select 1,true];
player setVariable["A3PI_Pilot_License",_newLicense select 2 select 1,true];
player setVariable["A3PI_Firearm_License",_newLicense select 3 select 1,true];
player setVariable["A3PI_Rifle_License",_newLicense select 4 select 1,true];

[_target,_newLicense] remoteExec ["A3PIsys_fnc_updateLicense",2];