//Author: Steve Adtur

private _shop = param [0, "", [""]];

_countNum = -1;

try {
	if (_shop isEqualTo "") throw (getText(missionConfigFile >> "Server_Vehicles" >> "localization" >> "msgParamEmpty"));
	if !(isNull objectParent player) throw (getText(missionConfigFile >> "Server_Vehicles" >> "localization" >> "msgInVehicle"));

    private _cfg = missionConfigFile >> "Server_Vehicles" >> "shops" >> _shop;

    if !(isClass _cfg) throw (getText(missionConfigFile >> "Server_Vehicles" >> "localization" >> "msgShopExists"));
    if !(call compile (getText(_cfg >> "condition"))) throw (getText(missionConfigFile >> "Server_Vehicles" >> "localization" >> "msgCondition"));

    if (getNumber (_cfg >> "simple") isEqualTo 0) then {



        createDialog "RscDisplay_VehShop";
		
		waitUntil {!isNull (findDisplay 29872);};
		
		private _vehArray  = getArray(missionConfigFile >> "Server_Vehicles" >> "shops" >> _shop >> "vehicles");
		
		player setVariable ["currentShop", _shop,true];
		
		
		{
			_vehClass = _x select 0;
			_vehName = getText(configFile >> "CfgVehicles" >> _x select 0 >> "displayName");
			_vehPrice = _x select 1;
			_countNum = _countNum + 1;
			if( _x select 3 != "")then{_vahName = _x select 3};
			lbAdd [1500,format["%1 $%2", _vehName, _vehPrice]];
			lbSetData[1500,_countNum, _vehClass];
		
		}forEach _vehArray;
		

    };

}catch {
	hint _exception;
};