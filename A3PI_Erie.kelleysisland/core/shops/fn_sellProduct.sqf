// Steve Adtur
_ctrl1 = (findDisplay 19874) displayCtrl 1101;
_itemNumber = lbCurSel 1500;
_itemName = lbData [1500,_itemNumber];


_cost = getNumber(missionConfigFile >> "Server_Items" >> _itemName >> "sellValue");

_sellAmount = parseNumber(ctrlText 1400);
_income = _cost * _sellAmount;

if !(_sellAmount >=1) exitWith {["0 Selected!",10,"red"] call A3PI_fnc_msg;};


	[_itemName, _sellAmount, false] call A3PI_fnc_handleItem;
	["Item_Cash", _income, true] call A3PI_fnc_handleItem;
call A3PI_fnc_refreshShopDialog;