// Heisen


if (isNil "A3PI_ShopShowClass") then {
	A3PI_ShopShowClass = [uniform player,vest player,backpack player,headgear player,assigneditems player,backpackitems player,currentWeapon player,magazines player];
};

_configName = (lbData [1501,lbCurSel 1501]);
_config = (missionConfigFile >> "Server_Items" >> _configName);
_typeClass = getText (_config >> "physicalClassName");
_type = getText (_config >> "physicalClassSpecificBracket");
	
call {
	if (_type isEqualTo "Uniforms") exitWith {
		player addUniform _typeClass;
	};
	if (_type isEqualTo "Vests") exitWith {
		player addVest _typeClass;
	};
	if (_type isEqualTo "Backpacks") exitWith {
		removebackpack player;
		player addBackpack _typeClass;
	};
	if (_type isEqualTo "Headgear") exitWith {
		_wepORmag = getText(_config >> "physicalClassType");
		if (_wepOrMag isEqualTo "linkItem") then {
			player linkItem _typeClass;
		} else {
			player addHeadgear _typeClass;
		};
	};
};