//Author : Steve Adtur /Sikoras

_vehNumber = lbCurSel 1500;


if( _vehNumber == -1)exitWith {["no vehicle selected!",10,red] call A3PI_fnc_msg;};

_shop = player getVariable "currentShop";

_vehConfig = getArray(missionConfigFile >> "Server_Vehicles" >> "shops" >> _shop >> "vehicles");

_spawnPoint = getText(missionConfigFile >> "Server_Vehicles" >> "shops" >> _shop >> "spawnPoint");

_vehCost = (_vehConfig select _vehNumber) select 1;
_vehClass = (_vehConfig select _vehNumber) select 0;
_trunkSpace =(_vehConfig select _vehNumber) select 4;
if ((call compile "Item_Cash") < _vehCost) exitWith {["not enough Cash!", 10, "red"] call A3PI_fnc_msg;};

_spawnPos = getMarkerPos _spawnPoint;

_nearOb = nearestObjects[_spawnPos,["Car","Truck"],10];
if((count _nearOb) > 0) exitWith {["vehicle blocking point!", 10, "red"] call A3PI_fnc_msg;};
systemchat format ["Output info: Class = %1, Number = %2",_vehClass, _vehNumber];
[player, _vehClass,_spawnPoint, _trunkSpace] remoteExec ["A3PIsys_fnc_insertVehicle",2];

["Item_Cash", _vehCost, false] call A3PI_fnc_handleItem;
["Vehicle Purchased", 10, "green"] call A3PI_fnc_msg;
player setVariable ["currentShop", "null",true];
closeDialog 0;