// Heisen


_item = (missionConfigFile >> "Server_Items" >> (lbData[1501,lbCurSel 1501]));
_type = getText(_item >> "physicalClassSpecificBracket");
_physical = getText(_item >> "physicalClassName");
_wepORmag = getText(_item >> "physicalClassType");
_price = getNumber(_item >> "buyValue");

if ((call compile "Item_Cash") < (_price * A3PI_Tax)) exitWith {
	[localize"STR_Notification_NoFunds",10,"red"] call A3PI_fnc_msg;
};
	
call {
	if (_type isEqualTo "Items") exitWith {
		if (_wepOrMag isEqualTo "linkItem") then {
			player linkItem _physical;
		} else {
			//virtual item add
		};
	};
	if (_type isEqualTo "Uniforms") exitWith {
		player forceAddUniform _physical;
		A3PI_ShopShowClass set [0,_physical];
		// take cash
	};
	if (_type isEqualTo "Vests") exitWith {
		player addVest _physical;
		A3PI_ShopShowClass set [1,_physical];
	};
	if (_type isEqualTo "Backpacks") exitWith {
		player addBackpack _physical;
		A3PI_ShopShowClass set [2,_physical];
	};
	if (_type isEqualTo "Headgear") exitWith {
		if (_wepOrMag isEqualTo "linkItem") then {
			player linkItem _physical;
			A3PI_ShopShowClass pushback _physical;
		} else {
			player addHeadgear _physical;
			A3PI_ShopShowClass set [3,_physical];
		};
	};
	if (_type isEqualTo "Weapons/Magazines") exitWith {
		switch (_wepORmag) do {
			case "cfgWeapons": {
				A3PI_ShopShowClass set [6,_physical];
			};
			case "cfgMagazines": {
				(A3PI_ShopShowClass select 7) pushback _physical;
			};
		};
		hint format ["%1 | %2",_wepORmag,_physical];
	};
};
	
systemChat format ["item: %1 |type: %2 |phyx: %3 |wepormag: %4",_item,_type,_physical,_wepORmag];
["Item_Cash",(_price * A3PI_Tax),false] call A3PI_fnc_handleItem;
[localize"STR_Notification_Purchased",10,"green"] call A3PI_fnc_msg;