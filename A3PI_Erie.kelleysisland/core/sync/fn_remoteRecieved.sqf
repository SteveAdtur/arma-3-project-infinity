/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_remoteRecieved.sqf
Author: Heisen http://heisen.pw
Description: Pass through A3PI_dataRemoteRecieved var data.
Parameter(s): N/A
************************************************************/


params [
	"_packet"
];

A3PI_dataRemoteRecieved = _packet;

diag_log format ["A3PI Client REMOTE RECIEVED new PACKET(%1)",_packet];