// Heisen


params [
	"_player"
];

if !(A3PI_JobCurrent isEqualTo "TowTrucker") exitWith {};

[(format["%1 a Police Officer has requested a Tow Truck!",name _player]),10,"blue"] call A3PI_fnc_msg;

_markerName = format ["Marker_Tow_Trucker_%1_%2",_player,(round(random 9999))]; 
_markerStr = createMarkerLocal [_markerName,(getPos _player)]; 
_markerName setMarkerColorLocal "ColorRed"; 
_markerName setMarkerTextLocal "Tow Truck Requested!"; 
_markerName setMarkerShapeLocal "ICON";
_markerName setMarkerTypeLocal "mil_objective";

sleep 180; //--- 3 Minutes

deleteMarkerLocal _markerName;
hint format["Marker Deleted %1",_markerName];
