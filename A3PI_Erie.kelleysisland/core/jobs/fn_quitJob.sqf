// Heisen


params [
	"_type"
];


A3PI_JobActive = false;
A3PI_JobRecent = true;

if (A3PI_JobCurrent isEqualTo "TowTrucker") then {
	_find = A3PI_TOWTRUCKER_PLAYERLIST find (player);
	A3PI_TOWTRUCKER_PLAYERLIST deleteAt _find;
	publicVariable "A3PI_TOWTRUCKER_PLAYERLIST";
	deleteVehicle theJobVehicle;
};

A3PI_JobCurrent = "";

if (_type isEqualTo 0) exitWith {
	hint "You've quit your Job!";
};


if (_type isEqualTo 1) exitWith {
	hint "You've moved too far away from your job vehicle, quiting vehicle!";
};

sleep 300;
A3PI_JobRecent = false;

