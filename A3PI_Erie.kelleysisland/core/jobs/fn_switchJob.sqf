// Heisen
// this needs to be spawned


params [
	"_type"
];


if (A3PI_JobActive) exitWith { hint "Please resign from your current job before searching for employment!"; };  
if (A3PI_JobRecent) exitWith { hint "You've had a job recently.. try again later!"; };


_job = (missionConfigFile >> "Server_Jobs" >> _type);
_jobVehicle = getText(_job >> "jobVehicle");

//--- Setup new job!
A3PI_JobCurrent =  _type;

if !(_jobVehicle isEqualTo "") then {

	hint format ["You've been employed as a %1",getText(_job >> "jobTitle")];

	//--- Setup job vehicle.
	_near = nearestObjects [(getMarkerPos "Job_Vehicle_Spawn"),["Car","Truck","Air","Tank"],10];
	if !(_near isEqualTo []) exitWith { 
		hint "Vehicle can't spawn, signing off job.";
		A3PI_JobCurrent = "";
	};

	A3PI_JobActive = true;

	A3PI_TOWTRUCKER_PLAYERLIST pushback (player);
	publicVariable "A3PI_TOWTRUCKER_PLAYERLIST";
	
	theJobVehicle = _jobVehicle createVehicle (getMarkerPos "Job_Vehicle_Spawn");
	theJobVehicle setVariable ["isJobVehicle",true,true];

	for "_i" from 0 to 1 step +0 do {
		if ((player distance theJobVehicle) > 25) exitWith {
			[1] spawn A3PI_fnc_quitJob;
		};
		sleep 3;
	};
	
} else {
	hint format ["You've been employed as a %1 , stay within 25m of your tow truck.",getText(_job >> "jobTitle")];
	A3PI_JobActive = true;
	
	A3PI_TOWTRUCKER_PLAYERLIST pushback (player);
	publicVariable "A3PI_TOWTRUCKER_PLAYERLIST";
};