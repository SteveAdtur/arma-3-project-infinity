/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_variables.sqf
Author: Heisen http://heisen.pw
Description: Handle Usage of an Item
Parameter(s): N/A
************************************************************/

A3PI_SyncRecent = false;
A3PI_dataRemoteRecieved = [];

A3PI_Jailed = false;

A3PI_Bank = 5000;

A3PI_Blood = 5000;


A3PI_Thirst = 100;
A3PI_Hunger = 100;

A3PI_Weight = 0;
A3PI_MaxCarry = getNumber (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "maxCarry");
A3PI_FactoryMaxCarry = 20000;

A3PI_antiSpam = false;

A3PI_CopOnDuty = false;
A3PI_medicOnDuty = false;

A3PI_Spawning = true;
A3PI_VehicleInteract = "";

A3PI_RespawnReady = false;

A3PI_JobActive = false;
A3PI_JobRecent = false;
A3PI_JobCurrent = "";

A3PI_SandMiner = 0;
A3PI_inAnimation = false;

A3PI_Wounds = [];

player setVariable ["_onDrugs",false,true];
player setVariable ["_showName",true,true];

player setVariable ["_haveGloves",false,true];
player setVariable ["_bloodLevel",A3PI_Blood,true];
player setVariable["_revived",false,true];
player setVariable ["A3PI_Tazed",false,true];
player setVariable ["A3PI_cuffed",0,true]; //0: not restrain | 1: restrain | 2: floor | 3: walk
player setVariable ["A3PI_drag",false,true];
player setVariable ["Uncon",false,true];
player setVariable ["A3PI_NameSetting",false,true];
player setVariable ["A3PI_CopOnDuty",false,true];
player setVariable ["A3PI_MedicOnDuty",false,true];
player setVariable ["A3PI_CurrentWounds", A3PI_Wounds, true];
player setVariable ["coplevel",0,true];