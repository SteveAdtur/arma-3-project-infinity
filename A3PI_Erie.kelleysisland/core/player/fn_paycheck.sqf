/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_survival.sqf
Author: Heisen http://heisen.pw
Description: Survival aspect, decrease hunger/food. - spawn this
Parameter(s): N/A
************************************************************/


_incomeConfig = (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Income");


_incomeBase= getNumber (_incomeConfig >> "baseIncome"); 
_multiple = 1;


	if(player getVariable "A3PI_CopOnDuty") then
	{ 
		_duty = A3PI_copLevel;
		_multiplier = _duty;
		_multiple = _multiple + _multiplier;
	}else{
		_multiple = 1;
	};
	if(player getVariable "A3PI_MedicOnDuty") then
	{ 
		_duty = A3PI_medicLevel;
		_multiplier = _duty;
		_multiple = _multiple + _multiplier;
	}else{
		_multiple = 1;
	};
	_outIncome = _incomeBase*_multiple;
	["Item_Cash",_outIncome,true] call A3PI_fnc_handleItem;
	["You recieved a paycheque",10,"green"] call A3PI_fnc_msg;


