/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_getGear.sqf
Author: Heisen http://heisen.pw
Description: Returns gear of player
Parameter(s): N/A
************************************************************/

//return: -- uniform, vest, backpack, hat, assigneditems, items, primaryweapon, secondaryweapon, primaryweaponattachments, secondarygunattachments, magazines

_uniform = uniform player;
_vest = vest player;
_backpack = backpack player;
_hat = headgear player;
_assignedItems = assigneditems player;
_items = items player;
_primaryWeapon = primaryWeapon player;
_secondaryWeapon = handgunWeapon player;
_primaryWeaponAttachments = primaryWeaponItems player;
_secondaryWeaponAttachments = secondaryWeaponItems player;
_magazines = magazines player;

_return = [_uniform,_vest,_backpack,_hat,_assignedItems,_items,_primaryWeapon,_secondaryWeapon,_primaryWeaponAttachments,_secondaryWeaponAttachments,_magazines];
_return;