// Heisen


private ["_data"];

createDialog "RscDisplay_Bec";

A3PI_AdminSpawnBox = "ITEMS";
["SWITCH"] call A3PI_fnc_adminRequest;

{
	if (alive _x) then {
		_data = format["(%2) %1",name _x,_x getVariable "CommunicationID"];
	} else {
		_data = format ["(%1) DEAD",_x getVariable "CommunicationID"];
	};
	lbAdd [1500,_data];
	lbSetData [1500,_forEachIndex,str(_x getVariable "CommunicationID")];
} forEach (allPlayers);

// 1(Events),2(Senior-Admin),3(Head-Admin),4(PM,PL,*devs for testing Kappa.*) - LOGS ENFORCED TO ALL!

call {
	if (A3PI_StaffLevel isEqualTo 1) exitWith {
		[localize"STR_AdminMenu_AdminOpenEvents",10,"blue"] call A3PI_fnc_msg;
		{
			ctrlShow [_x,false];
		} forEach [2409,2408,2407,2406,2405,2404,2403,2402,2401,2400,1400,1501];
	};

};