// Heisen


params [
	"_admin"
];

[format[localize"STR_AdminMenu_AdminRevivePlayer",name _admin],10,"green"] call A3PI_fnc_msg;

player setDamage 0;
[false] call A3PI_fnc_setUnconcious;
[player,""] remoteExec ["switchMove",0,true];

player setVariable ["revived",true,true];