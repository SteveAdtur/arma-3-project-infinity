[] call compile preprocessFile "dialogs\functions\A3PI_fnc_msg.sqf";

["System: Waiting for Server to be Ready!",10,"blue"] call A3PI_fnc_msg;
waitUntil {!isNil{A3PI_Server_isReady}};
["System: Server Ready!",10,"blue"] call A3PI_fnc_msg;

[format ["System: Checking for existing data.. (%1)",getPlayerUID player],10,"blue"] call A3PI_fnc_msg;
player setVariable ["CommunicationID",clientOwner,true];
[player] remoteExec ["A3PIsys_fnc_existPlayer",2];
[format ["System: Check for (%1) completed!",getPlayerUID player],10,"blue"] call A3PI_fnc_msg;

call A3PI_fnc_variables;
call A3PI_fnc_setupItems;
call A3PI_fnc_eventHandlers;
[] spawn A3PI_fnc_survival;
[] spawn A3PI_fnc_createMarkers;
[] spawn A3PI_fnc_tfrCheck;
[] spawn A3PI_fnc_bloodloss;
//[] spawn A3PI_fnc_paycheck;

player addItem "ItemMap"; // temp
player assignItem "ItemMap"; // temp

player setVariable ["trueDead", 0, true];



waitUntil { !(isNull (findDisplay 46)) };
A3PI_KeyHandler = (findDisplay 46) displayAddEventHandler ["KeyDown", "[_this] call A3PI_fnc_setupKeyHandler"];

player enableSimulation false;
call A3PI_fnc_spawnMenu;

42 cutRsc ["A3PIhud", "PLAIN"];
[] call compile preprocessFile "dialogs\functions\A3PI_fnc_initHud.sqf";


[] spawn {
	[("Welcome to"), ("Project Infinity")] spawn BIS_fnc_infoText;
	sleep 5;
	[("If you find a bug.."), ("Submit a bug report!")] spawn BIS_fnc_infoText;
};