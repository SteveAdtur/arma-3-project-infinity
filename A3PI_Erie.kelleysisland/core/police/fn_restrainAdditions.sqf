/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_restrainAdditions.sqf
Author: Heisen http://heisen.pw
Description: Restrain Actions.. Anim Sync w/ Switch Move
Parameter(s): N/A
************************************************************/


params [
	"_type"
];

call {
	if (_type isEqualTo -1) exitWith {
		[player,""] remoteExec ["switchMove",0,true];
		player setVariable ["A3PI_Cuffed",1,true];
		[] spawn A3PI_fnc_cuff;

	};
	if (_type isEqualTo 0) exitWith {
		player setVariable ['A3PI_Cuffed',2,true];
		player playMovenow "Acts_AidlPsitMstpSsurWnonDnon_loop";
		[] spawn A3PI_fnc_cuff;

	};
	if (_type isEqualTo 1) exitWith {
		[player,""] remoteExec ["switchMove",0,true];
		player setVariable["tf_unable_to_use_radio",false];
	};
};

