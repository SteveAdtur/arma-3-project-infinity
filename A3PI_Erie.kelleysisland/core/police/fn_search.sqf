/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_search.sqf
Author: Heisen http://heisen.pw
Description: Search Person for all Physical Items within.
Parameter(s): N/A
************************************************************/


//return: -- uniform, vest, backpack, hat, assigneditems, items, primaryweapon, secondaryweapon, primaryweaponattachments, secondarygunattachments, magazines
[] spawn {
	[localize"STR_Notification_BeingSearchPlayer",10,"red"] call A3PI_fnc_msg;

	private ["_holder"];

	_primaryWeapon = primaryWeapon player;
	_secondaryWeapon = handgunWeapon player;

	_primaryWeaponItems = primaryWeaponItems player;
	_secondaryWeaponItems = secondaryWeaponItems player;

	_holder = createVehicle ["groundweaponholder",getPos player,[],0,"CAN_COLLIDE"]; //--- Create ground weapon holder
	
	{
		_holder addItemCargoGlobal [_x,1];
		player removeItemFromBackpack _x;
	} forEach (backpackItems player);
	
	{
		_holder addItemCargoGlobal [_x,1];
		player removeItemFromVest _x;
	} forEach (vestItems player);
	
	{
		_holder addItemCargoGlobal [_x,1];
		player removeItemFromUniform _x;
	} forEach (uniformItems player);
	
	{
		_holder addItemCargoGlobal [_x,1];
		player unassignItem _x;
	} forEach (assigneditems player);

	_holder addWeaponCargoGlobal [_primaryWeapon,1];
	_holder addWeaponCargoGlobal [_secondaryWeapon,1];
	
	{ 
		_holder addItemCargoGlobal [_x,1];
	} forEach _primaryWeaponItems;
	
	{ 
		_holder addItemCargoGlobal [_x,1];
	} forEach _secondaryWeaponItems;
	
	_holder addItemCargoGlobal [(goggles player),1];
	_holder addItemCargoGlobal [(headgear player),1];

	player removeWeaponGlobal _primaryWeapon;
	player removeWeaponGlobal _secondaryWeapon;

	removeGoggles player;
	removeHeadgear player;
	
	removeAllItems player;
};