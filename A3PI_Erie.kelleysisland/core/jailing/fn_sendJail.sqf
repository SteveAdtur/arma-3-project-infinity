// Heisen


_hours = ctrlText 1400;
_minutes = ctrlText 1401;
_seconds = ctrlText 1402;
_reasons = ctrlText 1403;

systemChat "1";

//--- Bad Player Check.
if !(isPlayer cursorObject) exitWith {
	[localize"STR_Notification_BadCurOBj",10,"red"] call A3PI_fnc_msg;
};

//--- Bad Character Input Checks.
	{
		if !(_x IN ["1","2","3","4","5","6","7","8","9"]) exitWith {
				[localize"STR_Notification_BadInput",10,"red"] call A3PI_fnc_msg;
		};
	} forEach ((_hours + _minutes + _seconds) splitString "");

systemChat "2";

//--- Close send jail dialog.
closeDialog 0; 

[format[localize"STR_Notification_SenderJail",(name cursorObject),_hours,_minutes,_seconds]] call A3PI_fnc_msg;

_timeArray = [(parseNumber(_hours)),(parseNumber(_minutes)),(parseNumber(_seconds))];
[cursorObject,_timeArray,_reasons,1] remoteExec ["A3PIsys_fnc_insertJail",2];

systemChat "3";