/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_plantSeed.sqf
Author: Heisen http://heisen.pw
Description: Plant a seed.
Parameter(s): N/A
************************************************************/


params ["_type"];

_base = (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Farming");
_plants = getArray (_base >> "farmingPlants");
_surfaceTypes = getArray (_base >> "farmingSurfaces");
_surfaceUnder = getArray (_base >> "farmingGround");
_tooClose = getNumber (_base >> "farmingTooClose");

hint str(_surfaceTypes);

if !(((surfaceType(getPos player)) IN _surfaceTypes)) exitWith {
	["Invalid growing surface",10,"red"] call A3PI_fnc_msg;
};

if !((nearestObjects[player,[(_plants select _type)],_tooClose]) isEqualTo []) exitWith {
	["Cant plant this close to another plant..",10,"red"] call A3PI_fnc_msg;
};

_plantPosition = [getPos player select 0,getPos player select 1,(getPos player select 2) - (_surfaceUnder select _type)];
_plant = createVehicle [_plants select _type,_plantPosition, [], 0, "CAN_COLLIDE"];

[_plant,_type] remoteExec ["A3PIsys_fnc_growPlant",2];

[(getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Seeds")) select _type,1,false] call A3PI_fnc_handleItem;

["Planted Seed",10,"green"] call A3PI_fnc_msg;

