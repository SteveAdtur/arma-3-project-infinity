/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_chopTree.sqf
Author: Heisen http://heisen.pw
Description: Chop trees with an Axe!
Parameter(s): N/A
************************************************************/


params [
	"_cursorObject"
];

_cutTrees = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_WoodCutting" >> "woodCutting");
_cutTreesLoot = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_WoodCutting" >> "woodCuttingLoot");

if !((typeOf _cursorObject) IN _cutTrees) exitWith {};

_currentDamage = cursorObject getVariable "treeHealth";
cursorObject setVariable ["treeHealth",(_currentDamage - (random(5))),true];

if (_currentDamage <= 1) exitWith {
	_position = getPos _cursorObject;
	_treeType = _cutTreesLoot select (_cursorObject getVariable "treeType");
	deleteVehicle _cursorObject;
	for "_i" from 0 to (round(random(3))) do {
		_newTree = createVehicle [_treeType,[(_position select 0) + random(3),(_position select 1) + random(3),(_position select 2)],[],0,"CAN_COLLIDE"];
		_newTree setVariable ["Server_Item_Data",["Item_WoodLog",1],true];
	};
};