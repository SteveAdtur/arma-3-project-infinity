// Heisen


params [
	"_cursorObject"
];


{
	_vehData = _x getVariable "vehicleData";
	if ((_vehData select 0) isEqualTo (getPlayerUID _cursorObject)) then {
		if ((getPlayerUID player) IN (_vehData select 1)) exitWith {
			[(format[localize "STR_Notification_KeysAlready",typeOf _x]),10,"red"] call A3PI_fnc_msg;
		};
		_push = (_vehData select 1) pushback (getPlayerUID player);
		[(format[localize "STR_Notification_KeysAdded",typeOf _x]),10,"green"] call A3PI_fnc_msg;
	};
} forEach (nearestObjects [player,["Car","Air","Boat"],10]);
