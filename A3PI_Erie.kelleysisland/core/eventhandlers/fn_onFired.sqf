/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_onFired.sqf
Author: Heisen http://heisen.pw
Description: onFired eventHandler
Parameter(s): N/A
************************************************************/


_weapon = currentWeapon player;

_meleeWeapons = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Weapons" >> "meleeWeapons");

if !(_weapon IN _meleeWeapons) exitWith {};
player playAction "GestureSwing";

call {
	if (_weapon isEqualTo "A3PI_Pickaxe2017") exitWith {
		[cursorObject] call A3PI_fnc_mine;
	};
	if (_weapon isEqualTo "A3PI_Hatchet2017") exitWith {
		[cursorObject] call A3PI_fnc_chopTree;
	};
	if (_weapon isEqualTo "A3PI_Spade2017") exitWith {
		[] call A3PI_fnc_mineSand;
	};
};

