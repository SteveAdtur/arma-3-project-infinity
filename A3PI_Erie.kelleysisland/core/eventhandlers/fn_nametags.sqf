// Heisen
// ["A3PI_Nametags", "onEachFrame"] call BIS_fnc_removeStackedEventHandler;


['A3PI_Nametags','onEachFrame',{
	{
		
		if (((player distance _x) <= 5) && !(_x isEqualTo player) && ((vehicle player) isEqualTo player)) then {
		

				_name = name _x;


			_headPosition = _x modelToWorld (_x selectionPosition "head") select 2;
		
			drawIcon3D [
				"", 
				[1,1,1,1], 
				[(visiblePosition _x select 0),(visiblePosition _x select 1),_headPosition + 0.55],
				0, 
				0, 
				0, 
				_name, 
				1, 
				0.035, 
				"PuristaMedium"
			];
			
		};
	} forEach (allPlayers);	
}] call BIS_fnc_addStackedEventHandler;