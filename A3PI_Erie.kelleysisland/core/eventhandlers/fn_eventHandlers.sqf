/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_eventHandlers.sqf
Author: Heisen http://heisen.pw
Description: EventHandlers
Parameter(s): N/A
************************************************************/


player addEventHandler ["Fired",{ [_this select 0] call A3PI_fnc_onFired; }];
player addEventHandler ["HandleDamage",{ [_this] call A3PI_fnc_handleDamageAdvanced; }];

player addEventHandler ["HandleDisconnect",{[_this] call A3PI_fnc_handleDisconnect; }];
player addEventHandler ["Killed",{[_this] call A3PI_fnc_killed;}];
player addEventHandler ["InventoryOpened",{
        if ((_this select 1 isKindOf "Man")or(_this select 1 isKindOf "Bag_base")) then {closeDialog 602; true}
}];
//player addEventHandler ["Take",{[_this] call A3PI_fnc_handleTake; }];

call A3PI_fnc_nametags;