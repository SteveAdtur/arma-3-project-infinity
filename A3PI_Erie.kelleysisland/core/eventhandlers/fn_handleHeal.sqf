//Steve

params[
	"_healData"
	];
_injured = _healData select 0;
_healer = _healData select 1;
_return = true;

	if (player getVariable "revived")then{	
		_damage = damage _injured;
		if !(_healer getVariable "MedicOnDuty")then{
			waitUntil {damage _injured != _damage};
			if (damage _injured != _damage) then{
				_injured setDamage 0.5;
			};
		}else{
			waitUntil {damage _injured != _damage};
			if (damage _injured < _damage ) then{
				_injured setDamage 0;
				player setVariable ["revived",false,true];
			};
		};
	}else{
		if !(_healer getVariable "MedicOnDuty")then{
			_damage = damage _injured;
			"Healer off Duty" remoteExec["hint"];
			waitUntil {damage _injured != _damage};
			if(damage _injured != _damage) then {
				_injured setDamage 0.35;
			};
		}else{
			_damage = damage _injured;
			"Healer on Duty" remoteExec["hint"];
			waitUntil {damage _injured != _damage};
			if(damage _injured != _damage) then {
				_injured setDamage 0;
			};
		};
	};
	_return;