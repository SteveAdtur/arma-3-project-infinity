// Heisen, Steve

params [
	"_damageData"
];

_ammo = _damageData select 4;

_taserRounds = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Weapons" >> "nonLethalRounds");
_tranqRounds = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Weapons" >> "tranqRounds");
_toRespawn = player getVariable "trueDead";
_newDmg = _damageData select 2;
_hitIndex = _damageData select 5;
private _hitSelect = _damageData select 1;
_hitter = _damageData select 3;
_hitterStr = format["%1",_hitter];


if(lifeState player == "DEAD")exitWith{};
if(A3PI_Spawning)exitWith{};
//if(lifeState player = "INCAPACITATED")exitWith{0;};

if (_ammo IN _taserRounds) then {
	if(_ammo IN _tranqRounds)then
	{
		
		[] spawn A3PI_fnc_tranqEffect;
		
			player setDamage (damage player);
		
	}else{
		
		call KK_fnc_forceRagdoll;
		[] spawn A3PI_fnc_x26taser_effect;
		
			player setDamage (damage player);
		
	};
	
}else{
	if !(player getVariable "A3PI_Tazed")then {
		if((_newDmg >= 1) && (_hitSelect IN ["face_hub","head","neck","pelvis","spine1","spine2","spine3","body",""])) then
		{
			if(_hitterStr != "<NULL-object>") then{
				if(vehicle player != player)then{player action ["Eject", vehicle player]};
					player setVariable ['A3PI_Cuffed',0,true]; 
					[1] call A3PI_fnc_restrainAdditions;
					player setDamage 0.9;
					player setUnconscious true;
					player allowDamage false;
					
					player setHitIndex [_hitIndex,0.9];
					_hitOut = format["hitIndex%1",_hitIndex];
					player setVariable[_hitOut,0.9,true];
					player setVariable["tf_voiceVolume",0,true];
					player setVariable["tf_unable_to_use_radio",true];
					[] spawn A3PI_fnc_respawn;
				}else{
					player setHitIndex [_hitIndex,_newDmg];
					_hitOut = format["hitIndex%1",_hitIndex];
					player setVariable[_hitOut,_newDmg,true];
				};
			};
		}else{
		"Testing" remoteExec ["hint"];
		player setDamage (damage player);
		};
};






