// Steve

params [
	"_damageData"
];

_ammo = _damageData select 4;

_taserRounds = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Weapons" >> "nonLethalRounds");
_tranqRounds = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Weapons" >> "tranqRounds");
_toRespawn = player getVariable "trueDead";
_newDmg = _damageData select 2;
_hitIndex = _damageData select 5;
private _hitSelect = _damageData select 1;
_hitter = _damageData select 3;
_hitterStr = format["%1",_hitter];
_hitProj = _damageData select 4;
_minDamage = 0.1;
_damage = 0;
//Left Arm = 8, Right Arm = 9, Left Leg = 12, Right Leg = 13;

if(lifeState player == "DEAD")exitWith{};
if(A3PI_Spawning)exitWith{};


if (_ammo IN _taserRounds) then {
	if(_ammo IN _tranqRounds)then
	{
		[] spawn A3PI_fnc_tranqEffect;
		player setDamage 0;
	}else{
		call KK_fnc_forceRagdoll;
		[] spawn A3PI_fnc_x26taser_effect;
		player setDamage 0;
	};
}else{
	if !(player getVariable "A3PI_Tazed")then {
		if(_newDmg > _minDamage && _hitSelect != "")then{
			if((_hitter == player) || ((vehicle _hitter) isKindOf "LandVehicle"))then {
			_vehSpeed = speed (vehicle _hitter);

			[_hitIndex, 0, _vehSpeed] call A3PI_fnc_injury;
			player setDamage 0;
			}else{
			
			[_hitIndex, 1, _hitProj] call A3PI_fnc_injury;
			player setDamage 0;
			};
		};
		
	};
};
player setDamage 0;



