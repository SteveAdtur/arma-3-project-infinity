// Steve Adtur
_ctrl1 = (findDisplay 19872) displayCtrl 1101;
_itemNumber = lbCurSel 1500;
_itemName = lbData [1500,_itemNumber];

_itemArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Crafting");


_caller = player;
_pweap = primaryWeapon _caller;

_outputString = "Missing: <br />";

//Land_Workbench_01_F
//Land_WoodenTable_large_F
_missingItems = 0;

if(_itemName IN _itemArray) then {
	
	_materialsReq = getArray(missionConfigFile >> "Server_Items" >> _itemName >> "materialsNeeded");
	{
		_itemX = (missionConfigFile >> "Server_Items" >> _x select 0);
		_varAmount = call compile (configName _itemX);
		systemChat format["%1 ",_varAmount];
		if(_varAmount < _x select 1) then {
			_outputString = format["%1 %2 x %3 <br />", _outputString, _x select 1,localize(getText(_itemX >> "displayName"))];
			_missingItems = _missingItems + 1;
		};
		
	}foreach _materialsReq;
	if(_missingItems > 0) then {
	_ctrl1 ctrlSetStructuredText parseText _outputString;
	}else{
		[_itemName, 1, true] call A3PI_fnc_handleItem;
		{
			[_x select 0, _x select 1, false] call A3PI_fnc_handleItem;
		}foreach _materialsReq;
	};

}else{
	_itemArray = getArray(missionConfigFile >> "Server_Crafting" >> "Server_Crafting_Guns");
	if(_itemName IN _itemArray) then {
		_materialsReq = getArray(missionConfigFile >> "Server_Crafting" >> _itemName >> "partsNeeded");
		_rifleReqClass = getText(missionConfigFile >> "Server_Crafting" >> _itemName >> "rifleNeeded");
		_rifleReq = getText(missionConfigFile >> "Server_Crafting" >> _itemName >> "rifleNeededName");
		_returned = getArray(missionConfigFile >> "Server_Crafting" >> _itemName >> "partsReturned");
		if(_pweap != _rifleReqClass) then {
		_outputString = format["%1 %2 x %3 <br />", _outputString, "1", _rifleReq];
		_missingItems = _missingItems + 1;
		};
		
		{
			_itemX = (missionConfigFile >> "Server_Items" >> _x select 0);
			_varAmount = call compile (configName _itemX);
			if(_varAmount < _x select 1) then {
			_outputString = format["%1 %2 x %3 <br />", _outputString, _x select 1,localize(getText(_itemX >> "displayName"))];
			_missingItems = _missingItems + 1;
			};
		
		}foreach _materialsReq;
		if(_missingItems > 0) then {
		_ctrl1 ctrlSetStructuredText parseText _outputString;
		}else{
		_outputWep = getText(missionConfigFile >> "Server_Crafting" >> _itemName >> "outputGun");
		_optic = _pweapItems select 2;
		_caller addItem _optic;
		removeAllPrimaryWeaponItems _caller;
		_caller removeWeapon _pweap;
		_caller addWeapon _outputWep;
		{
			[_x select 0, _x select 1, false] call A3PI_fnc_handleItem;
		}foreach _materialsReq;
		
		{
			[_x select 0, _x select 1, true] call A3PI_fnc_handleItem;
		}foreach _returned;
	};

	};
};