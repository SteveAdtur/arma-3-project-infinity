// Heisen


private ["_return_number"];
params [
	"_array"
];
_return_number = _array select 0;
if (_array isEqualTo []) exitWith {_return_number = 0;};
{
	if (_x < _return_number) then {
		_return_number = _x;
	};
} forEach _array;
_return_number;