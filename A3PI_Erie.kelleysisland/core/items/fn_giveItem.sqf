/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_giveItem.sqf
Author: Heisen http://heisen.pw
Description: Give Item
Parameter(s): N/A
************************************************************/


_player = cursorObject;

if ((lbCurSel 1500) isEqualTo -1) exitWith {hint "Nothing Selected!";};

if !(isPlayer _player) exitWith {};
if !(alive _player) exitWith {};

_item = (missionConfigFile >> "Server_Items" >> (lbData [1500,(lbCurSel 1500)]));
_itemName = lbData [1500,(lbCurSel 1500)];

if (parseNumber(ctrlText 1400) < 1) exitWith {hint "Selected 0, Invalid!";};
if ((parseNumber(ctrlText 1400)) > ((call compile _itemName))) exitWith {hint "Too High Capacity.";};

[(lbData [1500,(lbCurSel 1500)]),(parseNumber(ctrlText 1400)),false] call A3PI_fnc_handleItem;

[(lbData [1500,(lbCurSel 1500)]),(parseNumber(ctrlText 1400))] remoteExec ["A3PI_fnc_recieveItem",_player getVariable "CommunicationID"]; 