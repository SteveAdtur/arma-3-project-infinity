/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_calculateWeight.sqf
Author: Heisen http://heisen.pw
Description: Weight??
Parameter(s): N/A
************************************************************/


params [
	"_item",
	"_amount"
];

_weight = (getNumber (missionConfigFile >> "Server_Items" >> _item >> "weightValue")) * _amount;
_combined = A3PI_Weight + _weight;

systemChat format ["%1 %2",_weight,_combined];

if (_combined > A3PI_MaxCarry) then {
	_sum = true;
	_sum;
} else {
	_sum = false;
	_sum;
};