/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_itemCheck.sqf
Author: Heisen http://heisen.pw
Description: Check if Item or Not!
Parameter(s): N/A
************************************************************/


private ["_sum"];
_sum = false;

params [
	"_cursorObject"
];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	_varName = configName _x;
	_itemModel = getText(_x >> "displayModel");
	if (_itemModel isEqualTo (typeOf _cursorObject)) then {
		_sum = true;
	};
} forEach _items;

if ((typeOf _cursorObject) isEqualTo "") then {_sum = false;};

_sum;