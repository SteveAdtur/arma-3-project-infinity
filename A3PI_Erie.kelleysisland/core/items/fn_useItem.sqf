/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_useItem.sqf
Author: Heisen http://heisen.pw
Description: Handle Usage of an Item
Parameter(s): N/A
************************************************************/


if ((lbCurSel 1500) isEqualTo -1) exitWith {hint "Nothing Selected!";};

_item = (missionConfigFile >> "Server_Items" >> (lbData [1500,(lbCurSel 1500)]));
_itemName = lbData [1500,(lbCurSel 1500)];

if (parseNumber(ctrlText 1400) < 1) exitWith {hint "Selected 0, Invalid!";};
if ((parseNumber(ctrlText 1400)) > ((call compile _itemName))) exitWith {hint "Too High Capacity.";};

_thirstArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Thirst");
_hungerArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Hunger");
_drugArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Drugs");

if (!(_itemName IN _thirstArray) && !(_itemName IN _hungerArray) && !(_itemName IN _drugArray) && !(_itemName isEqualTo "Item_Gloves")) exitWith {hint localize "STR_Notification_NoUsage";};

[(lbData [1500,(lbCurSel 1500)]),(parseNumber(ctrlText 1400)),false] call A3PI_fnc_handleItem;

if (_itemName IN _thirstArray) then {
	_increase = getNumber (missionConfigFile >> "Server_Items" >> _itemName >> "thirst");
	A3PI_Thirst = A3PI_Thirst + (_increase * (parseNumber(ctrlText 1400)));
	if (A3PI_Thirst > 100) then {A3PI_Thirst = 100;};
};

if (_itemName IN _hungerArray) then {
	_increase = getNumber (missionConfigFile >> "Server_Items" >> _itemName >> "hunger");
	A3PI_Hunger = A3PI_Hunger + (_increase * (parseNumber(ctrlText 1400)));
	if (A3PI_Hunger > 100) then {A3PI_Hunger = 100;};
};

if (_itemName IN _drugArray) then {
	_onDrugs = player getVariable "_onDrugs";
	if(_onDrugs)then{
		[localize"STR_Notification_onDrugs",10,"red"]call A3PI_fnc_msg;
	}else{
		[_itemName] spawn A3PI_fnc_drugEffects;
	};
};

if (_itemName isEqualTo "Item_Gloves") then {
	player setVariable ["_haveGloves",true,true];
};

call A3PI_fnc_updateInventory;



