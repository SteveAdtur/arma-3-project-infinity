/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_dropItem.sqf
Author: Heisen http://heisen.pw
Description: Drop Item? Self Explained..
Parameter(s): N/A
************************************************************/


private ["_displayModel"];

if ((lbCurSel 1500) isEqualTo -1) exitWith {hint "Nothing Selected!";};

_item = (missionConfigFile >> "Server_Items" >> (lbData [1500,(lbCurSel 1500)]));
_itemName = lbData [1500,(lbCurSel 1500)];

if (parseNumber(ctrlText 1400) < 1) exitWith {hint "Selected 0, Invalid!";};
if ((parseNumber(ctrlText 1400)) > ((call compile _itemName))) exitWith {hint "Too High Capacity.";};

if (((lbData [1500,(lbCurSel 1500)]) isEqualTo "Item_Cash") && ((parseNumber(ctrlText 1400)) < 250)) exitWith {hint "Only drop 250 Min.";};

[(lbData [1500,(lbCurSel 1500)]),(parseNumber(ctrlText 1400)),false] call A3PI_fnc_handleItem;
if((['Item_SpareWheel'] call A3PI_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};

call A3PI_fnc_updateInventory;

_displayModel = getText(_item >> "displayModel");
if (_displayModel isEqualTo "") then {
	_displayModel = "Land_Pillow_grey_F";
};

_item = createVehicle [_displayModel,[((getPos player) select 0),((getPos player) select 1),(getPos player) select 2], [], 0, "CAN_COLLIDE"];
_item setVariable ["Server_Item_Data",([_itemName,(parseNumber(ctrlText 1400))]),true];