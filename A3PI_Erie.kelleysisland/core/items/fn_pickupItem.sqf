/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_pickupItem.sqf
Author: Heisen http://heisen.pw
Description: Pickup Item? Self Explained..
Parameter(s): N/A
************************************************************/


params [
	"_item"
];

systemChat format ["%1",_item];

[((_item getVariable "Server_Item_Data") select 0),((_item getVariable "Server_Item_Data") select 1),true] call A3PI_fnc_handleItem;
player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
deleteVehicle _item;	
