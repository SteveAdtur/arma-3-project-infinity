/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3PIife http://www.arma-life.com/forums
File: fn_recieveItem.sqf
Author: Heisen http://heisen.pw
Description: Recieve Item
Parameter(s): N/A
************************************************************/


params [
	"_item",
	"_amount"
];

[_item,_amount,true] call A3PI_fnc_handleItem;

//--- Stop those dirty dupers...
A3PI_SyncRecent = false;
[] spawn A3PI_fnc_updateRequest;